﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary::set_spriteLibraryAsset(UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset)
extern void SpriteLibrary_set_spriteLibraryAsset_mA0FEE070CA470C87B4B88344FF52F50F959D272D ();
// 0x00000002 UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset UnityEngine.Experimental.U2D.Animation.SpriteLibrary::get_spriteLibraryAsset()
extern void SpriteLibrary_get_spriteLibraryAsset_m7BCCFB664DF26CE614C1E8D47ABBAF8C9267A047 ();
// 0x00000003 UnityEngine.Sprite UnityEngine.Experimental.U2D.Animation.SpriteLibrary::GetSprite(System.String,System.String)
extern void SpriteLibrary_GetSprite_m9682FEED4B15AF5DAD098905B0EC06E63E4A26B9 ();
// 0x00000004 UnityEngine.Sprite UnityEngine.Experimental.U2D.Animation.SpriteLibrary::GetSprite(System.Int32,System.Int32)
extern void SpriteLibrary_GetSprite_m34A399148F3F9155C653B465DF7EB9CE51C458CA ();
// 0x00000005 UnityEngine.Sprite UnityEngine.Experimental.U2D.Animation.SpriteLibrary::GetSprite(System.Int32,System.Int32,System.Boolean&)
extern void SpriteLibrary_GetSprite_m1F5EABC7E2641F05C12BE6DB357C66E6DB5BB162 ();
// 0x00000006 System.String UnityEngine.Experimental.U2D.Animation.SpriteLibrary::GetCategoryNameFromHash(System.Int32)
extern void SpriteLibrary_GetCategoryNameFromHash_m7B2985C1F203C863E8DB97E34455158F3A1119C1 ();
// 0x00000007 System.String UnityEngine.Experimental.U2D.Animation.SpriteLibrary::GetLabelNameFromHash(System.Int32,System.Int32)
extern void SpriteLibrary_GetLabelNameFromHash_m7D60F0940CBEF2AE94427C29686172CBFDBDF962 ();
// 0x00000008 System.Collections.Generic.Dictionary`2<UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash,UnityEngine.Sprite> UnityEngine.Experimental.U2D.Animation.SpriteLibrary::GetCategoryOverride(System.String,System.Boolean)
extern void SpriteLibrary_GetCategoryOverride_mF8C83E5448048A323850A849E364116C397D9BE1 ();
// 0x00000009 System.Collections.Generic.Dictionary`2<UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash,UnityEngine.Sprite> UnityEngine.Experimental.U2D.Animation.SpriteLibrary::GetCategoryOverride(UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash,System.Boolean)
extern void SpriteLibrary_GetCategoryOverride_m81C559703764E5CB90CB62AABF7B42287C479C82 ();
// 0x0000000A System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary::AddSpriteToOverride(System.Collections.Generic.Dictionary`2<UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash,UnityEngine.Sprite>,UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash,UnityEngine.Sprite)
extern void SpriteLibrary_AddSpriteToOverride_mC674D76EB4C8FFCC04B0EFF51E462D464E62826F ();
// 0x0000000B System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary::AddOverride(UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset,System.String,System.String)
extern void SpriteLibrary_AddOverride_m5C9F89B784815B4CD39BAFAAB2BE2EC78777C1B3 ();
// 0x0000000C System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary::AddOverride(UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset,System.String)
extern void SpriteLibrary_AddOverride_mF2A2CC88048539F5968ADA55BEBA611540F5EA7B ();
// 0x0000000D System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary::AddOverride(UnityEngine.Sprite,System.String,System.String)
extern void SpriteLibrary_AddOverride_mF4EABE9FD82EE08677EB1009717C3695CCBD5FA6 ();
// 0x0000000E System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary::RemoveOverride(System.String)
extern void SpriteLibrary_RemoveOverride_m1E46415C65A43E69F600D3D95E3D13C01BD045FD ();
// 0x0000000F System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary::RemoveOverride(System.String,System.String)
extern void SpriteLibrary_RemoveOverride_mEED0056A191D7ED22F574C86C96684643F1B08E9 ();
// 0x00000010 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibrary::HasOverride(System.String,System.String)
extern void SpriteLibrary_HasOverride_m909553DCBA9783B33683D5B3FCAF96D3A17928AF ();
// 0x00000011 System.Collections.Generic.List`1<UnityEngine.Experimental.U2D.Animation.SpriteLibCategory> UnityEngine.Experimental.U2D.Animation.SpriteLibrary::get_labels()
extern void SpriteLibrary_get_labels_mC82C744091883CE3861916497FB6C58CF33DA223 ();
// 0x00000012 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary::RefreshSpriteResolvers()
extern void SpriteLibrary_RefreshSpriteResolvers_m41E0EB038F53CFAD4319127C1AF0CA8EF531515F ();
// 0x00000013 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary::.ctor()
extern void SpriteLibrary__ctor_m9E8692C6F9E370D360B763A30A1F4E1B8EB4AE3E ();
// 0x00000014 System.String UnityEngine.Experimental.U2D.Animation.INameHash::get_name()
// 0x00000015 System.Void UnityEngine.Experimental.U2D.Animation.INameHash::set_name(System.String)
// 0x00000016 System.Int32 UnityEngine.Experimental.U2D.Animation.INameHash::get_hash()
// 0x00000017 System.String UnityEngine.Experimental.U2D.Animation.Categorylabel::get_name()
extern void Categorylabel_get_name_m77E12FA9DCBE4DC689CCEB3FCD953E45648FDB2E ();
// 0x00000018 System.Void UnityEngine.Experimental.U2D.Animation.Categorylabel::set_name(System.String)
extern void Categorylabel_set_name_m7C5C09B1AA94F04F0F44A5C22BB8A04342E44E9B ();
// 0x00000019 System.Int32 UnityEngine.Experimental.U2D.Animation.Categorylabel::get_hash()
extern void Categorylabel_get_hash_mC6A2629C13E9F3CBFF1DB7CD03228C4844366436 ();
// 0x0000001A UnityEngine.Sprite UnityEngine.Experimental.U2D.Animation.Categorylabel::get_sprite()
extern void Categorylabel_get_sprite_m87F2393663AF2C0EBACD834CE86E4C0D513C6B2D ();
// 0x0000001B System.Void UnityEngine.Experimental.U2D.Animation.Categorylabel::set_sprite(UnityEngine.Sprite)
extern void Categorylabel_set_sprite_m6E2EE0544770E8241DCCCF549DE0B2D88027228C ();
// 0x0000001C System.Void UnityEngine.Experimental.U2D.Animation.Categorylabel::UpdateHash()
extern void Categorylabel_UpdateHash_m7F3526FB329C0AE17B0209F6BD5C148F07BF1ED5 ();
// 0x0000001D System.Void UnityEngine.Experimental.U2D.Animation.Categorylabel::.ctor()
extern void Categorylabel__ctor_m6C938CD36124AC60F4BE2ECC2BF023CDD571DB14 ();
// 0x0000001E System.String UnityEngine.Experimental.U2D.Animation.SpriteLibCategory::get_name()
extern void SpriteLibCategory_get_name_m6D7CD681165670EBC91E5EF539AB5E9E1A058C6B ();
// 0x0000001F System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibCategory::set_name(System.String)
extern void SpriteLibCategory_set_name_m655A41BA7304CCBF0D6C895F32EBF2A14EB6DDC0 ();
// 0x00000020 System.Int32 UnityEngine.Experimental.U2D.Animation.SpriteLibCategory::get_hash()
extern void SpriteLibCategory_get_hash_mA6EC4331446F264E24734836F6E13F51441B91AB ();
// 0x00000021 System.Collections.Generic.List`1<UnityEngine.Experimental.U2D.Animation.Categorylabel> UnityEngine.Experimental.U2D.Animation.SpriteLibCategory::get_categoryList()
extern void SpriteLibCategory_get_categoryList_mDFD6DECFDD66673E4C69C6A9EF2F8E4D91979DAC ();
// 0x00000022 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibCategory::set_categoryList(System.Collections.Generic.List`1<UnityEngine.Experimental.U2D.Animation.Categorylabel>)
extern void SpriteLibCategory_set_categoryList_mBEE534CD87140DEB2B704A4C174AACBFE37EFEFC ();
// 0x00000023 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibCategory::UpdateHash()
extern void SpriteLibCategory_UpdateHash_mCCA764BEC65FC8BACD659DDC524BD968D2C52A8F ();
// 0x00000024 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibCategory::ValidateLabels()
extern void SpriteLibCategory_ValidateLabels_mBD4681B482F479DC56AA1DB8CC584824BA25C6F8 ();
// 0x00000025 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibCategory::.ctor()
extern void SpriteLibCategory__ctor_m534F2F5C8B783DD99C5263AB4432EF3167D0C334 ();
// 0x00000026 System.Collections.Generic.List`1<UnityEngine.Experimental.U2D.Animation.SpriteLibCategory> UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::get_categories()
extern void SpriteLibraryAsset_get_categories_m4FF523E6F9FD12EA8E9B33DC26EDDF7F0856C3C3 ();
// 0x00000027 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::set_categories(System.Collections.Generic.List`1<UnityEngine.Experimental.U2D.Animation.SpriteLibCategory>)
extern void SpriteLibraryAsset_set_categories_m30A285AD1BCCFBCB52B36168305293AA51FE3DE2 ();
// 0x00000028 UnityEngine.Sprite UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::GetSprite(System.Int32,System.Int32)
extern void SpriteLibraryAsset_GetSprite_m69066DF6655A2207DAA2FE75E3CA1F6ACBC541FA ();
// 0x00000029 UnityEngine.Sprite UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::GetSprite(System.Int32,System.Int32,System.Boolean&)
extern void SpriteLibraryAsset_GetSprite_m080EBB15B251625433787ECBC12FEDDF15A5EA59 ();
// 0x0000002A UnityEngine.Sprite UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::GetSprite(System.String,System.String)
extern void SpriteLibraryAsset_GetSprite_mA19217250650444187A9A61EA5616CEE7AC5C648 ();
// 0x0000002B System.Collections.Generic.IEnumerable`1<System.String> UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::GetCategoryNames()
extern void SpriteLibraryAsset_GetCategoryNames_mAC04EA9C21A5797C95B3F47E92D3BA6E824166CF ();
// 0x0000002C System.Collections.Generic.IEnumerable`1<System.String> UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::GetCategorylabelNames(System.String)
extern void SpriteLibraryAsset_GetCategorylabelNames_m6C01E2E282DD50637BA08F54128127FF04BB40B5 ();
// 0x0000002D System.Collections.Generic.IEnumerable`1<System.String> UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::GetCategoryLabelNames(System.String)
extern void SpriteLibraryAsset_GetCategoryLabelNames_mD95D19094468BBEDEC604043B8E8FAC90E5F2126 ();
// 0x0000002E System.String UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::GetCategoryNameFromHash(System.Int32)
extern void SpriteLibraryAsset_GetCategoryNameFromHash_m45E58B32C998A6943D0844584D1F6CD3F0373873 ();
// 0x0000002F System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::AddCategoryLabel(UnityEngine.Sprite,System.String,System.String)
extern void SpriteLibraryAsset_AddCategoryLabel_mAE6805375A45E969092AFC08E547C0EDFEC303BC ();
// 0x00000030 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::RemoveCategoryLabel(System.String,System.String,System.Boolean)
extern void SpriteLibraryAsset_RemoveCategoryLabel_mC1809CB5762381CF72912E631D8BD7974CCCE20F ();
// 0x00000031 System.String UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::GetLabelNameFromHash(System.Int32,System.Int32)
extern void SpriteLibraryAsset_GetLabelNameFromHash_m7C303D933841F22E6E12E7635424832BFA6100BC ();
// 0x00000032 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::UpdateHashes()
extern void SpriteLibraryAsset_UpdateHashes_m7499B3104717798437ABEEE66C747EE946473904 ();
// 0x00000033 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::ValidateCategories()
extern void SpriteLibraryAsset_ValidateCategories_m10507DB04448F9BD0AD9957B016CF24ABE7F7BFE ();
// 0x00000034 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::RenameDuplicate(System.Collections.Generic.IEnumerable`1<UnityEngine.Experimental.U2D.Animation.INameHash>,System.Action`2<System.String,System.String>)
extern void SpriteLibraryAsset_RenameDuplicate_mC152BC315A5BD967F2E58F6804C092638ADD7DC4 ();
// 0x00000035 System.Int32 UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::Default_GetStringHash(System.String)
extern void SpriteLibraryAsset_Default_GetStringHash_mEEA523A2B020CE627D5BE6DF0808BBD94F6881EC ();
// 0x00000036 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::.ctor()
extern void SpriteLibraryAsset__ctor_mCD67168CD348AA1FBD954B94D7F0ED6B5B711CC9 ();
// 0x00000037 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::.cctor()
extern void SpriteLibraryAsset__cctor_mAE5ED49F14F61076BFE7BE390EEAF08ECBB0D1D9 ();
// 0x00000038 System.Void UnityEngine.Experimental.U2D.Animation.SpriteResolver::OnEnable()
extern void SpriteResolver_OnEnable_mEBC33FDD853902157CBD8A2762DD2D6F2457D983 ();
// 0x00000039 UnityEngine.SpriteRenderer UnityEngine.Experimental.U2D.Animation.SpriteResolver::get_spriteRenderer()
extern void SpriteResolver_get_spriteRenderer_m2F385D8D6F09C2493CE28862FF523A9E9A7F5208 ();
// 0x0000003A System.Void UnityEngine.Experimental.U2D.Animation.SpriteResolver::SetCategoryAndLabel(System.String,System.String)
extern void SpriteResolver_SetCategoryAndLabel_m5C74BC88070E6CB13E883A6F5A8221D390BC7FC8 ();
// 0x0000003B System.String UnityEngine.Experimental.U2D.Animation.SpriteResolver::GetCategory()
extern void SpriteResolver_GetCategory_mB4F887D8124D040B0975EAEE99DD803B2AD9EF14 ();
// 0x0000003C System.String UnityEngine.Experimental.U2D.Animation.SpriteResolver::GetLabel()
extern void SpriteResolver_GetLabel_m5B57515D2C2B5FE4242B350DB00D02A59444B2CF ();
// 0x0000003D UnityEngine.Experimental.U2D.Animation.SpriteLibrary UnityEngine.Experimental.U2D.Animation.SpriteResolver::get_spriteLibrary()
extern void SpriteResolver_get_spriteLibrary_mF5B27288A55774226484557F7B00DBEA4362DBAA ();
// 0x0000003E System.Void UnityEngine.Experimental.U2D.Animation.SpriteResolver::LateUpdate()
extern void SpriteResolver_LateUpdate_m72BA76C1CAE96EB77EA42EF2E5171CC4295AD164 ();
// 0x0000003F UnityEngine.Sprite UnityEngine.Experimental.U2D.Animation.SpriteResolver::GetSprite(System.Boolean&)
extern void SpriteResolver_GetSprite_mC48A6ABB24F0D99F36F2A47B1785B030300995D5 ();
// 0x00000040 System.Void UnityEngine.Experimental.U2D.Animation.SpriteResolver::ResolveSpriteToSpriteRenderer()
extern void SpriteResolver_ResolveSpriteToSpriteRenderer_m51041C425DC96F34D2F4F74AE92B3B78279E2EFA ();
// 0x00000041 System.Void UnityEngine.Experimental.U2D.Animation.SpriteResolver::OnTransformParentChanged()
extern void SpriteResolver_OnTransformParentChanged_m1268B5B949D5515D11E95367111635D88FEB02B7 ();
// 0x00000042 System.Int32 UnityEngine.Experimental.U2D.Animation.SpriteResolver::get_categoryHashInt()
extern void SpriteResolver_get_categoryHashInt_mAC6D8CEE85C8C2D4F3FD97210B85BA41446E8A85 ();
// 0x00000043 System.Void UnityEngine.Experimental.U2D.Animation.SpriteResolver::set_categoryHashInt(System.Int32)
extern void SpriteResolver_set_categoryHashInt_mDBC93AC493A0FB028220DE206F7F82886E005DF6 ();
// 0x00000044 System.Int32 UnityEngine.Experimental.U2D.Animation.SpriteResolver::get_labelHashInt()
extern void SpriteResolver_get_labelHashInt_m368982B2740B6075512349B58E862ACEEF6A3ECE ();
// 0x00000045 System.Void UnityEngine.Experimental.U2D.Animation.SpriteResolver::set_labelHashInt(System.Int32)
extern void SpriteResolver_set_labelHashInt_mF1C14B48E4BB28DCBE809DEFC85092A145DFB4BA ();
// 0x00000046 System.Int32 UnityEngine.Experimental.U2D.Animation.SpriteResolver::ConvertFloatToInt(System.Single)
extern void SpriteResolver_ConvertFloatToInt_mFD242FE17F00FEEDBC6098702C487EB6F7481DA6 ();
// 0x00000047 System.Single UnityEngine.Experimental.U2D.Animation.SpriteResolver::ConvertIntToFloat(System.Int32)
extern void SpriteResolver_ConvertIntToFloat_mDF7459E8F9E0DA2A4F6F9BF37CDB3585A8101642 ();
// 0x00000048 System.Void UnityEngine.Experimental.U2D.Animation.SpriteResolver::.ctor()
extern void SpriteResolver__ctor_m8BF156B2B1A4FE23822E4BB4E2AEAB1C10D0D6D9 ();
// 0x00000049 System.Void UnityEngine.U2D.Animation.NativeArrayHelpers::ResizeIfNeeded(Unity.Collections.NativeArray`1<T>&,System.Int32,Unity.Collections.Allocator)
// 0x0000004A System.Void UnityEngine.U2D.Animation.NativeArrayHelpers::DisposeIfCreated(Unity.Collections.NativeArray`1<T>)
// 0x0000004B System.Void UnityEngine.U2D.Animation.NativeArrayHelpers::CopyFromNativeSlice(Unity.Collections.NativeArray`1<T>,System.Int32,System.Int32,Unity.Collections.NativeSlice`1<S>,System.Int32,System.Int32)
// 0x0000004C UnityEngine.U2D.Animation.NativeCustomSlice`1<T> UnityEngine.U2D.Animation.NativeCustomSlice`1::Default()
// 0x0000004D System.Void UnityEngine.U2D.Animation.NativeCustomSlice`1::.ctor(Unity.Collections.NativeSlice`1<T>)
// 0x0000004E System.Void UnityEngine.U2D.Animation.NativeCustomSlice`1::.ctor(Unity.Collections.NativeSlice`1<System.Byte>,System.Int32,System.Int32)
// 0x0000004F T UnityEngine.U2D.Animation.NativeCustomSlice`1::get_Item(System.Int32)
// 0x00000050 System.Int32 UnityEngine.U2D.Animation.NativeCustomSlice`1::get_Length()
// 0x00000051 System.Void UnityEngine.U2D.Animation.NativeCustomSliceEnumerator`1::.ctor(Unity.Collections.NativeSlice`1<System.Byte>,System.Int32,System.Int32)
// 0x00000052 System.Collections.Generic.IEnumerator`1<T> UnityEngine.U2D.Animation.NativeCustomSliceEnumerator`1::GetEnumerator()
// 0x00000053 System.Collections.IEnumerator UnityEngine.U2D.Animation.NativeCustomSliceEnumerator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000054 System.Boolean UnityEngine.U2D.Animation.NativeCustomSliceEnumerator`1::MoveNext()
// 0x00000055 System.Void UnityEngine.U2D.Animation.NativeCustomSliceEnumerator`1::Reset()
// 0x00000056 T UnityEngine.U2D.Animation.NativeCustomSliceEnumerator`1::get_Current()
// 0x00000057 System.Object UnityEngine.U2D.Animation.NativeCustomSliceEnumerator`1::System.Collections.IEnumerator.get_Current()
// 0x00000058 System.Void UnityEngine.U2D.Animation.NativeCustomSliceEnumerator`1::System.IDisposable.Dispose()
// 0x00000059 System.Void UnityEngine.U2D.Animation.DeformVerticesBuffer::.ctor(System.Int32)
extern void DeformVerticesBuffer__ctor_mC9B318B4FFD14AC9353A01E1EDA611C71FC17751_AdjustorThunk ();
// 0x0000005A System.Void UnityEngine.U2D.Animation.DeformVerticesBuffer::Dispose()
extern void DeformVerticesBuffer_Dispose_mE2222F5C6C1C013BBBC9A22C77B768C522E5175B_AdjustorThunk ();
// 0x0000005B Unity.Collections.NativeArray`1<System.Byte>& UnityEngine.U2D.Animation.DeformVerticesBuffer::GetBuffer(System.Int32)
extern void DeformVerticesBuffer_GetBuffer_m3AA5A45F38094C05FB8B4247A19B086A2879C23B_AdjustorThunk ();
// 0x0000005C Unity.Collections.NativeArray`1<System.Byte>& UnityEngine.U2D.Animation.DeformVerticesBuffer::GetCurrentBuffer()
extern void DeformVerticesBuffer_GetCurrentBuffer_m3A7BD4BEE612BAEF6A5E1A12C9BBAABBEAB5C878_AdjustorThunk ();
// 0x0000005D System.Boolean UnityEngine.U2D.Animation.SpriteSkin::get_batchSkinning()
extern void SpriteSkin_get_batchSkinning_mC737C08514346C43DE836606C7C461BFD72B3E37 ();
// 0x0000005E System.Void UnityEngine.U2D.Animation.SpriteSkin::set_batchSkinning(System.Boolean)
extern void SpriteSkin_set_batchSkinning_m9AF365F2F0BDA385CC0E1BDA11AC3418CFE494F9 ();
// 0x0000005F System.Int32 UnityEngine.U2D.Animation.SpriteSkin::GetSpriteInstanceID()
extern void SpriteSkin_GetSpriteInstanceID_mDCDE405C695CB58B8351B4430CE81F9578643A2E ();
// 0x00000060 System.Void UnityEngine.U2D.Animation.SpriteSkin::OnEnable()
extern void SpriteSkin_OnEnable_m08D1FD40FDD317DB64ABDE00485A71994071DA6F ();
// 0x00000061 System.Void UnityEngine.U2D.Animation.SpriteSkin::CacheValidFlag()
extern void SpriteSkin_CacheValidFlag_mA0FBC307286CE6EB143E0BBBA734FE8575089CB6 ();
// 0x00000062 System.Void UnityEngine.U2D.Animation.SpriteSkin::Reset()
extern void SpriteSkin_Reset_m53994906194BC69065CF9014B199CDFB7E76823D ();
// 0x00000063 System.Void UnityEngine.U2D.Animation.SpriteSkin::UseBatching(System.Boolean)
extern void SpriteSkin_UseBatching_mEC2BD48461983D6971D9AB20E9C58DD73DF2D199 ();
// 0x00000064 Unity.Collections.NativeArray`1<System.Byte>& UnityEngine.U2D.Animation.SpriteSkin::GetDeformedVertices(System.Int32)
extern void SpriteSkin_GetDeformedVertices_m01E711868ED3F0F7702F279B113CA3B044796CD9 ();
// 0x00000065 System.Boolean UnityEngine.U2D.Animation.SpriteSkin::HasCurrentDeformedVertices()
extern void SpriteSkin_HasCurrentDeformedVertices_mD745AE94F89889CD552C7A999C64C299D39A5B78 ();
// 0x00000066 Unity.Collections.NativeArray`1<System.Byte> UnityEngine.U2D.Animation.SpriteSkin::GetCurrentDeformedVertices()
extern void SpriteSkin_GetCurrentDeformedVertices_m8F8EBBE7EE938415C9B0DB00DF39C8D91D53E0A0 ();
// 0x00000067 Unity.Collections.NativeSlice`1<UnityEngine.U2D.Animation.PositionVertex> UnityEngine.U2D.Animation.SpriteSkin::GetCurrentDeformedVertexPositions()
extern void SpriteSkin_GetCurrentDeformedVertexPositions_m46403BEA09111D5CCCF5D6440F5EAFBBE9747901 ();
// 0x00000068 Unity.Collections.NativeSlice`1<UnityEngine.U2D.Animation.PositionTangentVertex> UnityEngine.U2D.Animation.SpriteSkin::GetCurrentDeformedVertexPositionsAndTangents()
extern void SpriteSkin_GetCurrentDeformedVertexPositionsAndTangents_mC0EA62325BB61E327808E4BD82D5EC056FC809D6 ();
// 0x00000069 System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3> UnityEngine.U2D.Animation.SpriteSkin::GetDeformedVertexPositionData()
extern void SpriteSkin_GetDeformedVertexPositionData_m89448503FE097E50BBA8F27F971823ED72D45A40 ();
// 0x0000006A System.Collections.Generic.IEnumerable`1<UnityEngine.Vector4> UnityEngine.U2D.Animation.SpriteSkin::GetDeformedVertexTangentData()
extern void SpriteSkin_GetDeformedVertexTangentData_m816731F0ED83B51D05E935D79052C073103E482C ();
// 0x0000006B System.Void UnityEngine.U2D.Animation.SpriteSkin::OnDisable()
extern void SpriteSkin_OnDisable_m04F5BAD3906AD83E24FF9D2B3CA9FF8BD0395414 ();
// 0x0000006C System.Void UnityEngine.U2D.Animation.SpriteSkin::LateUpdate()
extern void SpriteSkin_LateUpdate_mA3EE66458E56228A4DF2DCB752F73915D497A42C ();
// 0x0000006D System.Void UnityEngine.U2D.Animation.SpriteSkin::CacheCurrentSprite()
extern void SpriteSkin_CacheCurrentSprite_m79653521A99F2DBA4680F0EC742007EC96CCF2F4 ();
// 0x0000006E UnityEngine.Sprite UnityEngine.U2D.Animation.SpriteSkin::get_sprite()
extern void SpriteSkin_get_sprite_m6AE7F0414180DBED689CB55A2078AB694F11B7E3 ();
// 0x0000006F UnityEngine.SpriteRenderer UnityEngine.U2D.Animation.SpriteSkin::get_spriteRenderer()
extern void SpriteSkin_get_spriteRenderer_m82F94ED567A37F8EACBA9B853F45B78CDBDBF945 ();
// 0x00000070 UnityEngine.Transform[] UnityEngine.U2D.Animation.SpriteSkin::get_boneTransforms()
extern void SpriteSkin_get_boneTransforms_m183C0E96FABE7B91545847416AA86066373D35C8 ();
// 0x00000071 System.Void UnityEngine.U2D.Animation.SpriteSkin::set_boneTransforms(UnityEngine.Transform[])
extern void SpriteSkin_set_boneTransforms_m12AD63B0C8C41D92E50E51427029E561A1E00B03 ();
// 0x00000072 UnityEngine.Transform UnityEngine.U2D.Animation.SpriteSkin::get_rootBone()
extern void SpriteSkin_get_rootBone_m41EF7EF107941B23B870FA57B415A663511CCE28 ();
// 0x00000073 System.Void UnityEngine.U2D.Animation.SpriteSkin::set_rootBone(UnityEngine.Transform)
extern void SpriteSkin_set_rootBone_m5418FBC215563D71A1AB693AAF2FCBADD28447F7 ();
// 0x00000074 UnityEngine.Bounds UnityEngine.U2D.Animation.SpriteSkin::get_bounds()
extern void SpriteSkin_get_bounds_mBD2295F3506D7E4275551DF6D4ED104AE905F7FF ();
// 0x00000075 System.Void UnityEngine.U2D.Animation.SpriteSkin::set_bounds(UnityEngine.Bounds)
extern void SpriteSkin_set_bounds_m00A9C0006889F784E671A812DB50D2CCFD836A1C ();
// 0x00000076 System.Boolean UnityEngine.U2D.Animation.SpriteSkin::get_alwaysUpdate()
extern void SpriteSkin_get_alwaysUpdate_m3CFC011C782A14D6AB8BFCDE192665AEA9A36DE0 ();
// 0x00000077 System.Void UnityEngine.U2D.Animation.SpriteSkin::set_alwaysUpdate(System.Boolean)
extern void SpriteSkin_set_alwaysUpdate_mC8A775AD29A19B97AF9C5B30E2EC77683E93DE4A ();
// 0x00000078 System.Boolean UnityEngine.U2D.Animation.SpriteSkin::get_isValid()
extern void SpriteSkin_get_isValid_m82C5221C64A235734D9F2A2AEFF1B16E8B412CCF ();
// 0x00000079 System.Void UnityEngine.U2D.Animation.SpriteSkin::OnDestroy()
extern void SpriteSkin_OnDestroy_mC081F8C17D696D38916E594DF893C2A9B1DB62B6 ();
// 0x0000007A System.Void UnityEngine.U2D.Animation.SpriteSkin::DeactivateSkinning()
extern void SpriteSkin_DeactivateSkinning_mFA5F6C270585C1C093FCBC51EC06D94FEC030245 ();
// 0x0000007B System.Void UnityEngine.U2D.Animation.SpriteSkin::ResetSprite()
extern void SpriteSkin_ResetSprite_mEC1A5630E2E302E292790253D5016263A8C39770 ();
// 0x0000007C System.Void UnityEngine.U2D.Animation.SpriteSkin::OnBeforeSerialize()
extern void SpriteSkin_OnBeforeSerialize_m1C2A4F79DA2D358A16EB77D7BD315A2C7B75C336 ();
// 0x0000007D System.Void UnityEngine.U2D.Animation.SpriteSkin::OnAfterDeserialize()
extern void SpriteSkin_OnAfterDeserialize_mE881E52D0591FAA7646063BAC21016087B8D0D94 ();
// 0x0000007E System.Void UnityEngine.U2D.Animation.SpriteSkin::OnEnableBatch()
extern void SpriteSkin_OnEnableBatch_m98ECFE4A60FB5F600D9CFEF85A7BEF9B76E1B133 ();
// 0x0000007F System.Void UnityEngine.U2D.Animation.SpriteSkin::UpdateSpriteDeform()
extern void SpriteSkin_UpdateSpriteDeform_m71D0D44ED98E197E87ED2F2DB827F2955D58BC76 ();
// 0x00000080 System.Void UnityEngine.U2D.Animation.SpriteSkin::OnResetBatch()
extern void SpriteSkin_OnResetBatch_m103F0EFDC15B728FA34ED8D38DA1FB32EFD02052 ();
// 0x00000081 System.Void UnityEngine.U2D.Animation.SpriteSkin::UseBatchingBatch()
extern void SpriteSkin_UseBatchingBatch_mA294DAC76182455D472F7A9D513C2FAB533DD760 ();
// 0x00000082 System.Void UnityEngine.U2D.Animation.SpriteSkin::OnDisableBatch()
extern void SpriteSkin_OnDisableBatch_mAD4BD1C1F3100FEFE93A7F02DDF610364A1955C3 ();
// 0x00000083 System.Void UnityEngine.U2D.Animation.SpriteSkin::OnBoneTransformChanged()
extern void SpriteSkin_OnBoneTransformChanged_mA98F985F4FFC490ED4BA5B59BC7AD71014CEDDA9 ();
// 0x00000084 System.Void UnityEngine.U2D.Animation.SpriteSkin::OnRootBoneTransformChanged()
extern void SpriteSkin_OnRootBoneTransformChanged_mD3E9A0C2D817897FE8A3F75BF00D34D496B80EB5 ();
// 0x00000085 System.Void UnityEngine.U2D.Animation.SpriteSkin::OnBeforeSerializeBatch()
extern void SpriteSkin_OnBeforeSerializeBatch_m11E59A0DD2ED20B230DCF132A67E35D0B58FD5A1 ();
// 0x00000086 System.Void UnityEngine.U2D.Animation.SpriteSkin::OnAfterSerializeBatch()
extern void SpriteSkin_OnAfterSerializeBatch_m25FDCEE4CD07869CBFB2E02733999FE77747FCD5 ();
// 0x00000087 System.Void UnityEngine.U2D.Animation.SpriteSkin::.ctor()
extern void SpriteSkin__ctor_mBE9343783A2E6D17C1805B81EE3F5AACFCA7EAF8 ();
// 0x00000088 System.Void UnityEngine.U2D.Animation.SpriteSkinEntity::.ctor()
extern void SpriteSkinEntity__ctor_mDDFFEC4F9F8DCC59323237F8A64D248A1476D967 ();
// 0x00000089 System.Void UnityEngine.U2D.Animation.SpriteSkinManager::.ctor()
extern void SpriteSkinManager__ctor_mA17787B1FA7F7F49541CA2ABF89FE233432A5E9A ();
// 0x0000008A UnityEngine.U2D.Animation.SpriteSkinValidationResult UnityEngine.U2D.Animation.SpriteSkinUtility::Validate(UnityEngine.U2D.Animation.SpriteSkin)
extern void SpriteSkinUtility_Validate_m71FEB7305CE1363B9A0C2AA3A61A3ECBAB81D268 ();
// 0x0000008B System.Void UnityEngine.U2D.Animation.SpriteSkinUtility::CreateBoneHierarchy(UnityEngine.U2D.Animation.SpriteSkin)
extern void SpriteSkinUtility_CreateBoneHierarchy_m8E094C6FC8320B23535470382E1D55DB0723BE88 ();
// 0x0000008C System.Int32 UnityEngine.U2D.Animation.SpriteSkinUtility::GetVertexStreamSize(UnityEngine.Sprite)
extern void SpriteSkinUtility_GetVertexStreamSize_m10CAAE580EB073958C3303B642AD7486BF356AD5 ();
// 0x0000008D System.Int32 UnityEngine.U2D.Animation.SpriteSkinUtility::GetVertexStreamOffset(UnityEngine.Sprite,UnityEngine.Rendering.VertexAttribute)
extern void SpriteSkinUtility_GetVertexStreamOffset_m2880DAA60A443378FD86E06801AB65E28456FF5D ();
// 0x0000008E System.Void UnityEngine.U2D.Animation.SpriteSkinUtility::CreateGameObject(System.Int32,UnityEngine.U2D.SpriteBone[],UnityEngine.Transform[],UnityEngine.Transform)
extern void SpriteSkinUtility_CreateGameObject_mE75B48A0B298E1C5177C529BAEA01140B7782B22 ();
// 0x0000008F System.Void UnityEngine.U2D.Animation.SpriteSkinUtility::ResetBindPose(UnityEngine.U2D.Animation.SpriteSkin)
extern void SpriteSkinUtility_ResetBindPose_m79C549EB6CB1B7E7C4B7FC0E7AE4AB46E537536C ();
// 0x00000090 System.Void UnityEngine.U2D.Animation.SpriteSkinUtility::Rebind(UnityEngine.U2D.Animation.SpriteSkin)
extern void SpriteSkinUtility_Rebind_m022E4AC07A834AD2C75A5AC00067AD228025220F ();
// 0x00000091 System.String UnityEngine.U2D.Animation.SpriteSkinUtility::CalculateBoneTransformPath(System.Int32,UnityEngine.U2D.SpriteBone[])
extern void SpriteSkinUtility_CalculateBoneTransformPath_mE8B00C2441F0DE56A40525FCB08FA8B82AB32ABD ();
// 0x00000092 System.Int32 UnityEngine.U2D.Animation.SpriteSkinUtility::GetHash(UnityEngine.Matrix4x4)
extern void SpriteSkinUtility_GetHash_m5577396126C61F69FA6B857653C8BE886D4FD05E ();
// 0x00000093 System.Int32 UnityEngine.U2D.Animation.SpriteSkinUtility::CalculateTransformHash(UnityEngine.U2D.Animation.SpriteSkin)
extern void SpriteSkinUtility_CalculateTransformHash_m46EC0AC109953771CC9448AB399F6C8EF00B3938 ();
// 0x00000094 System.Void UnityEngine.U2D.Animation.SpriteSkinUtility::Deform(UnityEngine.Sprite,UnityEngine.Matrix4x4,Unity.Collections.NativeSlice`1<UnityEngine.Vector3>,Unity.Collections.NativeSlice`1<UnityEngine.Vector4>,Unity.Collections.NativeSlice`1<UnityEngine.BoneWeight>,Unity.Collections.NativeArray`1<UnityEngine.Matrix4x4>,Unity.Collections.NativeSlice`1<UnityEngine.Matrix4x4>,Unity.Collections.NativeArray`1<System.Byte>)
extern void SpriteSkinUtility_Deform_m981222CD3C07F2AB53BCD093ABE83C66C8D645A9 ();
// 0x00000095 System.Void UnityEngine.U2D.Animation.SpriteSkinUtility::Deform(Unity.Mathematics.float4x4,Unity.Collections.NativeSlice`1<Unity.Mathematics.float3>,Unity.Collections.NativeSlice`1<UnityEngine.BoneWeight>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4x4>,Unity.Collections.NativeSlice`1<Unity.Mathematics.float4x4>,Unity.Collections.NativeSlice`1<Unity.Mathematics.float3>)
extern void SpriteSkinUtility_Deform_mD9D8828D52CAB0155A364F2E1788DF9ECBE3E7A7 ();
// 0x00000096 System.Void UnityEngine.U2D.Animation.SpriteSkinUtility::Deform(Unity.Mathematics.float4x4,Unity.Collections.NativeSlice`1<Unity.Mathematics.float3>,Unity.Collections.NativeSlice`1<Unity.Mathematics.float4>,Unity.Collections.NativeSlice`1<UnityEngine.BoneWeight>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4x4>,Unity.Collections.NativeSlice`1<Unity.Mathematics.float4x4>,Unity.Collections.NativeSlice`1<Unity.Mathematics.float3>,Unity.Collections.NativeSlice`1<Unity.Mathematics.float4>)
extern void SpriteSkinUtility_Deform_m69ECADA90DB5A147A3C038B76EAF157F47D9B56C ();
// 0x00000097 System.Void UnityEngine.U2D.Animation.SpriteSkinUtility::Deform(UnityEngine.Sprite,UnityEngine.Matrix4x4,UnityEngine.Transform[],Unity.Collections.NativeArray`1<System.Byte>&)
extern void SpriteSkinUtility_Deform_mA57D36C6204C389924701F65D49C524A28F88576 ();
// 0x00000098 System.Void UnityEngine.U2D.Animation.SpriteSkinUtility::Bake(UnityEngine.U2D.Animation.SpriteSkin,Unity.Collections.NativeArray`1<System.Byte>&)
extern void SpriteSkinUtility_Bake_m6EA711E8B02646A1C3E7EBE382E7A83A265A06AE ();
// 0x00000099 System.Void UnityEngine.U2D.Animation.SpriteSkinUtility::CalculateBounds(UnityEngine.U2D.Animation.SpriteSkin)
extern void SpriteSkinUtility_CalculateBounds_mBEE2B66B9DA73087C7FB4B0A437E6F2B8F790833 ();
// 0x0000009A UnityEngine.Bounds UnityEngine.U2D.Animation.SpriteSkinUtility::CalculateSpriteSkinBounds(Unity.Collections.NativeSlice`1<Unity.Mathematics.float3>)
extern void SpriteSkinUtility_CalculateSpriteSkinBounds_mAD27C6FFE5E6DBBCDC623B3512F0C4EEE44BFF0D ();
// 0x0000009B System.Void UnityEngine.U2D.Animation.SpriteSkinUtility::UpdateBounds(UnityEngine.U2D.Animation.SpriteSkin,Unity.Collections.NativeArray`1<System.Byte>)
extern void SpriteSkinUtility_UpdateBounds_m378984E6D634AC1F47E946BDA367B634DFD83486 ();
// 0x0000009C System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash::.ctor(System.String)
extern void StringAndHash__ctor_m20980FA2D7D88F09C954E08B175D39EC589AD158 ();
// 0x0000009D System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash::.ctor(System.Int32)
extern void StringAndHash__ctor_m3AF9AF9296888D8F0284CE4256BD8278DB6C20DE ();
// 0x0000009E System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash::op_Equality(UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash,UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash)
extern void StringAndHash_op_Equality_m71F9668221E94FB4D0F4CC1937AEF990254ECC41 ();
// 0x0000009F System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash::op_Inequality(UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash,UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash)
extern void StringAndHash_op_Inequality_m1DC125C79EEBD27D10DCF55E5D2EA6DD5A4EC044 ();
// 0x000000A0 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash::Equals(System.Object)
extern void StringAndHash_Equals_mB26A324294ADB7490E328E4F65DCD527836ACA07 ();
// 0x000000A1 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash::Equals(UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash)
extern void StringAndHash_Equals_m10342D27925050B07D751B074286696DAE4CDDBE ();
// 0x000000A2 System.Int32 UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash::GetHashCode()
extern void StringAndHash_GetHashCode_mC7254FC624057FB49C626EC1B575BD36F492A4D7 ();
// 0x000000A3 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m41D29810DE38F7801194D5F03AC0D988748A946D ();
// 0x000000A4 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibrary_<>c__DisplayClass9_0::<GetCategoryNameFromHash>b__0(UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash)
extern void U3CU3Ec__DisplayClass9_0_U3CGetCategoryNameFromHashU3Eb__0_mF4ACA2786F8477EF08DBB31F888CF1CE87CADCD6 ();
// 0x000000A5 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m1ABB04EE569245812D3801DFC5CBDD90C4E5B016 ();
// 0x000000A6 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibrary_<>c__DisplayClass10_0::<GetLabelNameFromHash>b__0(UnityEngine.Experimental.U2D.Animation.SpriteLibrary_StringAndHash)
extern void U3CU3Ec__DisplayClass10_0_U3CGetLabelNameFromHashU3Eb__0_m5ECA3FC5F9E438173A1C6033BA86D02EE76B5828 ();
// 0x000000A7 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m2732F157206A971B2CD2ABA95A691EDEBA62AD3A ();
// 0x000000A8 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibrary_<>c__DisplayClass15_0::<AddOverride>b__0(UnityEngine.Experimental.U2D.Animation.SpriteLibCategory)
extern void U3CU3Ec__DisplayClass15_0_U3CAddOverrideU3Eb__0_mC15FD692B7274704A077F1CF91C957D891FD0F75 ();
// 0x000000A9 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibCategory_<>c::.cctor()
extern void U3CU3Ec__cctor_mAF805EF1AD298EE285F5284C796F57AC4BE470BE ();
// 0x000000AA System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibCategory_<>c::.ctor()
extern void U3CU3Ec__ctor_m24C48FE00997601FA519E0F982470D72115156EB ();
// 0x000000AB System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibCategory_<>c::<ValidateLabels>b__12_0(System.String,System.String)
extern void U3CU3Ec_U3CValidateLabelsU3Eb__12_0_m3A85B5ED58B4925B27C0522C8B2D719B2EFC2A52 ();
// 0x000000AC System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m92A89C44CF1690B4AB9A4EBDD7434695126B4392 ();
// 0x000000AD System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass4_0::<GetSprite>b__0(UnityEngine.Experimental.U2D.Animation.SpriteLibCategory)
extern void U3CU3Ec__DisplayClass4_0_U3CGetSpriteU3Eb__0_m8EFB5F3A6C826DA993D2C103FDFF8CDD8A65408C ();
// 0x000000AE System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass4_0::<GetSprite>b__1(UnityEngine.Experimental.U2D.Animation.Categorylabel)
extern void U3CU3Ec__DisplayClass4_0_U3CGetSpriteU3Eb__1_m31899303AE8ADF8D40BB3965A5FB840C1906775D ();
// 0x000000AF System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c::.cctor()
extern void U3CU3Ec__cctor_m76FF1ED333F11DDDB827E21CA7732B4808F78E2A ();
// 0x000000B0 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c::.ctor()
extern void U3CU3Ec__ctor_mCAF8EE4CAED719061DFDC4ACBCECB9BDA08C5C81 ();
// 0x000000B1 System.String UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c::<GetCategoryNames>b__7_0(UnityEngine.Experimental.U2D.Animation.SpriteLibCategory)
extern void U3CU3Ec_U3CGetCategoryNamesU3Eb__7_0_m247F8677458DCDB9ED236FC2F2A475DD60B0A2D5 ();
// 0x000000B2 System.String UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c::<GetCategoryLabelNames>b__9_1(UnityEngine.Experimental.U2D.Animation.Categorylabel)
extern void U3CU3Ec_U3CGetCategoryLabelNamesU3Eb__9_1_m7CCDC0FD9DA73308C10B7F8C290A8E23D19F7B5F ();
// 0x000000B3 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c::<ValidateCategories>b__15_0(System.String,System.String)
extern void U3CU3Ec_U3CValidateCategoriesU3Eb__15_0_m926F5EB46F0C6327B17D71DBCA84E441FAE34D3C ();
// 0x000000B4 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mDE1EA7CF9B591CADCDEDF82B88D9AA84EDDFA083 ();
// 0x000000B5 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass9_0::<GetCategoryLabelNames>b__0(UnityEngine.Experimental.U2D.Animation.SpriteLibCategory)
extern void U3CU3Ec__DisplayClass9_0_U3CGetCategoryLabelNamesU3Eb__0_m0A88ED47AC19ABC08A232EACD9FBE5455D97AF51 ();
// 0x000000B6 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mB08F02FC36A4E5431E45007C3A35EDDF36256DD6 ();
// 0x000000B7 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass10_0::<GetCategoryNameFromHash>b__0(UnityEngine.Experimental.U2D.Animation.SpriteLibCategory)
extern void U3CU3Ec__DisplayClass10_0_U3CGetCategoryNameFromHashU3Eb__0_m9B9276FBDDCCD4CC168A917F1B87087C92DA31BB ();
// 0x000000B8 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m03E1E56202CCDD870586C3D5EBA43368A6050D2B ();
// 0x000000B9 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass11_0::<AddCategoryLabel>b__0(UnityEngine.Experimental.U2D.Animation.SpriteLibCategory)
extern void U3CU3Ec__DisplayClass11_0_U3CAddCategoryLabelU3Eb__0_m5FF5571623DCFC0A0679F6A47FAC2936295B5D7F ();
// 0x000000BA System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass11_1::.ctor()
extern void U3CU3Ec__DisplayClass11_1__ctor_m1815A6493FBEC329BF3399E58BE1BA41886BF7F1 ();
// 0x000000BB System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass11_1::<AddCategoryLabel>b__1(UnityEngine.Experimental.U2D.Animation.Categorylabel)
extern void U3CU3Ec__DisplayClass11_1_U3CAddCategoryLabelU3Eb__1_m5D23192BC46EB47C6FD10EB2ADDA373BB9EEF61F ();
// 0x000000BC System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m421735E77D7A4F61B5673A8EDC16F62FAC6929B2 ();
// 0x000000BD System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass12_0::<RemoveCategoryLabel>b__0(UnityEngine.Experimental.U2D.Animation.SpriteLibCategory)
extern void U3CU3Ec__DisplayClass12_0_U3CRemoveCategoryLabelU3Eb__0_m6B829E7FC969288C31FEFABD81AA6D3BC1B0D9CC ();
// 0x000000BE System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass12_0::<RemoveCategoryLabel>b__2(UnityEngine.Experimental.U2D.Animation.SpriteLibCategory)
extern void U3CU3Ec__DisplayClass12_0_U3CRemoveCategoryLabelU3Eb__2_m0FCD2227B5B3637EE54B086817707F7EA0F0636A ();
// 0x000000BF System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass12_1::.ctor()
extern void U3CU3Ec__DisplayClass12_1__ctor_mEDE2145B35EBF9AA0D777C6FA8EEB3BB88934B52 ();
// 0x000000C0 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass12_1::<RemoveCategoryLabel>b__1(UnityEngine.Experimental.U2D.Animation.Categorylabel)
extern void U3CU3Ec__DisplayClass12_1_U3CRemoveCategoryLabelU3Eb__1_m4D4AFA76203589818C9B5DCC5BA2243FF25ADC75 ();
// 0x000000C1 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m7A92540AE881EE337D229FEBF7419DABFB315875 ();
// 0x000000C2 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass13_0::<GetLabelNameFromHash>b__0(UnityEngine.Experimental.U2D.Animation.SpriteLibCategory)
extern void U3CU3Ec__DisplayClass13_0_U3CGetLabelNameFromHashU3Eb__0_m7673DA79223C5B29766C13FF490CB440EF70A6FE ();
// 0x000000C3 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass13_0::<GetLabelNameFromHash>b__1(UnityEngine.Experimental.U2D.Animation.Categorylabel)
extern void U3CU3Ec__DisplayClass13_0_U3CGetLabelNameFromHashU3Eb__1_m0B364DFFBA518B04B26137C48CC4275A02FE10AB ();
// 0x000000C4 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m269698BEE3E17E45BCC8CFCA212BCBC6A7C4F5B6 ();
// 0x000000C5 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass16_0::<RenameDuplicate>b__0(UnityEngine.Experimental.U2D.Animation.INameHash)
extern void U3CU3Ec__DisplayClass16_0_U3CRenameDuplicateU3Eb__0_mC02DC97958FCB56E9A255B7407FE8CB045C43DF0 ();
// 0x000000C6 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass16_1::.ctor()
extern void U3CU3Ec__DisplayClass16_1__ctor_m01FC6D3316C0359EE42CC1FCE0616A1A66DDE21F ();
// 0x000000C7 System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass16_2::.ctor()
extern void U3CU3Ec__DisplayClass16_2__ctor_m7010F9D32777F50FCAA7EFB3EB1E0E9BD23A5776 ();
// 0x000000C8 System.Boolean UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset_<>c__DisplayClass16_2::<RenameDuplicate>b__1(UnityEngine.Experimental.U2D.Animation.INameHash)
extern void U3CU3Ec__DisplayClass16_2_U3CRenameDuplicateU3Eb__1_m0CDFD70CB14AC53E723E0A13332EBC83091B656C ();
// 0x000000C9 System.Void UnityEngine.U2D.Animation.SpriteSkinManager_SpriteSkinManagerInternal::.ctor()
extern void SpriteSkinManagerInternal__ctor_mC7D424E6088972AADA7FF31344FEB718450D3577 ();
static Il2CppMethodPointer s_methodPointers[201] = 
{
	SpriteLibrary_set_spriteLibraryAsset_mA0FEE070CA470C87B4B88344FF52F50F959D272D,
	SpriteLibrary_get_spriteLibraryAsset_m7BCCFB664DF26CE614C1E8D47ABBAF8C9267A047,
	SpriteLibrary_GetSprite_m9682FEED4B15AF5DAD098905B0EC06E63E4A26B9,
	SpriteLibrary_GetSprite_m34A399148F3F9155C653B465DF7EB9CE51C458CA,
	SpriteLibrary_GetSprite_m1F5EABC7E2641F05C12BE6DB357C66E6DB5BB162,
	SpriteLibrary_GetCategoryNameFromHash_m7B2985C1F203C863E8DB97E34455158F3A1119C1,
	SpriteLibrary_GetLabelNameFromHash_m7D60F0940CBEF2AE94427C29686172CBFDBDF962,
	SpriteLibrary_GetCategoryOverride_mF8C83E5448048A323850A849E364116C397D9BE1,
	SpriteLibrary_GetCategoryOverride_m81C559703764E5CB90CB62AABF7B42287C479C82,
	SpriteLibrary_AddSpriteToOverride_mC674D76EB4C8FFCC04B0EFF51E462D464E62826F,
	SpriteLibrary_AddOverride_m5C9F89B784815B4CD39BAFAAB2BE2EC78777C1B3,
	SpriteLibrary_AddOverride_mF2A2CC88048539F5968ADA55BEBA611540F5EA7B,
	SpriteLibrary_AddOverride_mF4EABE9FD82EE08677EB1009717C3695CCBD5FA6,
	SpriteLibrary_RemoveOverride_m1E46415C65A43E69F600D3D95E3D13C01BD045FD,
	SpriteLibrary_RemoveOverride_mEED0056A191D7ED22F574C86C96684643F1B08E9,
	SpriteLibrary_HasOverride_m909553DCBA9783B33683D5B3FCAF96D3A17928AF,
	SpriteLibrary_get_labels_mC82C744091883CE3861916497FB6C58CF33DA223,
	SpriteLibrary_RefreshSpriteResolvers_m41E0EB038F53CFAD4319127C1AF0CA8EF531515F,
	SpriteLibrary__ctor_m9E8692C6F9E370D360B763A30A1F4E1B8EB4AE3E,
	NULL,
	NULL,
	NULL,
	Categorylabel_get_name_m77E12FA9DCBE4DC689CCEB3FCD953E45648FDB2E,
	Categorylabel_set_name_m7C5C09B1AA94F04F0F44A5C22BB8A04342E44E9B,
	Categorylabel_get_hash_mC6A2629C13E9F3CBFF1DB7CD03228C4844366436,
	Categorylabel_get_sprite_m87F2393663AF2C0EBACD834CE86E4C0D513C6B2D,
	Categorylabel_set_sprite_m6E2EE0544770E8241DCCCF549DE0B2D88027228C,
	Categorylabel_UpdateHash_m7F3526FB329C0AE17B0209F6BD5C148F07BF1ED5,
	Categorylabel__ctor_m6C938CD36124AC60F4BE2ECC2BF023CDD571DB14,
	SpriteLibCategory_get_name_m6D7CD681165670EBC91E5EF539AB5E9E1A058C6B,
	SpriteLibCategory_set_name_m655A41BA7304CCBF0D6C895F32EBF2A14EB6DDC0,
	SpriteLibCategory_get_hash_mA6EC4331446F264E24734836F6E13F51441B91AB,
	SpriteLibCategory_get_categoryList_mDFD6DECFDD66673E4C69C6A9EF2F8E4D91979DAC,
	SpriteLibCategory_set_categoryList_mBEE534CD87140DEB2B704A4C174AACBFE37EFEFC,
	SpriteLibCategory_UpdateHash_mCCA764BEC65FC8BACD659DDC524BD968D2C52A8F,
	SpriteLibCategory_ValidateLabels_mBD4681B482F479DC56AA1DB8CC584824BA25C6F8,
	SpriteLibCategory__ctor_m534F2F5C8B783DD99C5263AB4432EF3167D0C334,
	SpriteLibraryAsset_get_categories_m4FF523E6F9FD12EA8E9B33DC26EDDF7F0856C3C3,
	SpriteLibraryAsset_set_categories_m30A285AD1BCCFBCB52B36168305293AA51FE3DE2,
	SpriteLibraryAsset_GetSprite_m69066DF6655A2207DAA2FE75E3CA1F6ACBC541FA,
	SpriteLibraryAsset_GetSprite_m080EBB15B251625433787ECBC12FEDDF15A5EA59,
	SpriteLibraryAsset_GetSprite_mA19217250650444187A9A61EA5616CEE7AC5C648,
	SpriteLibraryAsset_GetCategoryNames_mAC04EA9C21A5797C95B3F47E92D3BA6E824166CF,
	SpriteLibraryAsset_GetCategorylabelNames_m6C01E2E282DD50637BA08F54128127FF04BB40B5,
	SpriteLibraryAsset_GetCategoryLabelNames_mD95D19094468BBEDEC604043B8E8FAC90E5F2126,
	SpriteLibraryAsset_GetCategoryNameFromHash_m45E58B32C998A6943D0844584D1F6CD3F0373873,
	SpriteLibraryAsset_AddCategoryLabel_mAE6805375A45E969092AFC08E547C0EDFEC303BC,
	SpriteLibraryAsset_RemoveCategoryLabel_mC1809CB5762381CF72912E631D8BD7974CCCE20F,
	SpriteLibraryAsset_GetLabelNameFromHash_m7C303D933841F22E6E12E7635424832BFA6100BC,
	SpriteLibraryAsset_UpdateHashes_m7499B3104717798437ABEEE66C747EE946473904,
	SpriteLibraryAsset_ValidateCategories_m10507DB04448F9BD0AD9957B016CF24ABE7F7BFE,
	SpriteLibraryAsset_RenameDuplicate_mC152BC315A5BD967F2E58F6804C092638ADD7DC4,
	SpriteLibraryAsset_Default_GetStringHash_mEEA523A2B020CE627D5BE6DF0808BBD94F6881EC,
	SpriteLibraryAsset__ctor_mCD67168CD348AA1FBD954B94D7F0ED6B5B711CC9,
	SpriteLibraryAsset__cctor_mAE5ED49F14F61076BFE7BE390EEAF08ECBB0D1D9,
	SpriteResolver_OnEnable_mEBC33FDD853902157CBD8A2762DD2D6F2457D983,
	SpriteResolver_get_spriteRenderer_m2F385D8D6F09C2493CE28862FF523A9E9A7F5208,
	SpriteResolver_SetCategoryAndLabel_m5C74BC88070E6CB13E883A6F5A8221D390BC7FC8,
	SpriteResolver_GetCategory_mB4F887D8124D040B0975EAEE99DD803B2AD9EF14,
	SpriteResolver_GetLabel_m5B57515D2C2B5FE4242B350DB00D02A59444B2CF,
	SpriteResolver_get_spriteLibrary_mF5B27288A55774226484557F7B00DBEA4362DBAA,
	SpriteResolver_LateUpdate_m72BA76C1CAE96EB77EA42EF2E5171CC4295AD164,
	SpriteResolver_GetSprite_mC48A6ABB24F0D99F36F2A47B1785B030300995D5,
	SpriteResolver_ResolveSpriteToSpriteRenderer_m51041C425DC96F34D2F4F74AE92B3B78279E2EFA,
	SpriteResolver_OnTransformParentChanged_m1268B5B949D5515D11E95367111635D88FEB02B7,
	SpriteResolver_get_categoryHashInt_mAC6D8CEE85C8C2D4F3FD97210B85BA41446E8A85,
	SpriteResolver_set_categoryHashInt_mDBC93AC493A0FB028220DE206F7F82886E005DF6,
	SpriteResolver_get_labelHashInt_m368982B2740B6075512349B58E862ACEEF6A3ECE,
	SpriteResolver_set_labelHashInt_mF1C14B48E4BB28DCBE809DEFC85092A145DFB4BA,
	SpriteResolver_ConvertFloatToInt_mFD242FE17F00FEEDBC6098702C487EB6F7481DA6,
	SpriteResolver_ConvertIntToFloat_mDF7459E8F9E0DA2A4F6F9BF37CDB3585A8101642,
	SpriteResolver__ctor_m8BF156B2B1A4FE23822E4BB4E2AEAB1C10D0D6D9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DeformVerticesBuffer__ctor_mC9B318B4FFD14AC9353A01E1EDA611C71FC17751_AdjustorThunk,
	DeformVerticesBuffer_Dispose_mE2222F5C6C1C013BBBC9A22C77B768C522E5175B_AdjustorThunk,
	DeformVerticesBuffer_GetBuffer_m3AA5A45F38094C05FB8B4247A19B086A2879C23B_AdjustorThunk,
	DeformVerticesBuffer_GetCurrentBuffer_m3A7BD4BEE612BAEF6A5E1A12C9BBAABBEAB5C878_AdjustorThunk,
	SpriteSkin_get_batchSkinning_mC737C08514346C43DE836606C7C461BFD72B3E37,
	SpriteSkin_set_batchSkinning_m9AF365F2F0BDA385CC0E1BDA11AC3418CFE494F9,
	SpriteSkin_GetSpriteInstanceID_mDCDE405C695CB58B8351B4430CE81F9578643A2E,
	SpriteSkin_OnEnable_m08D1FD40FDD317DB64ABDE00485A71994071DA6F,
	SpriteSkin_CacheValidFlag_mA0FBC307286CE6EB143E0BBBA734FE8575089CB6,
	SpriteSkin_Reset_m53994906194BC69065CF9014B199CDFB7E76823D,
	SpriteSkin_UseBatching_mEC2BD48461983D6971D9AB20E9C58DD73DF2D199,
	SpriteSkin_GetDeformedVertices_m01E711868ED3F0F7702F279B113CA3B044796CD9,
	SpriteSkin_HasCurrentDeformedVertices_mD745AE94F89889CD552C7A999C64C299D39A5B78,
	SpriteSkin_GetCurrentDeformedVertices_m8F8EBBE7EE938415C9B0DB00DF39C8D91D53E0A0,
	SpriteSkin_GetCurrentDeformedVertexPositions_m46403BEA09111D5CCCF5D6440F5EAFBBE9747901,
	SpriteSkin_GetCurrentDeformedVertexPositionsAndTangents_mC0EA62325BB61E327808E4BD82D5EC056FC809D6,
	SpriteSkin_GetDeformedVertexPositionData_m89448503FE097E50BBA8F27F971823ED72D45A40,
	SpriteSkin_GetDeformedVertexTangentData_m816731F0ED83B51D05E935D79052C073103E482C,
	SpriteSkin_OnDisable_m04F5BAD3906AD83E24FF9D2B3CA9FF8BD0395414,
	SpriteSkin_LateUpdate_mA3EE66458E56228A4DF2DCB752F73915D497A42C,
	SpriteSkin_CacheCurrentSprite_m79653521A99F2DBA4680F0EC742007EC96CCF2F4,
	SpriteSkin_get_sprite_m6AE7F0414180DBED689CB55A2078AB694F11B7E3,
	SpriteSkin_get_spriteRenderer_m82F94ED567A37F8EACBA9B853F45B78CDBDBF945,
	SpriteSkin_get_boneTransforms_m183C0E96FABE7B91545847416AA86066373D35C8,
	SpriteSkin_set_boneTransforms_m12AD63B0C8C41D92E50E51427029E561A1E00B03,
	SpriteSkin_get_rootBone_m41EF7EF107941B23B870FA57B415A663511CCE28,
	SpriteSkin_set_rootBone_m5418FBC215563D71A1AB693AAF2FCBADD28447F7,
	SpriteSkin_get_bounds_mBD2295F3506D7E4275551DF6D4ED104AE905F7FF,
	SpriteSkin_set_bounds_m00A9C0006889F784E671A812DB50D2CCFD836A1C,
	SpriteSkin_get_alwaysUpdate_m3CFC011C782A14D6AB8BFCDE192665AEA9A36DE0,
	SpriteSkin_set_alwaysUpdate_mC8A775AD29A19B97AF9C5B30E2EC77683E93DE4A,
	SpriteSkin_get_isValid_m82C5221C64A235734D9F2A2AEFF1B16E8B412CCF,
	SpriteSkin_OnDestroy_mC081F8C17D696D38916E594DF893C2A9B1DB62B6,
	SpriteSkin_DeactivateSkinning_mFA5F6C270585C1C093FCBC51EC06D94FEC030245,
	SpriteSkin_ResetSprite_mEC1A5630E2E302E292790253D5016263A8C39770,
	SpriteSkin_OnBeforeSerialize_m1C2A4F79DA2D358A16EB77D7BD315A2C7B75C336,
	SpriteSkin_OnAfterDeserialize_mE881E52D0591FAA7646063BAC21016087B8D0D94,
	SpriteSkin_OnEnableBatch_m98ECFE4A60FB5F600D9CFEF85A7BEF9B76E1B133,
	SpriteSkin_UpdateSpriteDeform_m71D0D44ED98E197E87ED2F2DB827F2955D58BC76,
	SpriteSkin_OnResetBatch_m103F0EFDC15B728FA34ED8D38DA1FB32EFD02052,
	SpriteSkin_UseBatchingBatch_mA294DAC76182455D472F7A9D513C2FAB533DD760,
	SpriteSkin_OnDisableBatch_mAD4BD1C1F3100FEFE93A7F02DDF610364A1955C3,
	SpriteSkin_OnBoneTransformChanged_mA98F985F4FFC490ED4BA5B59BC7AD71014CEDDA9,
	SpriteSkin_OnRootBoneTransformChanged_mD3E9A0C2D817897FE8A3F75BF00D34D496B80EB5,
	SpriteSkin_OnBeforeSerializeBatch_m11E59A0DD2ED20B230DCF132A67E35D0B58FD5A1,
	SpriteSkin_OnAfterSerializeBatch_m25FDCEE4CD07869CBFB2E02733999FE77747FCD5,
	SpriteSkin__ctor_mBE9343783A2E6D17C1805B81EE3F5AACFCA7EAF8,
	SpriteSkinEntity__ctor_mDDFFEC4F9F8DCC59323237F8A64D248A1476D967,
	SpriteSkinManager__ctor_mA17787B1FA7F7F49541CA2ABF89FE233432A5E9A,
	SpriteSkinUtility_Validate_m71FEB7305CE1363B9A0C2AA3A61A3ECBAB81D268,
	SpriteSkinUtility_CreateBoneHierarchy_m8E094C6FC8320B23535470382E1D55DB0723BE88,
	SpriteSkinUtility_GetVertexStreamSize_m10CAAE580EB073958C3303B642AD7486BF356AD5,
	SpriteSkinUtility_GetVertexStreamOffset_m2880DAA60A443378FD86E06801AB65E28456FF5D,
	SpriteSkinUtility_CreateGameObject_mE75B48A0B298E1C5177C529BAEA01140B7782B22,
	SpriteSkinUtility_ResetBindPose_m79C549EB6CB1B7E7C4B7FC0E7AE4AB46E537536C,
	SpriteSkinUtility_Rebind_m022E4AC07A834AD2C75A5AC00067AD228025220F,
	SpriteSkinUtility_CalculateBoneTransformPath_mE8B00C2441F0DE56A40525FCB08FA8B82AB32ABD,
	SpriteSkinUtility_GetHash_m5577396126C61F69FA6B857653C8BE886D4FD05E,
	SpriteSkinUtility_CalculateTransformHash_m46EC0AC109953771CC9448AB399F6C8EF00B3938,
	SpriteSkinUtility_Deform_m981222CD3C07F2AB53BCD093ABE83C66C8D645A9,
	SpriteSkinUtility_Deform_mD9D8828D52CAB0155A364F2E1788DF9ECBE3E7A7,
	SpriteSkinUtility_Deform_m69ECADA90DB5A147A3C038B76EAF157F47D9B56C,
	SpriteSkinUtility_Deform_mA57D36C6204C389924701F65D49C524A28F88576,
	SpriteSkinUtility_Bake_m6EA711E8B02646A1C3E7EBE382E7A83A265A06AE,
	SpriteSkinUtility_CalculateBounds_mBEE2B66B9DA73087C7FB4B0A437E6F2B8F790833,
	SpriteSkinUtility_CalculateSpriteSkinBounds_mAD27C6FFE5E6DBBCDC623B3512F0C4EEE44BFF0D,
	SpriteSkinUtility_UpdateBounds_m378984E6D634AC1F47E946BDA367B634DFD83486,
	StringAndHash__ctor_m20980FA2D7D88F09C954E08B175D39EC589AD158,
	StringAndHash__ctor_m3AF9AF9296888D8F0284CE4256BD8278DB6C20DE,
	StringAndHash_op_Equality_m71F9668221E94FB4D0F4CC1937AEF990254ECC41,
	StringAndHash_op_Inequality_m1DC125C79EEBD27D10DCF55E5D2EA6DD5A4EC044,
	StringAndHash_Equals_mB26A324294ADB7490E328E4F65DCD527836ACA07,
	StringAndHash_Equals_m10342D27925050B07D751B074286696DAE4CDDBE,
	StringAndHash_GetHashCode_mC7254FC624057FB49C626EC1B575BD36F492A4D7,
	U3CU3Ec__DisplayClass9_0__ctor_m41D29810DE38F7801194D5F03AC0D988748A946D,
	U3CU3Ec__DisplayClass9_0_U3CGetCategoryNameFromHashU3Eb__0_mF4ACA2786F8477EF08DBB31F888CF1CE87CADCD6,
	U3CU3Ec__DisplayClass10_0__ctor_m1ABB04EE569245812D3801DFC5CBDD90C4E5B016,
	U3CU3Ec__DisplayClass10_0_U3CGetLabelNameFromHashU3Eb__0_m5ECA3FC5F9E438173A1C6033BA86D02EE76B5828,
	U3CU3Ec__DisplayClass15_0__ctor_m2732F157206A971B2CD2ABA95A691EDEBA62AD3A,
	U3CU3Ec__DisplayClass15_0_U3CAddOverrideU3Eb__0_mC15FD692B7274704A077F1CF91C957D891FD0F75,
	U3CU3Ec__cctor_mAF805EF1AD298EE285F5284C796F57AC4BE470BE,
	U3CU3Ec__ctor_m24C48FE00997601FA519E0F982470D72115156EB,
	U3CU3Ec_U3CValidateLabelsU3Eb__12_0_m3A85B5ED58B4925B27C0522C8B2D719B2EFC2A52,
	U3CU3Ec__DisplayClass4_0__ctor_m92A89C44CF1690B4AB9A4EBDD7434695126B4392,
	U3CU3Ec__DisplayClass4_0_U3CGetSpriteU3Eb__0_m8EFB5F3A6C826DA993D2C103FDFF8CDD8A65408C,
	U3CU3Ec__DisplayClass4_0_U3CGetSpriteU3Eb__1_m31899303AE8ADF8D40BB3965A5FB840C1906775D,
	U3CU3Ec__cctor_m76FF1ED333F11DDDB827E21CA7732B4808F78E2A,
	U3CU3Ec__ctor_mCAF8EE4CAED719061DFDC4ACBCECB9BDA08C5C81,
	U3CU3Ec_U3CGetCategoryNamesU3Eb__7_0_m247F8677458DCDB9ED236FC2F2A475DD60B0A2D5,
	U3CU3Ec_U3CGetCategoryLabelNamesU3Eb__9_1_m7CCDC0FD9DA73308C10B7F8C290A8E23D19F7B5F,
	U3CU3Ec_U3CValidateCategoriesU3Eb__15_0_m926F5EB46F0C6327B17D71DBCA84E441FAE34D3C,
	U3CU3Ec__DisplayClass9_0__ctor_mDE1EA7CF9B591CADCDEDF82B88D9AA84EDDFA083,
	U3CU3Ec__DisplayClass9_0_U3CGetCategoryLabelNamesU3Eb__0_m0A88ED47AC19ABC08A232EACD9FBE5455D97AF51,
	U3CU3Ec__DisplayClass10_0__ctor_mB08F02FC36A4E5431E45007C3A35EDDF36256DD6,
	U3CU3Ec__DisplayClass10_0_U3CGetCategoryNameFromHashU3Eb__0_m9B9276FBDDCCD4CC168A917F1B87087C92DA31BB,
	U3CU3Ec__DisplayClass11_0__ctor_m03E1E56202CCDD870586C3D5EBA43368A6050D2B,
	U3CU3Ec__DisplayClass11_0_U3CAddCategoryLabelU3Eb__0_m5FF5571623DCFC0A0679F6A47FAC2936295B5D7F,
	U3CU3Ec__DisplayClass11_1__ctor_m1815A6493FBEC329BF3399E58BE1BA41886BF7F1,
	U3CU3Ec__DisplayClass11_1_U3CAddCategoryLabelU3Eb__1_m5D23192BC46EB47C6FD10EB2ADDA373BB9EEF61F,
	U3CU3Ec__DisplayClass12_0__ctor_m421735E77D7A4F61B5673A8EDC16F62FAC6929B2,
	U3CU3Ec__DisplayClass12_0_U3CRemoveCategoryLabelU3Eb__0_m6B829E7FC969288C31FEFABD81AA6D3BC1B0D9CC,
	U3CU3Ec__DisplayClass12_0_U3CRemoveCategoryLabelU3Eb__2_m0FCD2227B5B3637EE54B086817707F7EA0F0636A,
	U3CU3Ec__DisplayClass12_1__ctor_mEDE2145B35EBF9AA0D777C6FA8EEB3BB88934B52,
	U3CU3Ec__DisplayClass12_1_U3CRemoveCategoryLabelU3Eb__1_m4D4AFA76203589818C9B5DCC5BA2243FF25ADC75,
	U3CU3Ec__DisplayClass13_0__ctor_m7A92540AE881EE337D229FEBF7419DABFB315875,
	U3CU3Ec__DisplayClass13_0_U3CGetLabelNameFromHashU3Eb__0_m7673DA79223C5B29766C13FF490CB440EF70A6FE,
	U3CU3Ec__DisplayClass13_0_U3CGetLabelNameFromHashU3Eb__1_m0B364DFFBA518B04B26137C48CC4275A02FE10AB,
	U3CU3Ec__DisplayClass16_0__ctor_m269698BEE3E17E45BCC8CFCA212BCBC6A7C4F5B6,
	U3CU3Ec__DisplayClass16_0_U3CRenameDuplicateU3Eb__0_mC02DC97958FCB56E9A255B7407FE8CB045C43DF0,
	U3CU3Ec__DisplayClass16_1__ctor_m01FC6D3316C0359EE42CC1FCE0616A1A66DDE21F,
	U3CU3Ec__DisplayClass16_2__ctor_m7010F9D32777F50FCAA7EFB3EB1E0E9BD23A5776,
	U3CU3Ec__DisplayClass16_2_U3CRenameDuplicateU3Eb__1_m0CDFD70CB14AC53E723E0A13332EBC83091B656C,
	SpriteSkinManagerInternal__ctor_mC7D424E6088972AADA7FF31344FEB718450D3577,
};
static const int32_t s_InvokerIndices[201] = 
{
	26,
	14,
	113,
	163,
	961,
	34,
	163,
	112,
	112,
	171,
	171,
	27,
	171,
	26,
	27,
	115,
	14,
	23,
	23,
	14,
	26,
	10,
	14,
	26,
	10,
	14,
	26,
	23,
	23,
	14,
	26,
	10,
	14,
	26,
	23,
	23,
	23,
	14,
	26,
	163,
	961,
	113,
	14,
	28,
	28,
	34,
	171,
	110,
	163,
	23,
	23,
	134,
	95,
	23,
	3,
	23,
	14,
	27,
	14,
	14,
	14,
	23,
	453,
	23,
	23,
	10,
	32,
	10,
	32,
	231,
	244,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	2305,
	8,
	114,
	31,
	10,
	23,
	23,
	23,
	31,
	2305,
	114,
	2306,
	2307,
	2308,
	14,
	14,
	23,
	23,
	23,
	14,
	14,
	14,
	26,
	14,
	26,
	1175,
	1198,
	114,
	31,
	114,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	95,
	122,
	95,
	185,
	2309,
	122,
	122,
	263,
	2310,
	95,
	2311,
	2312,
	2313,
	2314,
	353,
	122,
	2315,
	1367,
	26,
	32,
	111,
	111,
	9,
	9,
	10,
	23,
	9,
	23,
	9,
	23,
	9,
	3,
	23,
	27,
	23,
	9,
	9,
	3,
	23,
	28,
	28,
	27,
	23,
	9,
	23,
	9,
	23,
	9,
	23,
	9,
	23,
	9,
	9,
	23,
	9,
	23,
	9,
	9,
	23,
	9,
	23,
	23,
	9,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x02000009, { 11, 4 } },
	{ 0x0200000A, { 15, 8 } },
	{ 0x06000049, { 0, 5 } },
	{ 0x0600004A, { 5, 2 } },
	{ 0x0600004B, { 7, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[23] = 
{
	{ (Il2CppRGCTXDataType)3, 21627 },
	{ (Il2CppRGCTXDataType)3, 21628 },
	{ (Il2CppRGCTXDataType)3, 21629 },
	{ (Il2CppRGCTXDataType)2, 24419 },
	{ (Il2CppRGCTXDataType)3, 21630 },
	{ (Il2CppRGCTXDataType)3, 21631 },
	{ (Il2CppRGCTXDataType)3, 21632 },
	{ (Il2CppRGCTXDataType)3, 21633 },
	{ (Il2CppRGCTXDataType)3, 21634 },
	{ (Il2CppRGCTXDataType)3, 21635 },
	{ (Il2CppRGCTXDataType)3, 21636 },
	{ (Il2CppRGCTXDataType)3, 21637 },
	{ (Il2CppRGCTXDataType)3, 21638 },
	{ (Il2CppRGCTXDataType)3, 21639 },
	{ (Il2CppRGCTXDataType)3, 21640 },
	{ (Il2CppRGCTXDataType)2, 26221 },
	{ (Il2CppRGCTXDataType)3, 21641 },
	{ (Il2CppRGCTXDataType)3, 21642 },
	{ (Il2CppRGCTXDataType)2, 26222 },
	{ (Il2CppRGCTXDataType)3, 21643 },
	{ (Il2CppRGCTXDataType)3, 21644 },
	{ (Il2CppRGCTXDataType)3, 21645 },
	{ (Il2CppRGCTXDataType)2, 24436 },
};
extern const Il2CppCodeGenModule g_Unity_2D_Animation_RuntimeCodeGenModule;
const Il2CppCodeGenModule g_Unity_2D_Animation_RuntimeCodeGenModule = 
{
	"Unity.2D.Animation.Runtime.dll",
	201,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	5,
	s_rgctxIndices,
	23,
	s_rgctxValues,
	NULL,
};
