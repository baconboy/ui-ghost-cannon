﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.U2D.Common.InternalEngineBridge::SetLocalAABB(UnityEngine.SpriteRenderer,UnityEngine.Bounds)
extern void InternalEngineBridge_SetLocalAABB_m32F522B56492AE6927553CEB1869D99D53F2A1DE ();
// 0x00000002 System.Void UnityEngine.U2D.Common.InternalEngineBridge::SetDeformableBuffer(UnityEngine.SpriteRenderer,Unity.Collections.NativeArray`1<System.Byte>)
extern void InternalEngineBridge_SetDeformableBuffer_m9F5CBE85D001DE72C109D0D7B4958B3076ECFC9A ();
static Il2CppMethodPointer s_methodPointers[2] = 
{
	InternalEngineBridge_SetLocalAABB_m32F522B56492AE6927553CEB1869D99D53F2A1DE,
	InternalEngineBridge_SetDeformableBuffer_m9F5CBE85D001DE72C109D0D7B4958B3076ECFC9A,
};
static const int32_t s_InvokerIndices[2] = 
{
	1364,
	1363,
};
extern const Il2CppCodeGenModule g_Unity_InternalAPIEngineBridge_001CodeGenModule;
const Il2CppCodeGenModule g_Unity_InternalAPIEngineBridge_001CodeGenModule = 
{
	"Unity.InternalAPIEngineBridge.001.dll",
	2,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
