﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.IFormatProvider
struct IFormatProvider_t4247E13AE2D97A079B88D594B7ABABF313259901;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;

IL2CPP_EXTERN_C RuntimeClass* Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral2211E2D567B9991441FFEFBE5AAA03456AB8ABCA;
IL2CPP_EXTERN_C String_t* _stringLiteral5DBD030B94725EF92B6F4BDDEB5A06DB312AF764;
IL2CPP_EXTERN_C String_t* _stringLiteral782960A2A1A5CFC6AA819F3CC53C89E5F0641539;
IL2CPP_EXTERN_C String_t* _stringLiteral901D0D2D30D5C7F19A6CF4A9C689EBAFE3B3FB54;
IL2CPP_EXTERN_C String_t* _stringLiteralDF02D7418655748837E0CFA19E962079704EDA2F;
IL2CPP_EXTERN_C const uint32_t float3_Equals_m2B1D30B076B6597B9F40527025211F33E8EC9145_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float3_ToString_m513168CE73E3A1943FBF999282E27D81DB726E1CUnity_Mathematics_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float3_ToString_m513168CE73E3A1943FBF999282E27D81DB726E1C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float3_ToString_mB4863B6E2512E53544B02772705EBF221D744416Unity_Mathematics_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float3_ToString_mB4863B6E2512E53544B02772705EBF221D744416_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float4_Equals_m6E45C7C37F14AD55CF58E8C81A570A7035C33987_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float4_ToString_m8D534BBA43D7804CD31E26FE4C1972284DE35FBCUnity_Mathematics_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float4_ToString_m8D534BBA43D7804CD31E26FE4C1972284DE35FBC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float4_ToString_m92F4A00D57F53DC94183F09F72ED6CAC1F390650Unity_Mathematics_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float4_ToString_m92F4A00D57F53DC94183F09F72ED6CAC1F390650_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float4x4_Equals_m2E4E7CA61E037ECF3FCFF3CDF4A454F2DA1DD202_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float4x4_ToString_m713F20FCA71B96C1A0C98544E066CA6552EB6DA6Unity_Mathematics_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float4x4_ToString_m713F20FCA71B96C1A0C98544E066CA6552EB6DA6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float4x4_ToString_m782366095AD7C5C4144B9A7EE0113328AAEC908BUnity_Mathematics_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float4x4_ToString_m782366095AD7C5C4144B9A7EE0113328AAEC908B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t float4x4__cctor_mFA9894818878962653AD1F8C490BF8B0D1E5F7EF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t math_sqrt_mFFB10D250782D3B931727A664F421D1061A8F76B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t uint3_Equals_mD0F46E10E9BBF31279855DAFE2325C6038C0E6D3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t uint3_ToString_m02FAB1C4D7D9A4831F7D0D78AF975B8C4B7EF4FDUnity_Mathematics_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t uint3_ToString_m02FAB1C4D7D9A4831F7D0D78AF975B8C4B7EF4FD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t uint3_ToString_m7705E589BDC0FCC9D87428DEFD95A66EF4A7A6AFUnity_Mathematics_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t uint3_ToString_m7705E589BDC0FCC9D87428DEFD95A66EF4A7A6AF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t uint4_Equals_mA3312E8FC1A4734E2402424ADAC7BCE386918488_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t uint4_ToString_m1ADFBF43F0EC44A59D7F9908C9891DC3C31690BCUnity_Mathematics_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t uint4_ToString_m1ADFBF43F0EC44A59D7F9908C9891DC3C31690BC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t uint4_ToString_mED39980F32C81D913660EC644111F2070E7870A2Unity_Mathematics_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t uint4_ToString_mED39980F32C81D913660EC644111F2070E7870A2_MetadataUsageId;

struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t5169D1AAD059D79561E32000F03E47724D94C68A 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// Unity.Mathematics.float3_DebuggerProxy
struct  DebuggerProxy_tE712E91E520C492D24FAA53E84FDB1EF96F16A57  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.float4_DebuggerProxy
struct  DebuggerProxy_t7E30432489A8A246A285C8E57CFD4F27A2A5001A  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.math
struct  math_tA6CA6C6033EA580128C811BD06C8777AE1E241ED  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.uint3_DebuggerProxy
struct  DebuggerProxy_tD7FDBA1E16900013590CEBDAB926FE48B12F0193  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.uint4_DebuggerProxy
struct  DebuggerProxy_t8C2E7867F9521559315ED276521C02382BC22C03  : public RuntimeObject
{
public:

public:
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// Unity.Mathematics.float3
struct  float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 
{
public:
	// System.Single Unity.Mathematics.float3::x
	float ___x_0;
	// System.Single Unity.Mathematics.float3::y
	float ___y_1;
	// System.Single Unity.Mathematics.float3::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};


// Unity.Mathematics.float4
struct  float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A 
{
public:
	// System.Single Unity.Mathematics.float4::x
	float ___x_0;
	// System.Single Unity.Mathematics.float4::y
	float ___y_1;
	// System.Single Unity.Mathematics.float4::z
	float ___z_2;
	// System.Single Unity.Mathematics.float4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};


// Unity.Mathematics.math_IntFloatUnion
struct  IntFloatUnion_t7ECB2A03CE51B658B77F53B39547C3BDB8164CD7 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 Unity.Mathematics.math_IntFloatUnion::intValue
			int32_t ___intValue_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___intValue_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single Unity.Mathematics.math_IntFloatUnion::floatValue
			float ___floatValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___floatValue_1_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_intValue_0() { return static_cast<int32_t>(offsetof(IntFloatUnion_t7ECB2A03CE51B658B77F53B39547C3BDB8164CD7, ___intValue_0)); }
	inline int32_t get_intValue_0() const { return ___intValue_0; }
	inline int32_t* get_address_of_intValue_0() { return &___intValue_0; }
	inline void set_intValue_0(int32_t value)
	{
		___intValue_0 = value;
	}

	inline static int32_t get_offset_of_floatValue_1() { return static_cast<int32_t>(offsetof(IntFloatUnion_t7ECB2A03CE51B658B77F53B39547C3BDB8164CD7, ___floatValue_1)); }
	inline float get_floatValue_1() const { return ___floatValue_1; }
	inline float* get_address_of_floatValue_1() { return &___floatValue_1; }
	inline void set_floatValue_1(float value)
	{
		___floatValue_1 = value;
	}
};


// Unity.Mathematics.uint3
struct  uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F 
{
public:
	// System.UInt32 Unity.Mathematics.uint3::x
	uint32_t ___x_0;
	// System.UInt32 Unity.Mathematics.uint3::y
	uint32_t ___y_1;
	// System.UInt32 Unity.Mathematics.uint3::z
	uint32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F, ___x_0)); }
	inline uint32_t get_x_0() const { return ___x_0; }
	inline uint32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(uint32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F, ___y_1)); }
	inline uint32_t get_y_1() const { return ___y_1; }
	inline uint32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(uint32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F, ___z_2)); }
	inline uint32_t get_z_2() const { return ___z_2; }
	inline uint32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(uint32_t value)
	{
		___z_2 = value;
	}
};


// Unity.Mathematics.uint4
struct  uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC 
{
public:
	// System.UInt32 Unity.Mathematics.uint4::x
	uint32_t ___x_0;
	// System.UInt32 Unity.Mathematics.uint4::y
	uint32_t ___y_1;
	// System.UInt32 Unity.Mathematics.uint4::z
	uint32_t ___z_2;
	// System.UInt32 Unity.Mathematics.uint4::w
	uint32_t ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC, ___x_0)); }
	inline uint32_t get_x_0() const { return ___x_0; }
	inline uint32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(uint32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC, ___y_1)); }
	inline uint32_t get_y_1() const { return ___y_1; }
	inline uint32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(uint32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC, ___z_2)); }
	inline uint32_t get_z_2() const { return ___z_2; }
	inline uint32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(uint32_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC, ___w_3)); }
	inline uint32_t get_w_3() const { return ___w_3; }
	inline uint32_t* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(uint32_t value)
	{
		___w_3 = value;
	}
};


// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// Unity.Mathematics.float4x4
struct  float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC 
{
public:
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c0
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c0_0;
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c1
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c1_1;
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c2
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c2_2;
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c3
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c3_3;

public:
	inline static int32_t get_offset_of_c0_0() { return static_cast<int32_t>(offsetof(float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC, ___c0_0)); }
	inline float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  get_c0_0() const { return ___c0_0; }
	inline float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * get_address_of_c0_0() { return &___c0_0; }
	inline void set_c0_0(float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  value)
	{
		___c0_0 = value;
	}

	inline static int32_t get_offset_of_c1_1() { return static_cast<int32_t>(offsetof(float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC, ___c1_1)); }
	inline float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  get_c1_1() const { return ___c1_1; }
	inline float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * get_address_of_c1_1() { return &___c1_1; }
	inline void set_c1_1(float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  value)
	{
		___c1_1 = value;
	}

	inline static int32_t get_offset_of_c2_2() { return static_cast<int32_t>(offsetof(float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC, ___c2_2)); }
	inline float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  get_c2_2() const { return ___c2_2; }
	inline float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * get_address_of_c2_2() { return &___c2_2; }
	inline void set_c2_2(float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  value)
	{
		___c2_2 = value;
	}

	inline static int32_t get_offset_of_c3_3() { return static_cast<int32_t>(offsetof(float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC, ___c3_3)); }
	inline float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  get_c3_3() const { return ___c3_3; }
	inline float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * get_address_of_c3_3() { return &___c3_3; }
	inline void set_c3_3(float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  value)
	{
		___c3_3 = value;
	}
};

struct float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC_StaticFields
{
public:
	// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::identity
	float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___identity_4;

public:
	inline static int32_t get_offset_of_identity_4() { return static_cast<int32_t>(offsetof(float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC_StaticFields, ___identity_4)); }
	inline float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  get_identity_4() const { return ___identity_4; }
	inline float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * get_address_of_identity_4() { return &___identity_4; }
	inline void set_identity_4(float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  value)
	{
		___identity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void Unity.Mathematics.float3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float3::Equals(Unity.Mathematics.float3)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool float3_Equals_mD9E67316EB63B276A4E7D47E0DA25377FC21C129_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float3_Equals_m2B1D30B076B6597B9F40527025211F33E8EC9145 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float3)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_hash_m2502AF91810C2607571B2000ECA8A7370055FEF5_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.float3::GetHashCode()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t float3_GetHashCode_mA8A92E15BE1FE55D94575007DAF37C6AB9379C24_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m26BBF75F9609FAD0B39C2242FEBAAD7D68F14D99 (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, RuntimeObject * ___arg23, const RuntimeMethod* method);
// System.String Unity.Mathematics.float3::ToString()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float3_ToString_mB4863B6E2512E53544B02772705EBF221D744416_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, const RuntimeMethod* method);
// System.String System.Single::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756 (float* __this, String_t* ___format0, RuntimeObject* ___provider1, const RuntimeMethod* method);
// System.String Unity.Mathematics.float3::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float3_ToString_m513168CE73E3A1943FBF999282E27D81DB726E1C_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float3,System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void float4__ctor_mC873E36671C90EFA5B0F4513EEA6D79B0E4D4333_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___xyz0, float ___w1, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float4::get_xyz()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  float4_get_xyz_mC5FD59B33F4C3F7DA22E8C0F0B1FFC2C38A1F8DB_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float4::Equals(Unity.Mathematics.float4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4_Equals_m6E45C7C37F14AD55CF58E8C81A570A7035C33987 (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_hash_m922609CC35709A98A21766DAB7DD2B0978C5D104_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.float4::GetHashCode()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t float4_GetHashCode_m9510EAC95094BD49DF7CF63937B46669614AB277_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865 (String_t* ___format0, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args1, const RuntimeMethod* method);
// System.String Unity.Mathematics.float4::ToString()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float4_ToString_m8D534BBA43D7804CD31E26FE4C1972284DE35FBC_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.float4::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float4_ToString_m92F4A00D57F53DC94183F09F72ED6CAC1F390650_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4x4::.ctor(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void float4x4__ctor_m9A2D24D07FF8DCB7495AC3FD76364FD3FB81D807_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c00, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c11, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c22, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c33, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4x4::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void float4x4__ctor_m9C451E986E9271E34E011C51F35160C8E9E6CF98_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, float ___m000, float ___m011, float ___m022, float ___m033, float ___m104, float ___m115, float ___m126, float ___m137, float ___m208, float ___m219, float ___m2210, float ___m2311, float ___m3012, float ___m3113, float ___m3214, float ___m3315, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float4x4::Equals(Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool float4x4_Equals_m78EF9656D285326360D7957143A42017BE85AD96_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float4x4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4x4_Equals_m2E4E7CA61E037ECF3FCFF3CDF4A454F2DA1DD202 (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_hash_m8E61D111AA597992FA0D8C6D5FBE3FBC7B417B25_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.float4x4::GetHashCode()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t float4x4_GetHashCode_m35D91773F446B0D620E5B5E04BFF72BE8BDDD0F1_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.float4x4::ToString()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float4x4_ToString_m782366095AD7C5C4144B9A7EE0113328AAEC908B_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.float4x4::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float4x4_ToString_m713F20FCA71B96C1A0C98544E066CA6552EB6DA6_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * __this, int32_t ___index0, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Implicit(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  float4_op_Implicit_mD5B7AB76E24FAE3CB1B9F8257873D3294182C36E (Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___v0, const RuntimeMethod* method);
// Unity.Mathematics.uint3 Unity.Mathematics.math::asuint(Unity.Mathematics.float3)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  math_asuint_mAF6BF0C9EDA601A8692DB7BD3B443BFFA8A7C234_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.uint3 Unity.Mathematics.math::uint3(System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  math_uint3_m267A853D616F9ECD652755897C37C40C97EF2606_inline (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method);
// Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_Multiply(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  uint3_op_Multiply_mC75EC5D3A910BC87EC728F1D02D9766F6CBB5E44_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___lhs0, uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___rhs1, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint3)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_csum_mE0AA784B28235A836A585BE089A0D71C66087888_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.math::asuint(Unity.Mathematics.float4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.math::uint4(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Multiply(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs1, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_csum_m631B7AE1741D825306BA37F108EFB5BC37DBC20F_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(Unity.Mathematics.float4,System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___lhs0, float ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Addition(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___lhs0, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Addition(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs1, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.math::asint(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t math_asint_mE77DB058803DB637FC10ADA11C8F8B92C53364B6_inline (float ___x0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::asuint(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline (float ___x0, const RuntimeMethod* method);
// System.Boolean System.Single::IsNaN(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB (float ___f0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::min(System.Single,System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float math_min_mAFED8DFF69B82D3639513AD5275FBBBEE0034403_inline (float ___x0, float ___y1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::max(System.Single,System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float math_max_mDA87691EF0947E7D4DE6ADC8AE222FD3F2D15B37_inline (float ___x0, float ___y1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::sqrt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_sqrt_mFFB10D250782D3B931727A664F421D1061A8F76B (float ___x0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float math_dot_mD612AB280CD7FF05D3F393CDB7011CD8FBD2B0FA_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___x0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___y1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::rsqrt(System.Single)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float math_rsqrt_m9E4131955C2195AE4F694A4AE87815428FAB59F3_inline (float ___x0, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(System.Single,Unity.Mathematics.float3)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  float3_op_Multiply_m719CA87B984E18E247E64BE24D994AA45D97752A_inline (float ___lhs0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___rhs1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.uint4::.ctor(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Addition(Unity.Mathematics.uint4,System.UInt32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Addition_m58A8D6BFA5DDA9143D132B57084CB4B271EF1D35_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint32_t ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Multiply(Unity.Mathematics.uint4,System.UInt32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Multiply_m6302785769695A846A3A586E86A3455561C1582B_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint32_t ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_LeftShift(Unity.Mathematics.uint4,System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_LeftShift_m031F4835321567AB31E062A9BBD456FEBBF50405_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___x0, int32_t ___n1, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_RightShift(Unity.Mathematics.uint4,System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_RightShift_m8714E5CBC739B35AB61645F0E5B2569C2D929DD3_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___x0, int32_t ___n1, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_BitwiseOr(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_BitwiseOr_mA59ED29C9B5B61C2FAE0846C61049A7D69F1F0C0_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs1, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::rol(System.UInt32,System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_rol_mE88B527F8CC27B5191D4820D20E356489857BD86_inline (uint32_t ___x0, int32_t ___n1, const RuntimeMethod* method);
// Unity.Mathematics.float4x4 Unity.Mathematics.math::float4x4(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  math_float4x4_mDBFE1BFDCF3B3290DEA76F08CBFEF2537A5C60E1_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c00, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c11, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c22, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c33, const RuntimeMethod* method);
// System.Void Unity.Mathematics.uint3::.ctor(System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void uint3__ctor_m83EC50FF1E3D05E4220E4D77C4D2209FE1BDEE37_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.uint3::Equals(Unity.Mathematics.uint3)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool uint3_Equals_mF1A43BF5E958201193205C727DE277A13E8A7877_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.uint3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint3_Equals_mD0F46E10E9BBF31279855DAFE2325C6038C0E6D3 (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint3)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_hash_mCDE6963B79DB564C76CD37630D658920BFA586B9_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.uint3::GetHashCode()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t uint3_GetHashCode_mB20D3856AF84E5AA5A7222DE08C7F21CA127039D_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.uint3::ToString()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* uint3_ToString_m02FAB1C4D7D9A4831F7D0D78AF975B8C4B7EF4FD_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, const RuntimeMethod* method);
// System.String System.UInt32::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE (uint32_t* __this, String_t* ___format0, RuntimeObject* ___provider1, const RuntimeMethod* method);
// System.String Unity.Mathematics.uint3::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* uint3_ToString_m7705E589BDC0FCC9D87428DEFD95A66EF4A7A6AF_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.uint4::Equals(Unity.Mathematics.uint4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool uint4_Equals_m302069717D95B95C972C7C1B7D29D9E0DBBD6542_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.uint4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint4_Equals_mA3312E8FC1A4734E2402424ADAC7BCE386918488 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint4)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_hash_m1DF44299A6DEA06F77DA49DFA00039E341DB3F33_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.uint4::GetHashCode()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t uint4_GetHashCode_m187968010316E4D76A2FD32E8CD068A299007AA8_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.uint4::ToString()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* uint4_ToString_m1ADFBF43F0EC44A59D7F9908C9891DC3C31690BC_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.uint4::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* uint4_ToString_mED39980F32C81D913660EC644111F2070E7870A2_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.float3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * _thisAdjusted = reinterpret_cast<float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 *>(__this + _offset);
	float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline(_thisAdjusted, ___x0, ___y1, ___z2, method);
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(Unity.Mathematics.float3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  float3_op_Multiply_mB63D81E6A68BA3002AB0A2A1BBB2401C7CF7CC47 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator * (float3 lhs, float rhs) { return new float3 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs); }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(System.Single,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  float3_op_Multiply_m719CA87B984E18E247E64BE24D994AA45D97752A (float ___lhs0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator * (float lhs, float3 rhs) { return new float3 (lhs * rhs.x, lhs * rhs.y, lhs * rhs.z); }
		float L_0 = ___lhs0;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_1 = ___rhs1;
		float L_2 = L_1.get_x_0();
		float L_3 = ___lhs0;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___rhs1;
		float L_5 = L_4.get_y_1();
		float L_6 = ___lhs0;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_7 = ___rhs1;
		float L_8 = L_7.get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_0, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_3, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_6, (float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Addition(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  float3_op_Addition_mAC094AA119118FFD28ABADEA18C46849CF2D4609 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___lhs0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator + (float3 lhs, float3 rhs) { return new float3 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Subtraction(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  float3_op_Subtraction_mB767105FEAF2403E8597BF6867D214BEE08C3751 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___lhs0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator - (float3 lhs, float3 rhs) { return new float3 (lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z); }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean Unity.Mathematics.float3::Equals(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float3_Equals_mD9E67316EB63B276A4E7D47E0DA25377FC21C129 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float3 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z; }
		float L_0 = __this->get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_1 = ___rhs0;
		float L_2 = L_1.get_x_0();
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_002b;
		}
	}
	{
		float L_3 = __this->get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___rhs0;
		float L_5 = L_4.get_y_1();
		if ((!(((float)L_3) == ((float)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		float L_6 = __this->get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_7 = ___rhs0;
		float L_8 = L_7.get_z_2();
		return (bool)((((float)L_6) == ((float)L_8))? 1 : 0);
	}

IL_002b:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool float3_Equals_mD9E67316EB63B276A4E7D47E0DA25377FC21C129_AdjustorThunk (RuntimeObject * __this, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * _thisAdjusted = reinterpret_cast<float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 *>(__this + _offset);
	return float3_Equals_mD9E67316EB63B276A4E7D47E0DA25377FC21C129_inline(_thisAdjusted, ___rhs0, method);
}
// System.Boolean Unity.Mathematics.float3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float3_Equals_m2B1D30B076B6597B9F40527025211F33E8EC9145 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float3_Equals_m2B1D30B076B6597B9F40527025211F33E8EC9145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((float3)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1 = float3_Equals_mD9E67316EB63B276A4E7D47E0DA25377FC21C129_inline((float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 *)__this, ((*(float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 *)((float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 *)UnBox(L_0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool float3_Equals_m2B1D30B076B6597B9F40527025211F33E8EC9145_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * _thisAdjusted = reinterpret_cast<float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 *>(__this + _offset);
	return float3_Equals_m2B1D30B076B6597B9F40527025211F33E8EC9145(_thisAdjusted, ___o0, method);
}
// System.Int32 Unity.Mathematics.float3::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t float3_GetHashCode_mA8A92E15BE1FE55D94575007DAF37C6AB9379C24 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = (*(float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 *)__this);
		uint32_t L_1 = math_hash_m2502AF91810C2607571B2000ECA8A7370055FEF5_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t float3_GetHashCode_mA8A92E15BE1FE55D94575007DAF37C6AB9379C24_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * _thisAdjusted = reinterpret_cast<float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 *>(__this + _offset);
	return float3_GetHashCode_mA8A92E15BE1FE55D94575007DAF37C6AB9379C24_inline(_thisAdjusted, method);
}
// System.String Unity.Mathematics.float3::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float3_ToString_mB4863B6E2512E53544B02772705EBF221D744416 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float3_ToString_mB4863B6E2512E53544B02772705EBF221D744416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3({0}f, {1}f, {2}f)", x, y, z);
		float L_0 = __this->get_x_0();
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_1);
		float L_3 = __this->get_y_1();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_4);
		float L_6 = __this->get_z_2();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9 = String_Format_m26BBF75F9609FAD0B39C2242FEBAAD7D68F14D99(_stringLiteralDF02D7418655748837E0CFA19E962079704EDA2F, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_EXTERN_C  String_t* float3_ToString_mB4863B6E2512E53544B02772705EBF221D744416_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * _thisAdjusted = reinterpret_cast<float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 *>(__this + _offset);
	return float3_ToString_mB4863B6E2512E53544B02772705EBF221D744416_inline(_thisAdjusted, method);
}
// System.String Unity.Mathematics.float3::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float3_ToString_m513168CE73E3A1943FBF999282E27D81DB726E1C (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float3_ToString_m513168CE73E3A1943FBF999282E27D81DB726E1C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3({0}f, {1}f, {2}f)", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider));
		float* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_0, L_1, L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_4, L_5, L_6, /*hidden argument*/NULL);
		float* L_8 = __this->get_address_of_z_2();
		String_t* L_9 = ___format0;
		RuntimeObject* L_10 = ___formatProvider1;
		String_t* L_11 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_8, L_9, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Format_m26BBF75F9609FAD0B39C2242FEBAAD7D68F14D99(_stringLiteralDF02D7418655748837E0CFA19E962079704EDA2F, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C  String_t* float3_ToString_m513168CE73E3A1943FBF999282E27D81DB726E1C_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * _thisAdjusted = reinterpret_cast<float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 *>(__this + _offset);
	return float3_ToString_m513168CE73E3A1943FBF999282E27D81DB726E1C_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
}
// UnityEngine.Vector3 Unity.Mathematics.float3::op_Implicit(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  float3_op_Implicit_m36AA7A36BB934C8EF6E5D473B001631A692572BA (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___v0, const RuntimeMethod* method)
{
	{
		// public static implicit operator Vector3(float3 v)     { return new Vector3(v.x, v.y, v.z); }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___v0;
		float L_5 = L_4.get_z_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_6), L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.float4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * _thisAdjusted = reinterpret_cast<float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *>(__this + _offset);
	float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4__ctor_mC873E36671C90EFA5B0F4513EEA6D79B0E4D4333 (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___xyz0, float ___w1, const RuntimeMethod* method)
{
	{
		// this.x = xyz.x;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___xyz0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xyz.y;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_2 = ___xyz0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = xyz.z;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___xyz0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(L_5);
		// this.w = w;
		float L_6 = ___w1;
		__this->set_w_3(L_6);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4__ctor_mC873E36671C90EFA5B0F4513EEA6D79B0E4D4333_AdjustorThunk (RuntimeObject * __this, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___xyz0, float ___w1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * _thisAdjusted = reinterpret_cast<float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *>(__this + _offset);
	float4__ctor_mC873E36671C90EFA5B0F4513EEA6D79B0E4D4333_inline(_thisAdjusted, ___xyz0, ___w1, method);
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(Unity.Mathematics.float4,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93 (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator * (float4 lhs, float rhs) { return new float4 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_9 = ___lhs0;
		float L_10 = L_9.get_w_3();
		float L_11 = ___rhs1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), ((float)il2cpp_codegen_multiply((float)L_10, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Addition(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493 (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___lhs0, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator + (float4 lhs, float4 rhs) { return new float4 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_16), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), ((float)il2cpp_codegen_add((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float4::get_xyz()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  float4_get_xyz_mC5FD59B33F4C3F7DA22E8C0F0B1FFC2C38A1F8DB (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(x, y, z); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float L_2 = __this->get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C  float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  float4_get_xyz_mC5FD59B33F4C3F7DA22E8C0F0B1FFC2C38A1F8DB_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * _thisAdjusted = reinterpret_cast<float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *>(__this + _offset);
	return float4_get_xyz_mC5FD59B33F4C3F7DA22E8C0F0B1FFC2C38A1F8DB_inline(_thisAdjusted, method);
}
// System.Boolean Unity.Mathematics.float4::Equals(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65 (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float4 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w; }
		float L_0 = __this->get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = ___rhs0;
		float L_2 = L_1.get_x_0();
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_0039;
		}
	}
	{
		float L_3 = __this->get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_4 = ___rhs0;
		float L_5 = L_4.get_y_1();
		if ((!(((float)L_3) == ((float)L_5))))
		{
			goto IL_0039;
		}
	}
	{
		float L_6 = __this->get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_7 = ___rhs0;
		float L_8 = L_7.get_z_2();
		if ((!(((float)L_6) == ((float)L_8))))
		{
			goto IL_0039;
		}
	}
	{
		float L_9 = __this->get_w_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_10 = ___rhs0;
		float L_11 = L_10.get_w_3();
		return (bool)((((float)L_9) == ((float)L_11))? 1 : 0);
	}

IL_0039:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_AdjustorThunk (RuntimeObject * __this, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * _thisAdjusted = reinterpret_cast<float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *>(__this + _offset);
	return float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline(_thisAdjusted, ___rhs0, method);
}
// System.Boolean Unity.Mathematics.float4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4_Equals_m6E45C7C37F14AD55CF58E8C81A570A7035C33987 (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float4_Equals_m6E45C7C37F14AD55CF58E8C81A570A7035C33987_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((float4)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1 = float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline((float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)__this, ((*(float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)((float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)UnBox(L_0, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool float4_Equals_m6E45C7C37F14AD55CF58E8C81A570A7035C33987_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * _thisAdjusted = reinterpret_cast<float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *>(__this + _offset);
	return float4_Equals_m6E45C7C37F14AD55CF58E8C81A570A7035C33987(_thisAdjusted, ___o0, method);
}
// System.Int32 Unity.Mathematics.float4::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t float4_GetHashCode_m9510EAC95094BD49DF7CF63937B46669614AB277 (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = (*(float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)__this);
		uint32_t L_1 = math_hash_m922609CC35709A98A21766DAB7DD2B0978C5D104_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t float4_GetHashCode_m9510EAC95094BD49DF7CF63937B46669614AB277_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * _thisAdjusted = reinterpret_cast<float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *>(__this + _offset);
	return float4_GetHashCode_m9510EAC95094BD49DF7CF63937B46669614AB277_inline(_thisAdjusted, method);
}
// System.String Unity.Mathematics.float4::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float4_ToString_m8D534BBA43D7804CD31E26FE4C1972284DE35FBC (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float4_ToString_m8D534BBA43D7804CD31E26FE4C1972284DE35FBC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4({0}f, {1}f, {2}f, {3}f)", x, y, z, w);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		float L_2 = __this->get_x_0();
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_1;
		float L_6 = __this->get_y_1();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_5;
		float L_10 = __this->get_z_2();
		float L_11 = L_10;
		RuntimeObject * L_12 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_9;
		float L_14 = __this->get_w_3();
		float L_15 = L_14;
		RuntimeObject * L_16 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral2211E2D567B9991441FFEFBE5AAA03456AB8ABCA, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
IL2CPP_EXTERN_C  String_t* float4_ToString_m8D534BBA43D7804CD31E26FE4C1972284DE35FBC_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * _thisAdjusted = reinterpret_cast<float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *>(__this + _offset);
	return float4_ToString_m8D534BBA43D7804CD31E26FE4C1972284DE35FBC_inline(_thisAdjusted, method);
}
// System.String Unity.Mathematics.float4::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float4_ToString_m92F4A00D57F53DC94183F09F72ED6CAC1F390650 (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float4_ToString_m92F4A00D57F53DC94183F09F72ED6CAC1F390650_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4({0}f, {1}f, {2}f, {3}f)", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider), w.ToString(format, formatProvider));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		float* L_2 = __this->get_address_of_x_0();
		String_t* L_3 = ___format0;
		RuntimeObject* L_4 = ___formatProvider1;
		String_t* L_5 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_1;
		float* L_7 = __this->get_address_of_y_1();
		String_t* L_8 = ___format0;
		RuntimeObject* L_9 = ___formatProvider1;
		String_t* L_10 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_6;
		float* L_12 = __this->get_address_of_z_2();
		String_t* L_13 = ___format0;
		RuntimeObject* L_14 = ___formatProvider1;
		String_t* L_15 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = L_11;
		float* L_17 = __this->get_address_of_w_3();
		String_t* L_18 = ___format0;
		RuntimeObject* L_19 = ___formatProvider1;
		String_t* L_20 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral2211E2D567B9991441FFEFBE5AAA03456AB8ABCA, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
IL2CPP_EXTERN_C  String_t* float4_ToString_m92F4A00D57F53DC94183F09F72ED6CAC1F390650_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * _thisAdjusted = reinterpret_cast<float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *>(__this + _offset);
	return float4_ToString_m92F4A00D57F53DC94183F09F72ED6CAC1F390650_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Implicit(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  float4_op_Implicit_mD5B7AB76E24FAE3CB1B9F8257873D3294182C36E (Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___v0, const RuntimeMethod* method)
{
	{
		// public static implicit operator float4(Vector4 v)     { return new float4(v.x, v.y, v.z, v.w); }
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_0 = ___v0;
		float L_1 = L_0.get_x_1();
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_2 = ___v0;
		float L_3 = L_2.get_y_2();
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_4 = ___v0;
		float L_5 = L_4.get_z_3();
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_6 = ___v0;
		float L_7 = L_6.get_w_4();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_8;
		memset((&L_8), 0, sizeof(L_8));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_8), L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.float4x4::.ctor(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4x4__ctor_m9A2D24D07FF8DCB7495AC3FD76364FD3FB81D807 (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c00, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c11, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c22, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c33, const RuntimeMethod* method)
{
	{
		// this.c0 = c0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___c00;
		__this->set_c0_0(L_0);
		// this.c1 = c1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = ___c11;
		__this->set_c1_1(L_1);
		// this.c2 = c2;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_2 = ___c22;
		__this->set_c2_2(L_2);
		// this.c3 = c3;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_3 = ___c33;
		__this->set_c3_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4x4__ctor_m9A2D24D07FF8DCB7495AC3FD76364FD3FB81D807_AdjustorThunk (RuntimeObject * __this, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c00, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c11, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c22, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c33, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * _thisAdjusted = reinterpret_cast<float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *>(__this + _offset);
	float4x4__ctor_m9A2D24D07FF8DCB7495AC3FD76364FD3FB81D807_inline(_thisAdjusted, ___c00, ___c11, ___c22, ___c33, method);
}
// System.Void Unity.Mathematics.float4x4::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4x4__ctor_m9C451E986E9271E34E011C51F35160C8E9E6CF98 (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, float ___m000, float ___m011, float ___m022, float ___m033, float ___m104, float ___m115, float ___m126, float ___m137, float ___m208, float ___m219, float ___m2210, float ___m2311, float ___m3012, float ___m3113, float ___m3214, float ___m3315, const RuntimeMethod* method)
{
	{
		// this.c0 = new float4(m00, m10, m20, m30);
		float L_0 = ___m000;
		float L_1 = ___m104;
		float L_2 = ___m208;
		float L_3 = ___m3012;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_c0_0(L_4);
		// this.c1 = new float4(m01, m11, m21, m31);
		float L_5 = ___m011;
		float L_6 = ___m115;
		float L_7 = ___m219;
		float L_8 = ___m3113;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_9), L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->set_c1_1(L_9);
		// this.c2 = new float4(m02, m12, m22, m32);
		float L_10 = ___m022;
		float L_11 = ___m126;
		float L_12 = ___m2210;
		float L_13 = ___m3214;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_14;
		memset((&L_14), 0, sizeof(L_14));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_14), L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		__this->set_c2_2(L_14);
		// this.c3 = new float4(m03, m13, m23, m33);
		float L_15 = ___m033;
		float L_16 = ___m137;
		float L_17 = ___m2311;
		float L_18 = ___m3315;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_19;
		memset((&L_19), 0, sizeof(L_19));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_19), L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		__this->set_c3_3(L_19);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4x4__ctor_m9C451E986E9271E34E011C51F35160C8E9E6CF98_AdjustorThunk (RuntimeObject * __this, float ___m000, float ___m011, float ___m022, float ___m033, float ___m104, float ___m115, float ___m126, float ___m137, float ___m208, float ___m219, float ___m2210, float ___m2311, float ___m3012, float ___m3113, float ___m3214, float ___m3315, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * _thisAdjusted = reinterpret_cast<float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *>(__this + _offset);
	float4x4__ctor_m9C451E986E9271E34E011C51F35160C8E9E6CF98_inline(_thisAdjusted, ___m000, ___m011, ___m022, ___m033, ___m104, ___m115, ___m126, ___m137, ___m208, ___m219, ___m2210, ___m2311, ___m3012, ___m3113, ___m3214, ___m3315, method);
}
// System.Boolean Unity.Mathematics.float4x4::Equals(Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4x4_Equals_m78EF9656D285326360D7957143A42017BE85AD96 (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float4x4 rhs) { return c0.Equals(rhs.c0) && c1.Equals(rhs.c1) && c2.Equals(rhs.c2) && c3.Equals(rhs.c3); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_0 = __this->get_address_of_c0_0();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_1 = ___rhs0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_2 = L_1.get_c0_0();
		bool L_3 = float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline((float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_4 = __this->get_address_of_c1_1();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_5 = ___rhs0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = L_5.get_c1_1();
		bool L_7 = float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline((float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_8 = __this->get_address_of_c2_2();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_9 = ___rhs0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_10 = L_9.get_c2_2();
		bool L_11 = float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline((float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004b;
		}
	}
	{
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_12 = __this->get_address_of_c3_3();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_13 = ___rhs0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_14 = L_13.get_c3_3();
		bool L_15 = float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline((float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)L_12, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_004b:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool float4x4_Equals_m78EF9656D285326360D7957143A42017BE85AD96_AdjustorThunk (RuntimeObject * __this, float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * _thisAdjusted = reinterpret_cast<float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *>(__this + _offset);
	return float4x4_Equals_m78EF9656D285326360D7957143A42017BE85AD96_inline(_thisAdjusted, ___rhs0, method);
}
// System.Boolean Unity.Mathematics.float4x4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4x4_Equals_m2E4E7CA61E037ECF3FCFF3CDF4A454F2DA1DD202 (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float4x4_Equals_m2E4E7CA61E037ECF3FCFF3CDF4A454F2DA1DD202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((float4x4)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1 = float4x4_Equals_m78EF9656D285326360D7957143A42017BE85AD96_inline((float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *)__this, ((*(float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *)((float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *)UnBox(L_0, float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool float4x4_Equals_m2E4E7CA61E037ECF3FCFF3CDF4A454F2DA1DD202_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * _thisAdjusted = reinterpret_cast<float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *>(__this + _offset);
	return float4x4_Equals_m2E4E7CA61E037ECF3FCFF3CDF4A454F2DA1DD202(_thisAdjusted, ___o0, method);
}
// System.Int32 Unity.Mathematics.float4x4::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t float4x4_GetHashCode_m35D91773F446B0D620E5B5E04BFF72BE8BDDD0F1 (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_0 = (*(float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *)__this);
		uint32_t L_1 = math_hash_m8E61D111AA597992FA0D8C6D5FBE3FBC7B417B25_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t float4x4_GetHashCode_m35D91773F446B0D620E5B5E04BFF72BE8BDDD0F1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * _thisAdjusted = reinterpret_cast<float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *>(__this + _offset);
	return float4x4_GetHashCode_m35D91773F446B0D620E5B5E04BFF72BE8BDDD0F1_inline(_thisAdjusted, method);
}
// System.String Unity.Mathematics.float4x4::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float4x4_ToString_m782366095AD7C5C4144B9A7EE0113328AAEC908B (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float4x4_ToString_m782366095AD7C5C4144B9A7EE0113328AAEC908B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4x4({0}f, {1}f, {2}f, {3}f,  {4}f, {5}f, {6}f, {7}f,  {8}f, {9}f, {10}f, {11}f,  {12}f, {13}f, {14}f, {15}f)", c0.x, c1.x, c2.x, c3.x, c0.y, c1.y, c2.y, c3.y, c0.z, c1.z, c2.z, c3.z, c0.w, c1.w, c2.w, c3.w);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_2 = __this->get_address_of_c0_0();
		float L_3 = L_2->get_x_0();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_7 = __this->get_address_of_c1_1();
		float L_8 = L_7->get_x_0();
		float L_9 = L_8;
		RuntimeObject * L_10 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_6;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_12 = __this->get_address_of_c2_2();
		float L_13 = L_12->get_x_0();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = L_11;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_17 = __this->get_address_of_c3_3();
		float L_18 = L_17->get_x_0();
		float L_19 = L_18;
		RuntimeObject * L_20 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_21 = L_16;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_22 = __this->get_address_of_c0_0();
		float L_23 = L_22->get_y_1();
		float L_24 = L_23;
		RuntimeObject * L_25 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_25);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_25);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_26 = L_21;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_27 = __this->get_address_of_c1_1();
		float L_28 = L_27->get_y_1();
		float L_29 = L_28;
		RuntimeObject * L_30 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_30);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_30);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_31 = L_26;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_32 = __this->get_address_of_c2_2();
		float L_33 = L_32->get_y_1();
		float L_34 = L_33;
		RuntimeObject * L_35 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_35);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_35);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_36 = L_31;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_37 = __this->get_address_of_c3_3();
		float L_38 = L_37->get_y_1();
		float L_39 = L_38;
		RuntimeObject * L_40 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_40);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_40);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_41 = L_36;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_42 = __this->get_address_of_c0_0();
		float L_43 = L_42->get_z_2();
		float L_44 = L_43;
		RuntimeObject * L_45 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_45);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_45);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_46 = L_41;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_47 = __this->get_address_of_c1_1();
		float L_48 = L_47->get_z_2();
		float L_49 = L_48;
		RuntimeObject * L_50 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_46);
		ArrayElementTypeCheck (L_46, L_50);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)L_50);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_51 = L_46;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_52 = __this->get_address_of_c2_2();
		float L_53 = L_52->get_z_2();
		float L_54 = L_53;
		RuntimeObject * L_55 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, L_55);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_55);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_56 = L_51;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_57 = __this->get_address_of_c3_3();
		float L_58 = L_57->get_z_2();
		float L_59 = L_58;
		RuntimeObject * L_60 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_59);
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_60);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)L_60);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_61 = L_56;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_62 = __this->get_address_of_c0_0();
		float L_63 = L_62->get_w_3();
		float L_64 = L_63;
		RuntimeObject * L_65 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_65);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_65);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_66 = L_61;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_67 = __this->get_address_of_c1_1();
		float L_68 = L_67->get_w_3();
		float L_69 = L_68;
		RuntimeObject * L_70 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_69);
		NullCheck(L_66);
		ArrayElementTypeCheck (L_66, L_70);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)L_70);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_71 = L_66;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_72 = __this->get_address_of_c2_2();
		float L_73 = L_72->get_w_3();
		float L_74 = L_73;
		RuntimeObject * L_75 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_74);
		NullCheck(L_71);
		ArrayElementTypeCheck (L_71, L_75);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_75);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_76 = L_71;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_77 = __this->get_address_of_c3_3();
		float L_78 = L_77->get_w_3();
		float L_79 = L_78;
		RuntimeObject * L_80 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_79);
		NullCheck(L_76);
		ArrayElementTypeCheck (L_76, L_80);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)L_80);
		String_t* L_81 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral782960A2A1A5CFC6AA819F3CC53C89E5F0641539, L_76, /*hidden argument*/NULL);
		return L_81;
	}
}
IL2CPP_EXTERN_C  String_t* float4x4_ToString_m782366095AD7C5C4144B9A7EE0113328AAEC908B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * _thisAdjusted = reinterpret_cast<float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *>(__this + _offset);
	return float4x4_ToString_m782366095AD7C5C4144B9A7EE0113328AAEC908B_inline(_thisAdjusted, method);
}
// System.String Unity.Mathematics.float4x4::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float4x4_ToString_m713F20FCA71B96C1A0C98544E066CA6552EB6DA6 (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float4x4_ToString_m713F20FCA71B96C1A0C98544E066CA6552EB6DA6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4x4({0}f, {1}f, {2}f, {3}f,  {4}f, {5}f, {6}f, {7}f,  {8}f, {9}f, {10}f, {11}f,  {12}f, {13}f, {14}f, {15}f)", c0.x.ToString(format, formatProvider), c1.x.ToString(format, formatProvider), c2.x.ToString(format, formatProvider), c3.x.ToString(format, formatProvider), c0.y.ToString(format, formatProvider), c1.y.ToString(format, formatProvider), c2.y.ToString(format, formatProvider), c3.y.ToString(format, formatProvider), c0.z.ToString(format, formatProvider), c1.z.ToString(format, formatProvider), c2.z.ToString(format, formatProvider), c3.z.ToString(format, formatProvider), c0.w.ToString(format, formatProvider), c1.w.ToString(format, formatProvider), c2.w.ToString(format, formatProvider), c3.w.ToString(format, formatProvider));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_2 = __this->get_address_of_c0_0();
		float* L_3 = L_2->get_address_of_x_0();
		String_t* L_4 = ___format0;
		RuntimeObject* L_5 = ___formatProvider1;
		String_t* L_6 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_6);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_7 = L_1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_8 = __this->get_address_of_c1_1();
		float* L_9 = L_8->get_address_of_x_0();
		String_t* L_10 = ___format0;
		RuntimeObject* L_11 = ___formatProvider1;
		String_t* L_12 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_12);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_12);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_7;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_14 = __this->get_address_of_c2_2();
		float* L_15 = L_14->get_address_of_x_0();
		String_t* L_16 = ___format0;
		RuntimeObject* L_17 = ___formatProvider1;
		String_t* L_18 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_18);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_18);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_19 = L_13;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_20 = __this->get_address_of_c3_3();
		float* L_21 = L_20->get_address_of_x_0();
		String_t* L_22 = ___format0;
		RuntimeObject* L_23 = ___formatProvider1;
		String_t* L_24 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_24);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_25 = L_19;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_26 = __this->get_address_of_c0_0();
		float* L_27 = L_26->get_address_of_y_1();
		String_t* L_28 = ___format0;
		RuntimeObject* L_29 = ___formatProvider1;
		String_t* L_30 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_27, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_30);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_30);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_31 = L_25;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_32 = __this->get_address_of_c1_1();
		float* L_33 = L_32->get_address_of_y_1();
		String_t* L_34 = ___format0;
		RuntimeObject* L_35 = ___formatProvider1;
		String_t* L_36 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_33, L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_36);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_36);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_37 = L_31;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_38 = __this->get_address_of_c2_2();
		float* L_39 = L_38->get_address_of_y_1();
		String_t* L_40 = ___format0;
		RuntimeObject* L_41 = ___formatProvider1;
		String_t* L_42 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_39, L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_42);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_42);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_43 = L_37;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_44 = __this->get_address_of_c3_3();
		float* L_45 = L_44->get_address_of_y_1();
		String_t* L_46 = ___format0;
		RuntimeObject* L_47 = ___formatProvider1;
		String_t* L_48 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_45, L_46, L_47, /*hidden argument*/NULL);
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, L_48);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_48);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_49 = L_43;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_50 = __this->get_address_of_c0_0();
		float* L_51 = L_50->get_address_of_z_2();
		String_t* L_52 = ___format0;
		RuntimeObject* L_53 = ___formatProvider1;
		String_t* L_54 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_51, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, L_54);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_54);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_55 = L_49;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_56 = __this->get_address_of_c1_1();
		float* L_57 = L_56->get_address_of_z_2();
		String_t* L_58 = ___format0;
		RuntimeObject* L_59 = ___formatProvider1;
		String_t* L_60 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_57, L_58, L_59, /*hidden argument*/NULL);
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, L_60);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)L_60);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_61 = L_55;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_62 = __this->get_address_of_c2_2();
		float* L_63 = L_62->get_address_of_z_2();
		String_t* L_64 = ___format0;
		RuntimeObject* L_65 = ___formatProvider1;
		String_t* L_66 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_63, L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_66);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_66);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_67 = L_61;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_68 = __this->get_address_of_c3_3();
		float* L_69 = L_68->get_address_of_z_2();
		String_t* L_70 = ___format0;
		RuntimeObject* L_71 = ___formatProvider1;
		String_t* L_72 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_69, L_70, L_71, /*hidden argument*/NULL);
		NullCheck(L_67);
		ArrayElementTypeCheck (L_67, L_72);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)L_72);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_73 = L_67;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_74 = __this->get_address_of_c0_0();
		float* L_75 = L_74->get_address_of_w_3();
		String_t* L_76 = ___format0;
		RuntimeObject* L_77 = ___formatProvider1;
		String_t* L_78 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_75, L_76, L_77, /*hidden argument*/NULL);
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, L_78);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_78);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_79 = L_73;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_80 = __this->get_address_of_c1_1();
		float* L_81 = L_80->get_address_of_w_3();
		String_t* L_82 = ___format0;
		RuntimeObject* L_83 = ___formatProvider1;
		String_t* L_84 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_81, L_82, L_83, /*hidden argument*/NULL);
		NullCheck(L_79);
		ArrayElementTypeCheck (L_79, L_84);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)L_84);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_85 = L_79;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_86 = __this->get_address_of_c2_2();
		float* L_87 = L_86->get_address_of_w_3();
		String_t* L_88 = ___format0;
		RuntimeObject* L_89 = ___formatProvider1;
		String_t* L_90 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_87, L_88, L_89, /*hidden argument*/NULL);
		NullCheck(L_85);
		ArrayElementTypeCheck (L_85, L_90);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_90);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_91 = L_85;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_92 = __this->get_address_of_c3_3();
		float* L_93 = L_92->get_address_of_w_3();
		String_t* L_94 = ___format0;
		RuntimeObject* L_95 = ___formatProvider1;
		String_t* L_96 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_93, L_94, L_95, /*hidden argument*/NULL);
		NullCheck(L_91);
		ArrayElementTypeCheck (L_91, L_96);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)L_96);
		String_t* L_97 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral782960A2A1A5CFC6AA819F3CC53C89E5F0641539, L_91, /*hidden argument*/NULL);
		return L_97;
	}
}
IL2CPP_EXTERN_C  String_t* float4x4_ToString_m713F20FCA71B96C1A0C98544E066CA6552EB6DA6_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * _thisAdjusted = reinterpret_cast<float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *>(__this + _offset);
	return float4x4_ToString_m713F20FCA71B96C1A0C98544E066CA6552EB6DA6_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
}
// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Implicit(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  float4x4_op_Implicit_m6AE393AD5B13121437B0FACB0DFCA8098C8C8F9A (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m0, const RuntimeMethod* method)
{
	{
		// public static implicit operator float4x4(Matrix4x4 m) { return new float4x4(m.GetColumn(0), m.GetColumn(1), m.GetColumn(2), m.GetColumn(3)); }
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_0 = Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&___m0), 0, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = float4_op_Implicit_mD5B7AB76E24FAE3CB1B9F8257873D3294182C36E(L_0, /*hidden argument*/NULL);
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_2 = Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&___m0), 1, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_3 = float4_op_Implicit_mD5B7AB76E24FAE3CB1B9F8257873D3294182C36E(L_2, /*hidden argument*/NULL);
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_4 = Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&___m0), 2, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_5 = float4_op_Implicit_mD5B7AB76E24FAE3CB1B9F8257873D3294182C36E(L_4, /*hidden argument*/NULL);
		Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  L_6 = Matrix4x4_GetColumn_m34D9081FB464BB7CF124C50BB5BE4C22E2DBFA9E((Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA *)(&___m0), 3, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_7 = float4_op_Implicit_mD5B7AB76E24FAE3CB1B9F8257873D3294182C36E(L_6, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_8;
		memset((&L_8), 0, sizeof(L_8));
		float4x4__ctor_m9A2D24D07FF8DCB7495AC3FD76364FD3FB81D807_inline((&L_8), L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void Unity.Mathematics.float4x4::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4x4__cctor_mFA9894818878962653AD1F8C490BF8B0D1E5F7EF (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float4x4__cctor_mFA9894818878962653AD1F8C490BF8B0D1E5F7EF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly float4x4 identity = new float4x4(1.0f, 0.0f, 0.0f, 0.0f,   0.0f, 1.0f, 0.0f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f,   0.0f, 0.0f, 0.0f, 1.0f);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_0;
		memset((&L_0), 0, sizeof(L_0));
		float4x4__ctor_m9C451E986E9271E34E011C51F35160C8E9E6CF98_inline((&L_0), (1.0f), (0.0f), (0.0f), (0.0f), (0.0f), (1.0f), (0.0f), (0.0f), (0.0f), (0.0f), (1.0f), (0.0f), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC_StaticFields*)il2cpp_codegen_static_fields_for(float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC_il2cpp_TypeInfo_var))->set_identity_4(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m2502AF91810C2607571B2000ECA8A7370055FEF5 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint3(0x9B13B92Du, 0x4ABF0813u, 0x86068063u)) + 0xD75513F9u;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___v0;
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_1 = math_asuint_mAF6BF0C9EDA601A8692DB7BD3B443BFFA8A7C234_inline(L_0, /*hidden argument*/NULL);
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_2 = math_uint3_m267A853D616F9ECD652755897C37C40C97EF2606_inline(((int32_t)-1693206227), ((int32_t)1254033427), ((int32_t)-2046394269), /*hidden argument*/NULL);
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_3 = uint3_op_Multiply_mC75EC5D3A910BC87EC728F1D02D9766F6CBB5E44_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4 = math_csum_mE0AA784B28235A836A585BE089A0D71C66087888_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-682290183)));
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m922609CC35709A98A21766DAB7DD2B0978C5D104 (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint4(0xE69626FFu, 0xBD010EEBu, 0x9CEDE1D1u, 0x43BE0B51u)) + 0xAF836EE1u;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___v0;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_1 = math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline(L_0, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)-426367233), ((int32_t)-1124004117), ((int32_t)-1662131759), ((int32_t)1136528209), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4 = math_csum_m631B7AE1741D825306BA37F108EFB5BC37DBC20F_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-1350340895)));
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.math::float4x4(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  math_float4x4_mDBFE1BFDCF3B3290DEA76F08CBFEF2537A5C60E1 (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c00, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c11, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c22, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c33, const RuntimeMethod* method)
{
	{
		// public static float4x4 float4x4(float4 c0, float4 c1, float4 c2, float4 c3) { return new float4x4(c0, c1, c2, c3); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___c00;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = ___c11;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_2 = ___c22;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_3 = ___c33;
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4x4__ctor_m9A2D24D07FF8DCB7495AC3FD76364FD3FB81D807_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::transform(Unity.Mathematics.float4x4,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  math_transform_m67EB6EEB72DC249326CB6D222CB9DD761FE9D551 (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___a0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___b1, const RuntimeMethod* method)
{
	float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// return (a.c0 * b.x + a.c1 * b.y + a.c2 * b.z + a.c3).xyz;
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_0 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = L_0.get_c0_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_2 = ___b1;
		float L_3 = L_2.get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_4 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_1, L_3, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_5 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = L_5.get_c1_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_7 = ___b1;
		float L_8 = L_7.get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_9 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_6, L_8, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_10 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_4, L_9, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_11 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_12 = L_11.get_c2_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_13 = ___b1;
		float L_14 = L_13.get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_15 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_12, L_14, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_16 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_10, L_15, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_17 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_18 = L_17.get_c3_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_19 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_16, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_20 = float4_get_xyz_mC5FD59B33F4C3F7DA22E8C0F0B1FFC2C38A1F8DB_inline((float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)(&V_0), /*hidden argument*/NULL);
		return L_20;
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m8E61D111AA597992FA0D8C6D5FBE3FBC7B417B25 (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v.c0) * uint4(0xC4B1493Fu, 0xBA0966D3u, 0xAFBEE253u, 0x5B419C01u) +
		//             asuint(v.c1) * uint4(0x515D90F5u, 0xEC9F68F3u, 0xF9EA92D5u, 0xC2FAFCB9u) +
		//             asuint(v.c2) * uint4(0x616E9CA1u, 0xC5C5394Bu, 0xCAE78587u, 0x7A1541C9u) +
		//             asuint(v.c3) * uint4(0xF83BD927u, 0x6A243BCBu, 0x509B84C9u, 0x91D13847u)) + 0x52F7230Fu;
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_0 = ___v0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = L_0.get_c0_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline(L_1, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)-995014337), ((int32_t)-1173788973), ((int32_t)-1346444717), ((int32_t)1531026433), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_2, L_3, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_5 = ___v0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = L_5.get_c1_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_7 = math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline(L_6, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_8 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)1365086453), ((int32_t)-325097229), ((int32_t)-102067499), ((int32_t)-1023738695), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_9 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_7, L_8, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_10 = uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE_inline(L_4, L_9, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_11 = ___v0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_12 = L_11.get_c2_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_13 = math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline(L_12, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_14 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)1634639009), ((int32_t)-976930485), ((int32_t)-890796665), ((int32_t)2048213449), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_15 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_13, L_14, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_16 = uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE_inline(L_10, L_15, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_17 = ___v0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_18 = L_17.get_c3_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_19 = math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline(L_18, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_20 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)-130295513), ((int32_t)1780759499), ((int32_t)1352369353), ((int32_t)-1848559545), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_21 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_19, L_20, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_22 = uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE_inline(L_16, L_21, /*hidden argument*/NULL);
		uint32_t L_23 = math_csum_m631B7AE1741D825306BA37F108EFB5BC37DBC20F_inline(L_22, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)((int32_t)1391928079)));
	}
}
// System.Int32 Unity.Mathematics.math::asint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t math_asint_mE77DB058803DB637FC10ADA11C8F8B92C53364B6 (float ___x0, const RuntimeMethod* method)
{
	IntFloatUnion_t7ECB2A03CE51B658B77F53B39547C3BDB8164CD7  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// u.intValue = 0;
		(&V_0)->set_intValue_0(0);
		// u.floatValue = x;
		float L_0 = ___x0;
		(&V_0)->set_floatValue_1(L_0);
		// return u.intValue;
		IntFloatUnion_t7ECB2A03CE51B658B77F53B39547C3BDB8164CD7  L_1 = V_0;
		int32_t L_2 = L_1.get_intValue_0();
		return L_2;
	}
}
// System.UInt32 Unity.Mathematics.math::asuint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707 (float ___x0, const RuntimeMethod* method)
{
	{
		// public static uint asuint(float x) { return (uint)asint(x); }
		float L_0 = ___x0;
		int32_t L_1 = math_asint_mE77DB058803DB637FC10ADA11C8F8B92C53364B6_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Unity.Mathematics.uint3 Unity.Mathematics.math::asuint(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  math_asuint_mAF6BF0C9EDA601A8692DB7BD3B443BFFA8A7C234 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint3 asuint(float3 x) { return uint3(asuint(x.x), asuint(x.y), asuint(x.z)); }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		uint32_t L_2 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_1, /*hidden argument*/NULL);
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		uint32_t L_5 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_4, /*hidden argument*/NULL);
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		uint32_t L_8 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_7, /*hidden argument*/NULL);
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_9 = math_uint3_m267A853D616F9ECD652755897C37C40C97EF2606_inline(L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.math::asuint(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint4 asuint(float4 x) { return uint4(asuint(x.x), asuint(x.y), asuint(x.z), asuint(x.w)); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		uint32_t L_2 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_1, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		uint32_t L_5 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_4, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		uint32_t L_8 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_7, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_9 = ___x0;
		float L_10 = L_9.get_w_3();
		uint32_t L_11 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_10, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single Unity.Mathematics.math::min(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_min_mAFED8DFF69B82D3639513AD5275FBBBEE0034403 (float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// public static float min(float x, float y) { return float.IsNaN(y) || x < y ? x : y; }
		float L_0 = ___y1;
		bool L_1 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___x0;
		float L_3 = ___y1;
		if ((((float)L_2) < ((float)L_3)))
		{
			goto IL_000e;
		}
	}
	{
		float L_4 = ___y1;
		return L_4;
	}

IL_000e:
	{
		float L_5 = ___x0;
		return L_5;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::min(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  math_min_m02CCAAC6A1744B2ECC218EA4A627D565D6B36FC7 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___x0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___y1, const RuntimeMethod* method)
{
	{
		// public static float3 min(float3 x, float3 y) { return new float3(min(x.x, y.x), min(x.y, y.y), min(x.z, y.z)); }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float L_4 = math_min_mAFED8DFF69B82D3639513AD5275FBBBEE0034403_inline(L_1, L_3, /*hidden argument*/NULL);
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_5 = ___x0;
		float L_6 = L_5.get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_7 = ___y1;
		float L_8 = L_7.get_y_1();
		float L_9 = math_min_mAFED8DFF69B82D3639513AD5275FBBBEE0034403_inline(L_6, L_8, /*hidden argument*/NULL);
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_10 = ___x0;
		float L_11 = L_10.get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_12 = ___y1;
		float L_13 = L_12.get_z_2();
		float L_14 = math_min_mAFED8DFF69B82D3639513AD5275FBBBEE0034403_inline(L_11, L_13, /*hidden argument*/NULL);
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_15;
		memset((&L_15), 0, sizeof(L_15));
		float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline((&L_15), L_4, L_9, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Single Unity.Mathematics.math::max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_max_mDA87691EF0947E7D4DE6ADC8AE222FD3F2D15B37 (float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// public static float max(float x, float y) { return float.IsNaN(y) || x > y ? x : y; }
		float L_0 = ___y1;
		bool L_1 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___x0;
		float L_3 = ___y1;
		if ((((float)L_2) > ((float)L_3)))
		{
			goto IL_000e;
		}
	}
	{
		float L_4 = ___y1;
		return L_4;
	}

IL_000e:
	{
		float L_5 = ___x0;
		return L_5;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::max(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  math_max_mBE2D84B4A9FC8CFD5473473FB0E2D6D7A16B2010 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___x0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___y1, const RuntimeMethod* method)
{
	{
		// public static float3 max(float3 x, float3 y) { return new float3(max(x.x, y.x), max(x.y, y.y), max(x.z, y.z)); }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float L_4 = math_max_mDA87691EF0947E7D4DE6ADC8AE222FD3F2D15B37_inline(L_1, L_3, /*hidden argument*/NULL);
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_5 = ___x0;
		float L_6 = L_5.get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_7 = ___y1;
		float L_8 = L_7.get_y_1();
		float L_9 = math_max_mDA87691EF0947E7D4DE6ADC8AE222FD3F2D15B37_inline(L_6, L_8, /*hidden argument*/NULL);
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_10 = ___x0;
		float L_11 = L_10.get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_12 = ___y1;
		float L_13 = L_12.get_z_2();
		float L_14 = math_max_mDA87691EF0947E7D4DE6ADC8AE222FD3F2D15B37_inline(L_11, L_13, /*hidden argument*/NULL);
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_15;
		memset((&L_15), 0, sizeof(L_15));
		float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline((&L_15), L_4, L_9, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_dot_mD612AB280CD7FF05D3F393CDB7011CD8FBD2B0FA (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___x0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___y1, const RuntimeMethod* method)
{
	{
		// public static float dot(float3 x, float3 y) { return x.x * y.x + x.y * y.y + x.z * y.z; }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___x0;
		float L_5 = L_4.get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_6 = ___y1;
		float L_7 = L_6.get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_8 = ___x0;
		float L_9 = L_8.get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_10 = ___y1;
		float L_11 = L_10.get_z_2();
		return ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11))));
	}
}
// System.Single Unity.Mathematics.math::sqrt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_sqrt_mFFB10D250782D3B931727A664F421D1061A8F76B (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (math_sqrt_mFFB10D250782D3B931727A664F421D1061A8F76B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float sqrt(float x) { return (float)System.Math.Sqrt((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tFB388E53C7FDC6FCCF9A19ABF5A4E521FBD52E19_il2cpp_TypeInfo_var);
		double L_1 = sqrt((((double)((double)(((float)((float)L_0)))))));
		return (((float)((float)L_1)));
	}
}
// System.Single Unity.Mathematics.math::rsqrt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_rsqrt_m9E4131955C2195AE4F694A4AE87815428FAB59F3 (float ___x0, const RuntimeMethod* method)
{
	{
		// public static float rsqrt(float x) { return 1.0f / sqrt(x); }
		float L_0 = ___x0;
		float L_1 = math_sqrt_mFFB10D250782D3B931727A664F421D1061A8F76B(L_0, /*hidden argument*/NULL);
		return ((float)((float)(1.0f)/(float)L_1));
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::normalize(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  math_normalize_mC205C6CF9B74165AA38214CE6692950982E7CDD4 (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___x0, const RuntimeMethod* method)
{
	{
		// public static float3 normalize(float3 x) { return rsqrt(dot(x, x)) * x; }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___x0;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_1 = ___x0;
		float L_2 = math_dot_mD612AB280CD7FF05D3F393CDB7011CD8FBD2B0FA_inline(L_0, L_1, /*hidden argument*/NULL);
		float L_3 = math_rsqrt_m9E4131955C2195AE4F694A4AE87815428FAB59F3_inline(L_2, /*hidden argument*/NULL);
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___x0;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_5 = float3_op_Multiply_m719CA87B984E18E247E64BE24D994AA45D97752A_inline(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.UInt32 Unity.Mathematics.math::rol(System.UInt32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_rol_mE88B527F8CC27B5191D4820D20E356489857BD86 (uint32_t ___x0, int32_t ___n1, const RuntimeMethod* method)
{
	{
		// public static uint rol(uint x, int n) { return (x << n) | (x >> (32 - n)); }
		uint32_t L_0 = ___x0;
		int32_t L_1 = ___n1;
		uint32_t L_2 = ___x0;
		int32_t L_3 = ___n1;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0<<(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))))|(int32_t)((int32_t)((uint32_t)L_2>>((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)32), (int32_t)L_3))&(int32_t)((int32_t)31)))))));
	}
}
// System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_csum_mE0AA784B28235A836A585BE089A0D71C66087888 (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint csum(uint3 x) { return x.x + x.y + x.z; }
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_2 = ___x0;
		uint32_t L_3 = L_2.get_y_1();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_4 = ___x0;
		uint32_t L_5 = L_4.get_z_2();
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), (int32_t)L_5));
	}
}
// System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_csum_m631B7AE1741D825306BA37F108EFB5BC37DBC20F (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint csum(uint4 x) { return x.x + x.y + x.z + x.w; }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = ___x0;
		uint32_t L_3 = L_2.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = ___x0;
		uint32_t L_5 = L_4.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___x0;
		uint32_t L_7 = L_6.get_w_3();
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), (int32_t)L_5)), (int32_t)L_7));
	}
}
// System.UInt32 Unity.Mathematics.math::hash(System.Void*,System.Int32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m4D2F3D8727152898513C7FF229FE9C8B4A48260F (void* ___pBuffer0, int32_t ___numBytes1, uint32_t ___seed2, const RuntimeMethod* method)
{
	uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * V_0 = NULL;
	uint32_t V_1 = 0;
	uint32_t* V_2 = NULL;
	uint8_t* V_3 = NULL;
	uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  V_4;
	memset((&V_4), 0, sizeof(V_4));
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		// uint4* p = (uint4*)pBuffer;
		void* L_0 = ___pBuffer0;
		V_0 = (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *)L_0;
		// uint hash = seed + Prime5;
		uint32_t L_1 = ___seed2;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)((int32_t)374761393)));
		// if (numBytes >= 16)
		int32_t L_2 = ___numBytes1;
		if ((((int32_t)L_2) < ((int32_t)((int32_t)16))))
		{
			goto IL_00c8;
		}
	}
	{
		// uint4 state = new uint4(Prime1 + Prime2, Prime2, 0, (uint)-Prime1) + seed;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3;
		memset((&L_3), 0, sizeof(L_3));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_3), ((int32_t)606290984), ((int32_t)-2048144777), 0, ((int32_t)1640531535), /*hidden argument*/NULL);
		uint32_t L_4 = ___seed2;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_5 = uint4_op_Addition_m58A8D6BFA5DDA9143D132B57084CB4B271EF1D35_inline(L_3, L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		// int count = numBytes >> 4;
		int32_t L_6 = ___numBytes1;
		V_5 = ((int32_t)((int32_t)L_6>>(int32_t)4));
		// for (int i = 0; i < count; ++i)
		V_6 = 0;
		goto IL_0088;
	}

IL_0039:
	{
		// state += *p++ * Prime2;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_7 = V_4;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * L_8 = V_0;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * L_9 = (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *)L_8;
		uint32_t L_10 = sizeof(uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC );
		V_0 = (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *)((uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *)il2cpp_codegen_add((intptr_t)L_9, (int32_t)L_10));
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_11 = (*(uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *)L_9);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12 = uint4_op_Multiply_m6302785769695A846A3A586E86A3455561C1582B_inline(L_11, ((int32_t)-2048144777), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_13 = uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE_inline(L_7, L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		// state = (state << 13) | (state >> 19);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_14 = V_4;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_15 = uint4_op_LeftShift_m031F4835321567AB31E062A9BBD456FEBBF50405_inline(L_14, ((int32_t)13), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_16 = V_4;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_17 = uint4_op_RightShift_m8714E5CBC739B35AB61645F0E5B2569C2D929DD3_inline(L_16, ((int32_t)19), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_18 = uint4_op_BitwiseOr_mA59ED29C9B5B61C2FAE0846C61049A7D69F1F0C0_inline(L_15, L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		// state *= Prime1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_19 = V_4;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_20 = uint4_op_Multiply_m6302785769695A846A3A586E86A3455561C1582B_inline(L_19, ((int32_t)-1640531535), /*hidden argument*/NULL);
		V_4 = L_20;
		// for (int i = 0; i < count; ++i)
		int32_t L_21 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0088:
	{
		// for (int i = 0; i < count; ++i)
		int32_t L_22 = V_6;
		int32_t L_23 = V_5;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0039;
		}
	}
	{
		// hash = rol(state.x, 1) + rol(state.y, 7) + rol(state.z, 12) + rol(state.w, 18);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_24 = V_4;
		uint32_t L_25 = L_24.get_x_0();
		uint32_t L_26 = math_rol_mE88B527F8CC27B5191D4820D20E356489857BD86_inline(L_25, 1, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_27 = V_4;
		uint32_t L_28 = L_27.get_y_1();
		uint32_t L_29 = math_rol_mE88B527F8CC27B5191D4820D20E356489857BD86_inline(L_28, 7, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_30 = V_4;
		uint32_t L_31 = L_30.get_z_2();
		uint32_t L_32 = math_rol_mE88B527F8CC27B5191D4820D20E356489857BD86_inline(L_31, ((int32_t)12), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_33 = V_4;
		uint32_t L_34 = L_33.get_w_3();
		uint32_t L_35 = math_rol_mE88B527F8CC27B5191D4820D20E356489857BD86_inline(L_34, ((int32_t)18), /*hidden argument*/NULL);
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)L_29)), (int32_t)L_32)), (int32_t)L_35));
	}

IL_00c8:
	{
		// hash += (uint)numBytes;
		uint32_t L_36 = V_1;
		int32_t L_37 = ___numBytes1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_36, (int32_t)L_37));
		// uint* puint = (uint*)p;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * L_38 = V_0;
		V_2 = (uint32_t*)L_38;
		// for (int i = 0; i < ((numBytes >> 2) & 3); ++i)
		V_7 = 0;
		goto IL_00f7;
	}

IL_00d3:
	{
		// hash += *puint++ * Prime3;
		uint32_t L_39 = V_1;
		uint32_t* L_40 = V_2;
		uint32_t* L_41 = (uint32_t*)L_40;
		V_2 = (uint32_t*)((uint32_t*)il2cpp_codegen_add((intptr_t)L_41, (int32_t)4));
		int32_t L_42 = *((uint32_t*)L_41);
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_42, (int32_t)((int32_t)-1028477379)))));
		// hash = rol(hash, 17) * Prime4;
		uint32_t L_43 = V_1;
		uint32_t L_44 = math_rol_mE88B527F8CC27B5191D4820D20E356489857BD86_inline(L_43, ((int32_t)17), /*hidden argument*/NULL);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_44, (int32_t)((int32_t)668265263)));
		// for (int i = 0; i < ((numBytes >> 2) & 3); ++i)
		int32_t L_45 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1));
	}

IL_00f7:
	{
		// for (int i = 0; i < ((numBytes >> 2) & 3); ++i)
		int32_t L_46 = V_7;
		int32_t L_47 = ___numBytes1;
		if ((((int32_t)L_46) < ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_47>>(int32_t)2))&(int32_t)3)))))
		{
			goto IL_00d3;
		}
	}
	{
		// byte* pbyte = (byte*)puint;
		uint32_t* L_48 = V_2;
		V_3 = (uint8_t*)L_48;
		// for (int i = 0; i < ((numBytes) & 3); ++i)
		V_8 = 0;
		goto IL_012b;
	}

IL_0107:
	{
		// hash += (*pbyte++) * Prime5;
		uint32_t L_49 = V_1;
		uint8_t* L_50 = V_3;
		uint8_t* L_51 = (uint8_t*)L_50;
		V_3 = (uint8_t*)((uint8_t*)il2cpp_codegen_add((intptr_t)L_51, (int32_t)1));
		int32_t L_52 = *((uint8_t*)L_51);
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_52, (int32_t)((int32_t)374761393)))));
		// hash = rol(hash, 11) * Prime1;
		uint32_t L_53 = V_1;
		uint32_t L_54 = math_rol_mE88B527F8CC27B5191D4820D20E356489857BD86_inline(L_53, ((int32_t)11), /*hidden argument*/NULL);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_54, (int32_t)((int32_t)-1640531535)));
		// for (int i = 0; i < ((numBytes) & 3); ++i)
		int32_t L_55 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_55, (int32_t)1));
	}

IL_012b:
	{
		// for (int i = 0; i < ((numBytes) & 3); ++i)
		int32_t L_56 = V_8;
		int32_t L_57 = ___numBytes1;
		if ((((int32_t)L_56) < ((int32_t)((int32_t)((int32_t)L_57&(int32_t)3)))))
		{
			goto IL_0107;
		}
	}
	{
		// hash ^= hash >> 15;
		uint32_t L_58 = V_1;
		uint32_t L_59 = V_1;
		V_1 = ((int32_t)((int32_t)L_58^(int32_t)((int32_t)((uint32_t)L_59>>((int32_t)15)))));
		// hash *= Prime2;
		uint32_t L_60 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_60, (int32_t)((int32_t)-2048144777)));
		// hash ^= hash >> 13;
		uint32_t L_61 = V_1;
		uint32_t L_62 = V_1;
		V_1 = ((int32_t)((int32_t)L_61^(int32_t)((int32_t)((uint32_t)L_62>>((int32_t)13)))));
		// hash *= Prime3;
		uint32_t L_63 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_63, (int32_t)((int32_t)-1028477379)));
		// hash ^= hash >> 16;
		uint32_t L_64 = V_1;
		uint32_t L_65 = V_1;
		V_1 = ((int32_t)((int32_t)L_64^(int32_t)((int32_t)((uint32_t)L_65>>((int32_t)16)))));
		// return hash;
		uint32_t L_66 = V_1;
		return L_66;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::mul(Unity.Mathematics.float4x4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  math_mul_m37BCAA6C9FA01CCC5E8776F95EB14FAE8EDFAF86 (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___a0, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___b1, const RuntimeMethod* method)
{
	{
		// return a.c0 * b.x + a.c1 * b.y + a.c2 * b.z + a.c3 * b.w;
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_0 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = L_0.get_c0_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_2 = ___b1;
		float L_3 = L_2.get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_4 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_1, L_3, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_5 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = L_5.get_c1_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_7 = ___b1;
		float L_8 = L_7.get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_9 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_6, L_8, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_10 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_4, L_9, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_11 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_12 = L_11.get_c2_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_13 = ___b1;
		float L_14 = L_13.get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_15 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_12, L_14, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_16 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_10, L_15, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_17 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_18 = L_17.get_c3_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_19 = ___b1;
		float L_20 = L_19.get_w_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_21 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_18, L_20, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_22 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_16, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.math::mul(Unity.Mathematics.float4x4,Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  math_mul_m3B547595438404CE64BDF1410080BCC73017F892 (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___a0, float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___b1, const RuntimeMethod* method)
{
	{
		// return float4x4(
		//     a.c0 * b.c0.x + a.c1 * b.c0.y + a.c2 * b.c0.z + a.c3 * b.c0.w,
		//     a.c0 * b.c1.x + a.c1 * b.c1.y + a.c2 * b.c1.z + a.c3 * b.c1.w,
		//     a.c0 * b.c2.x + a.c1 * b.c2.y + a.c2 * b.c2.z + a.c3 * b.c2.w,
		//     a.c0 * b.c3.x + a.c1 * b.c3.y + a.c2 * b.c3.z + a.c3 * b.c3.w);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_0 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = L_0.get_c0_0();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_2 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_3 = L_2.get_c0_0();
		float L_4 = L_3.get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_5 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_1, L_4, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_6 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_7 = L_6.get_c1_1();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_8 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_9 = L_8.get_c0_0();
		float L_10 = L_9.get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_11 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_7, L_10, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_12 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_5, L_11, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_13 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_14 = L_13.get_c2_2();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_15 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_16 = L_15.get_c0_0();
		float L_17 = L_16.get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_18 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_14, L_17, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_19 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_12, L_18, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_20 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_21 = L_20.get_c3_3();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_22 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_23 = L_22.get_c0_0();
		float L_24 = L_23.get_w_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_25 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_21, L_24, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_26 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_19, L_25, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_27 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_28 = L_27.get_c0_0();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_29 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_30 = L_29.get_c1_1();
		float L_31 = L_30.get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_32 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_28, L_31, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_33 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_34 = L_33.get_c1_1();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_35 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_36 = L_35.get_c1_1();
		float L_37 = L_36.get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_38 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_34, L_37, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_39 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_32, L_38, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_40 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_41 = L_40.get_c2_2();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_42 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_43 = L_42.get_c1_1();
		float L_44 = L_43.get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_45 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_41, L_44, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_46 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_39, L_45, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_47 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_48 = L_47.get_c3_3();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_49 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_50 = L_49.get_c1_1();
		float L_51 = L_50.get_w_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_52 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_48, L_51, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_53 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_46, L_52, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_54 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_55 = L_54.get_c0_0();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_56 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_57 = L_56.get_c2_2();
		float L_58 = L_57.get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_59 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_55, L_58, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_60 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_61 = L_60.get_c1_1();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_62 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_63 = L_62.get_c2_2();
		float L_64 = L_63.get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_65 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_61, L_64, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_66 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_59, L_65, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_67 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_68 = L_67.get_c2_2();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_69 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_70 = L_69.get_c2_2();
		float L_71 = L_70.get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_72 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_68, L_71, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_73 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_66, L_72, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_74 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_75 = L_74.get_c3_3();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_76 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_77 = L_76.get_c2_2();
		float L_78 = L_77.get_w_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_79 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_75, L_78, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_80 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_73, L_79, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_81 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_82 = L_81.get_c0_0();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_83 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_84 = L_83.get_c3_3();
		float L_85 = L_84.get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_86 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_82, L_85, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_87 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_88 = L_87.get_c1_1();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_89 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_90 = L_89.get_c3_3();
		float L_91 = L_90.get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_92 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_88, L_91, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_93 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_86, L_92, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_94 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_95 = L_94.get_c2_2();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_96 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_97 = L_96.get_c3_3();
		float L_98 = L_97.get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_99 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_95, L_98, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_100 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_93, L_99, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_101 = ___a0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_102 = L_101.get_c3_3();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_103 = ___b1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_104 = L_103.get_c3_3();
		float L_105 = L_104.get_w_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_106 = float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline(L_102, L_105, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_107 = float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline(L_100, L_106, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_108 = math_float4x4_mDBFE1BFDCF3B3290DEA76F08CBFEF2537A5C60E1_inline(L_26, L_53, L_80, L_107, /*hidden argument*/NULL);
		return L_108;
	}
}
// Unity.Mathematics.uint3 Unity.Mathematics.math::uint3(System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  math_uint3_m267A853D616F9ECD652755897C37C40C97EF2606 (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	{
		// public static uint3 uint3(uint x, uint y, uint z) { return new uint3(x, y, z); }
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint32_t L_2 = ___z2;
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_3;
		memset((&L_3), 0, sizeof(L_3));
		uint3__ctor_m83EC50FF1E3D05E4220E4D77C4D2209FE1BDEE37_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_mCDE6963B79DB564C76CD37630D658920BFA586B9 (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(v * uint3(0xCD266C89u, 0xF1852A33u, 0x77E35E77u)) + 0x863E3729u;
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_0 = ___v0;
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_1 = math_uint3_m267A853D616F9ECD652755897C37C40C97EF2606_inline(((int32_t)-853119863), ((int32_t)-242931149), ((int32_t)2011389559), /*hidden argument*/NULL);
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_2 = uint3_op_Multiply_mC75EC5D3A910BC87EC728F1D02D9766F6CBB5E44_inline(L_0, L_1, /*hidden argument*/NULL);
		uint32_t L_3 = math_csum_mE0AA784B28235A836A585BE089A0D71C66087888_inline(L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)-2042742999)));
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.math::uint4(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method)
{
	{
		// public static uint4 uint4(uint x, uint y, uint z, uint w) { return new uint4(x, y, z, w); }
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint32_t L_2 = ___z2;
		uint32_t L_3 = ___w3;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4;
		memset((&L_4), 0, sizeof(L_4));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m1DF44299A6DEA06F77DA49DFA00039E341DB3F33 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(v * uint4(0xB492BF15u, 0xD37220E3u, 0x7AA2C2BDu, 0xE16BC89Du)) + 0x7AA07CD3u;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___v0;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_1 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)-1265451243), ((int32_t)-747495197), ((int32_t)2057487037), ((int32_t)-513029987), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_0, L_1, /*hidden argument*/NULL);
		uint32_t L_3 = math_csum_m631B7AE1741D825306BA37F108EFB5BC37DBC20F_inline(L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)2057338067)));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.uint3::.ctor(System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void uint3__ctor_m83EC50FF1E3D05E4220E4D77C4D2209FE1BDEE37 (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		uint32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		uint32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		uint32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void uint3__ctor_m83EC50FF1E3D05E4220E4D77C4D2209FE1BDEE37_AdjustorThunk (RuntimeObject * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * _thisAdjusted = reinterpret_cast<uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F *>(__this + _offset);
	uint3__ctor_m83EC50FF1E3D05E4220E4D77C4D2209FE1BDEE37_inline(_thisAdjusted, ___x0, ___y1, ___z2, method);
}
// Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_Multiply(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  uint3_op_Multiply_mC75EC5D3A910BC87EC728F1D02D9766F6CBB5E44 (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___lhs0, uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint3 operator * (uint3 lhs, uint3 rhs) { return new uint3 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z); }
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint3__ctor_m83EC50FF1E3D05E4220E4D77C4D2209FE1BDEE37_inline((&L_12), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean Unity.Mathematics.uint3::Equals(Unity.Mathematics.uint3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint3_Equals_mF1A43BF5E958201193205C727DE277A13E8A7877 (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(uint3 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z; }
		uint32_t L_0 = __this->get_x_0();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_1 = ___rhs0;
		uint32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_002b;
		}
	}
	{
		uint32_t L_3 = __this->get_y_1();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_4 = ___rhs0;
		uint32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		uint32_t L_6 = __this->get_z_2();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_7 = ___rhs0;
		uint32_t L_8 = L_7.get_z_2();
		return (bool)((((int32_t)L_6) == ((int32_t)L_8))? 1 : 0);
	}

IL_002b:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool uint3_Equals_mF1A43BF5E958201193205C727DE277A13E8A7877_AdjustorThunk (RuntimeObject * __this, uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * _thisAdjusted = reinterpret_cast<uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F *>(__this + _offset);
	return uint3_Equals_mF1A43BF5E958201193205C727DE277A13E8A7877_inline(_thisAdjusted, ___rhs0, method);
}
// System.Boolean Unity.Mathematics.uint3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint3_Equals_mD0F46E10E9BBF31279855DAFE2325C6038C0E6D3 (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uint3_Equals_mD0F46E10E9BBF31279855DAFE2325C6038C0E6D3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((uint3)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1 = uint3_Equals_mF1A43BF5E958201193205C727DE277A13E8A7877_inline((uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F *)__this, ((*(uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F *)((uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F *)UnBox(L_0, uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool uint3_Equals_mD0F46E10E9BBF31279855DAFE2325C6038C0E6D3_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * _thisAdjusted = reinterpret_cast<uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F *>(__this + _offset);
	return uint3_Equals_mD0F46E10E9BBF31279855DAFE2325C6038C0E6D3(_thisAdjusted, ___o0, method);
}
// System.Int32 Unity.Mathematics.uint3::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t uint3_GetHashCode_mB20D3856AF84E5AA5A7222DE08C7F21CA127039D (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_0 = (*(uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F *)__this);
		uint32_t L_1 = math_hash_mCDE6963B79DB564C76CD37630D658920BFA586B9_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t uint3_GetHashCode_mB20D3856AF84E5AA5A7222DE08C7F21CA127039D_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * _thisAdjusted = reinterpret_cast<uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F *>(__this + _offset);
	return uint3_GetHashCode_mB20D3856AF84E5AA5A7222DE08C7F21CA127039D_inline(_thisAdjusted, method);
}
// System.String Unity.Mathematics.uint3::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* uint3_ToString_m02FAB1C4D7D9A4831F7D0D78AF975B8C4B7EF4FD (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uint3_ToString_m02FAB1C4D7D9A4831F7D0D78AF975B8C4B7EF4FD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint3({0}, {1}, {2})", x, y, z);
		uint32_t L_0 = __this->get_x_0();
		uint32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_1);
		uint32_t L_3 = __this->get_y_1();
		uint32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_4);
		uint32_t L_6 = __this->get_z_2();
		uint32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9 = String_Format_m26BBF75F9609FAD0B39C2242FEBAAD7D68F14D99(_stringLiteral5DBD030B94725EF92B6F4BDDEB5A06DB312AF764, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_EXTERN_C  String_t* uint3_ToString_m02FAB1C4D7D9A4831F7D0D78AF975B8C4B7EF4FD_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * _thisAdjusted = reinterpret_cast<uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F *>(__this + _offset);
	return uint3_ToString_m02FAB1C4D7D9A4831F7D0D78AF975B8C4B7EF4FD_inline(_thisAdjusted, method);
}
// System.String Unity.Mathematics.uint3::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* uint3_ToString_m7705E589BDC0FCC9D87428DEFD95A66EF4A7A6AF (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uint3_ToString_m7705E589BDC0FCC9D87428DEFD95A66EF4A7A6AF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint3({0}, {1}, {2})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider));
		uint32_t* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_0, L_1, L_2, /*hidden argument*/NULL);
		uint32_t* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_4, L_5, L_6, /*hidden argument*/NULL);
		uint32_t* L_8 = __this->get_address_of_z_2();
		String_t* L_9 = ___format0;
		RuntimeObject* L_10 = ___formatProvider1;
		String_t* L_11 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_8, L_9, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Format_m26BBF75F9609FAD0B39C2242FEBAAD7D68F14D99(_stringLiteral5DBD030B94725EF92B6F4BDDEB5A06DB312AF764, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C  String_t* uint3_ToString_m7705E589BDC0FCC9D87428DEFD95A66EF4A7A6AF_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * _thisAdjusted = reinterpret_cast<uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F *>(__this + _offset);
	return uint3_ToString_m7705E589BDC0FCC9D87428DEFD95A66EF4A7A6AF_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.uint4::.ctor(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		uint32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		uint32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		uint32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		uint32_t L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_AdjustorThunk (RuntimeObject * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * _thisAdjusted = reinterpret_cast<uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *>(__this + _offset);
	uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Multiply(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator * (uint4 lhs, uint4 rhs) { return new uint4 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z, lhs.w * rhs.w); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12 = ___lhs0;
		uint32_t L_13 = L_12.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_14 = ___rhs1;
		uint32_t L_15 = L_14.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_16;
		memset((&L_16), 0, sizeof(L_16));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_16), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Multiply(Unity.Mathematics.uint4,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Multiply_m6302785769695A846A3A586E86A3455561C1582B (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint32_t ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator * (uint4 lhs, uint rhs) { return new uint4 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint32_t L_2 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = ___lhs0;
		uint32_t L_4 = L_3.get_y_1();
		uint32_t L_5 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___lhs0;
		uint32_t L_7 = L_6.get_z_2();
		uint32_t L_8 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_9 = ___lhs0;
		uint32_t L_10 = L_9.get_w_3();
		uint32_t L_11 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_12), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_2)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_4, (int32_t)L_5)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_7, (int32_t)L_8)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_10, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Addition(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator + (uint4 lhs, uint4 rhs) { return new uint4 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12 = ___lhs0;
		uint32_t L_13 = L_12.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_14 = ___rhs1;
		uint32_t L_15 = L_14.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_16;
		memset((&L_16), 0, sizeof(L_16));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_16), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Addition(Unity.Mathematics.uint4,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Addition_m58A8D6BFA5DDA9143D132B57084CB4B271EF1D35 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint32_t ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator + (uint4 lhs, uint rhs) { return new uint4 (lhs.x + rhs, lhs.y + rhs, lhs.z + rhs, lhs.w + rhs); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint32_t L_2 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = ___lhs0;
		uint32_t L_4 = L_3.get_y_1();
		uint32_t L_5 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___lhs0;
		uint32_t L_7 = L_6.get_z_2();
		uint32_t L_8 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_9 = ___lhs0;
		uint32_t L_10 = L_9.get_w_3();
		uint32_t L_11 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_12), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_2)), ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5)), ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8)), ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_LeftShift(Unity.Mathematics.uint4,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_LeftShift_m031F4835321567AB31E062A9BBD456FEBBF50405 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___x0, int32_t ___n1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator << (uint4 x, int n) { return new uint4 (x.x << n, x.y << n, x.z << n, x.w << n); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		int32_t L_2 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = ___x0;
		uint32_t L_4 = L_3.get_y_1();
		int32_t L_5 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___x0;
		uint32_t L_7 = L_6.get_z_2();
		int32_t L_8 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_9 = ___x0;
		uint32_t L_10 = L_9.get_w_3();
		int32_t L_11 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_12), ((int32_t)((int32_t)L_1<<(int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)31))))), ((int32_t)((int32_t)L_4<<(int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)31))))), ((int32_t)((int32_t)L_7<<(int32_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)31))))), ((int32_t)((int32_t)L_10<<(int32_t)((int32_t)((int32_t)L_11&(int32_t)((int32_t)31))))), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_RightShift(Unity.Mathematics.uint4,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_RightShift_m8714E5CBC739B35AB61645F0E5B2569C2D929DD3 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___x0, int32_t ___n1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator >> (uint4 x, int n) { return new uint4 (x.x >> n, x.y >> n, x.z >> n, x.w >> n); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		int32_t L_2 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = ___x0;
		uint32_t L_4 = L_3.get_y_1();
		int32_t L_5 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___x0;
		uint32_t L_7 = L_6.get_z_2();
		int32_t L_8 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_9 = ___x0;
		uint32_t L_10 = L_9.get_w_3();
		int32_t L_11 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_12), ((int32_t)((uint32_t)L_1>>((int32_t)((int32_t)L_2&(int32_t)((int32_t)31))))), ((int32_t)((uint32_t)L_4>>((int32_t)((int32_t)L_5&(int32_t)((int32_t)31))))), ((int32_t)((uint32_t)L_7>>((int32_t)((int32_t)L_8&(int32_t)((int32_t)31))))), ((int32_t)((uint32_t)L_10>>((int32_t)((int32_t)L_11&(int32_t)((int32_t)31))))), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_BitwiseOr(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_BitwiseOr_mA59ED29C9B5B61C2FAE0846C61049A7D69F1F0C0 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator | (uint4 lhs, uint4 rhs) { return new uint4 (lhs.x | rhs.x, lhs.y | rhs.y, lhs.z | rhs.z, lhs.w | rhs.w); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12 = ___lhs0;
		uint32_t L_13 = L_12.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_14 = ___rhs1;
		uint32_t L_15 = L_14.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_16;
		memset((&L_16), 0, sizeof(L_16));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_16), ((int32_t)((int32_t)L_1|(int32_t)L_3)), ((int32_t)((int32_t)L_5|(int32_t)L_7)), ((int32_t)((int32_t)L_9|(int32_t)L_11)), ((int32_t)((int32_t)L_13|(int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Boolean Unity.Mathematics.uint4::Equals(Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint4_Equals_m302069717D95B95C972C7C1B7D29D9E0DBBD6542 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(uint4 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w; }
		uint32_t L_0 = __this->get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_1 = ___rhs0;
		uint32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_3 = __this->get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = ___rhs0;
		uint32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_6 = __this->get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_7 = ___rhs0;
		uint32_t L_8 = L_7.get_z_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_9 = __this->get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_10 = ___rhs0;
		uint32_t L_11 = L_10.get_w_3();
		return (bool)((((int32_t)L_9) == ((int32_t)L_11))? 1 : 0);
	}

IL_0039:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool uint4_Equals_m302069717D95B95C972C7C1B7D29D9E0DBBD6542_AdjustorThunk (RuntimeObject * __this, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * _thisAdjusted = reinterpret_cast<uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *>(__this + _offset);
	return uint4_Equals_m302069717D95B95C972C7C1B7D29D9E0DBBD6542_inline(_thisAdjusted, ___rhs0, method);
}
// System.Boolean Unity.Mathematics.uint4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint4_Equals_mA3312E8FC1A4734E2402424ADAC7BCE386918488 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uint4_Equals_mA3312E8FC1A4734E2402424ADAC7BCE386918488_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((uint4)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1 = uint4_Equals_m302069717D95B95C972C7C1B7D29D9E0DBBD6542_inline((uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *)__this, ((*(uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *)((uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *)UnBox(L_0, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool uint4_Equals_mA3312E8FC1A4734E2402424ADAC7BCE386918488_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * _thisAdjusted = reinterpret_cast<uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *>(__this + _offset);
	return uint4_Equals_mA3312E8FC1A4734E2402424ADAC7BCE386918488(_thisAdjusted, ___o0, method);
}
// System.Int32 Unity.Mathematics.uint4::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t uint4_GetHashCode_m187968010316E4D76A2FD32E8CD068A299007AA8 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = (*(uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *)__this);
		uint32_t L_1 = math_hash_m1DF44299A6DEA06F77DA49DFA00039E341DB3F33_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t uint4_GetHashCode_m187968010316E4D76A2FD32E8CD068A299007AA8_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * _thisAdjusted = reinterpret_cast<uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *>(__this + _offset);
	return uint4_GetHashCode_m187968010316E4D76A2FD32E8CD068A299007AA8_inline(_thisAdjusted, method);
}
// System.String Unity.Mathematics.uint4::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* uint4_ToString_m1ADFBF43F0EC44A59D7F9908C9891DC3C31690BC (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uint4_ToString_m1ADFBF43F0EC44A59D7F9908C9891DC3C31690BC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint4({0}, {1}, {2}, {3})", x, y, z, w);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		uint32_t L_2 = __this->get_x_0();
		uint32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_1;
		uint32_t L_6 = __this->get_y_1();
		uint32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_5;
		uint32_t L_10 = __this->get_z_2();
		uint32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_9;
		uint32_t L_14 = __this->get_w_3();
		uint32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral901D0D2D30D5C7F19A6CF4A9C689EBAFE3B3FB54, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
IL2CPP_EXTERN_C  String_t* uint4_ToString_m1ADFBF43F0EC44A59D7F9908C9891DC3C31690BC_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * _thisAdjusted = reinterpret_cast<uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *>(__this + _offset);
	return uint4_ToString_m1ADFBF43F0EC44A59D7F9908C9891DC3C31690BC_inline(_thisAdjusted, method);
}
// System.String Unity.Mathematics.uint4::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* uint4_ToString_mED39980F32C81D913660EC644111F2070E7870A2 (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uint4_ToString_mED39980F32C81D913660EC644111F2070E7870A2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint4({0}, {1}, {2}, {3})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider), w.ToString(format, formatProvider));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		uint32_t* L_2 = __this->get_address_of_x_0();
		String_t* L_3 = ___format0;
		RuntimeObject* L_4 = ___formatProvider1;
		String_t* L_5 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_1;
		uint32_t* L_7 = __this->get_address_of_y_1();
		String_t* L_8 = ___format0;
		RuntimeObject* L_9 = ___formatProvider1;
		String_t* L_10 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_6;
		uint32_t* L_12 = __this->get_address_of_z_2();
		String_t* L_13 = ___format0;
		RuntimeObject* L_14 = ___formatProvider1;
		String_t* L_15 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = L_11;
		uint32_t* L_17 = __this->get_address_of_w_3();
		String_t* L_18 = ___format0;
		RuntimeObject* L_19 = ___formatProvider1;
		String_t* L_20 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral901D0D2D30D5C7F19A6CF4A9C689EBAFE3B3FB54, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
IL2CPP_EXTERN_C  String_t* uint4_ToString_mED39980F32C81D913660EC644111F2070E7870A2_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * _thisAdjusted = reinterpret_cast<uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *>(__this + _offset);
	return uint4_ToString_mED39980F32C81D913660EC644111F2070E7870A2_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool float3_Equals_mD9E67316EB63B276A4E7D47E0DA25377FC21C129_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float3 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z; }
		float L_0 = __this->get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_1 = ___rhs0;
		float L_2 = L_1.get_x_0();
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_002b;
		}
	}
	{
		float L_3 = __this->get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___rhs0;
		float L_5 = L_4.get_y_1();
		if ((!(((float)L_3) == ((float)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		float L_6 = __this->get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_7 = ___rhs0;
		float L_8 = L_7.get_z_2();
		return (bool)((((float)L_6) == ((float)L_8))? 1 : 0);
	}

IL_002b:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_hash_m2502AF91810C2607571B2000ECA8A7370055FEF5_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint3(0x9B13B92Du, 0x4ABF0813u, 0x86068063u)) + 0xD75513F9u;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___v0;
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_1 = math_asuint_mAF6BF0C9EDA601A8692DB7BD3B443BFFA8A7C234_inline(L_0, /*hidden argument*/NULL);
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_2 = math_uint3_m267A853D616F9ECD652755897C37C40C97EF2606_inline(((int32_t)-1693206227), ((int32_t)1254033427), ((int32_t)-2046394269), /*hidden argument*/NULL);
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_3 = uint3_op_Multiply_mC75EC5D3A910BC87EC728F1D02D9766F6CBB5E44_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4 = math_csum_mE0AA784B28235A836A585BE089A0D71C66087888_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-682290183)));
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t float3_GetHashCode_mA8A92E15BE1FE55D94575007DAF37C6AB9379C24_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = (*(float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 *)__this);
		uint32_t L_1 = math_hash_m2502AF91810C2607571B2000ECA8A7370055FEF5_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float3_ToString_mB4863B6E2512E53544B02772705EBF221D744416_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float3_ToString_mB4863B6E2512E53544B02772705EBF221D744416Unity_Mathematics_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3({0}f, {1}f, {2}f)", x, y, z);
		float L_0 = __this->get_x_0();
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_1);
		float L_3 = __this->get_y_1();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_4);
		float L_6 = __this->get_z_2();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9 = String_Format_m26BBF75F9609FAD0B39C2242FEBAAD7D68F14D99(_stringLiteralDF02D7418655748837E0CFA19E962079704EDA2F, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float3_ToString_m513168CE73E3A1943FBF999282E27D81DB726E1C_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float3_ToString_m513168CE73E3A1943FBF999282E27D81DB726E1CUnity_Mathematics_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3({0}f, {1}f, {2}f)", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider));
		float* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_0, L_1, L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_4, L_5, L_6, /*hidden argument*/NULL);
		float* L_8 = __this->get_address_of_z_2();
		String_t* L_9 = ___format0;
		RuntimeObject* L_10 = ___formatProvider1;
		String_t* L_11 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_8, L_9, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Format_m26BBF75F9609FAD0B39C2242FEBAAD7D68F14D99(_stringLiteralDF02D7418655748837E0CFA19E962079704EDA2F, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void float4__ctor_mC873E36671C90EFA5B0F4513EEA6D79B0E4D4333_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___xyz0, float ___w1, const RuntimeMethod* method)
{
	{
		// this.x = xyz.x;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___xyz0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xyz.y;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_2 = ___xyz0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = xyz.z;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___xyz0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(L_5);
		// this.w = w;
		float L_6 = ___w1;
		__this->set_w_3(L_6);
		// }
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  float4_get_xyz_mC5FD59B33F4C3F7DA22E8C0F0B1FFC2C38A1F8DB_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(x, y, z); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float L_2 = __this->get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float4 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w; }
		float L_0 = __this->get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = ___rhs0;
		float L_2 = L_1.get_x_0();
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_0039;
		}
	}
	{
		float L_3 = __this->get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_4 = ___rhs0;
		float L_5 = L_4.get_y_1();
		if ((!(((float)L_3) == ((float)L_5))))
		{
			goto IL_0039;
		}
	}
	{
		float L_6 = __this->get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_7 = ___rhs0;
		float L_8 = L_7.get_z_2();
		if ((!(((float)L_6) == ((float)L_8))))
		{
			goto IL_0039;
		}
	}
	{
		float L_9 = __this->get_w_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_10 = ___rhs0;
		float L_11 = L_10.get_w_3();
		return (bool)((((float)L_9) == ((float)L_11))? 1 : 0);
	}

IL_0039:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_hash_m922609CC35709A98A21766DAB7DD2B0978C5D104_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint4(0xE69626FFu, 0xBD010EEBu, 0x9CEDE1D1u, 0x43BE0B51u)) + 0xAF836EE1u;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___v0;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_1 = math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline(L_0, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)-426367233), ((int32_t)-1124004117), ((int32_t)-1662131759), ((int32_t)1136528209), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4 = math_csum_m631B7AE1741D825306BA37F108EFB5BC37DBC20F_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-1350340895)));
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t float4_GetHashCode_m9510EAC95094BD49DF7CF63937B46669614AB277_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = (*(float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)__this);
		uint32_t L_1 = math_hash_m922609CC35709A98A21766DAB7DD2B0978C5D104_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float4_ToString_m8D534BBA43D7804CD31E26FE4C1972284DE35FBC_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float4_ToString_m8D534BBA43D7804CD31E26FE4C1972284DE35FBCUnity_Mathematics_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4({0}f, {1}f, {2}f, {3}f)", x, y, z, w);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		float L_2 = __this->get_x_0();
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_1;
		float L_6 = __this->get_y_1();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_5;
		float L_10 = __this->get_z_2();
		float L_11 = L_10;
		RuntimeObject * L_12 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_9;
		float L_14 = __this->get_w_3();
		float L_15 = L_14;
		RuntimeObject * L_16 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral2211E2D567B9991441FFEFBE5AAA03456AB8ABCA, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float4_ToString_m92F4A00D57F53DC94183F09F72ED6CAC1F390650_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float4_ToString_m92F4A00D57F53DC94183F09F72ED6CAC1F390650Unity_Mathematics_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4({0}f, {1}f, {2}f, {3}f)", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider), w.ToString(format, formatProvider));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		float* L_2 = __this->get_address_of_x_0();
		String_t* L_3 = ___format0;
		RuntimeObject* L_4 = ___formatProvider1;
		String_t* L_5 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_1;
		float* L_7 = __this->get_address_of_y_1();
		String_t* L_8 = ___format0;
		RuntimeObject* L_9 = ___formatProvider1;
		String_t* L_10 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_6;
		float* L_12 = __this->get_address_of_z_2();
		String_t* L_13 = ___format0;
		RuntimeObject* L_14 = ___formatProvider1;
		String_t* L_15 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = L_11;
		float* L_17 = __this->get_address_of_w_3();
		String_t* L_18 = ___format0;
		RuntimeObject* L_19 = ___formatProvider1;
		String_t* L_20 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral2211E2D567B9991441FFEFBE5AAA03456AB8ABCA, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void float4x4__ctor_m9A2D24D07FF8DCB7495AC3FD76364FD3FB81D807_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c00, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c11, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c22, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c33, const RuntimeMethod* method)
{
	{
		// this.c0 = c0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___c00;
		__this->set_c0_0(L_0);
		// this.c1 = c1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = ___c11;
		__this->set_c1_1(L_1);
		// this.c2 = c2;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_2 = ___c22;
		__this->set_c2_2(L_2);
		// this.c3 = c3;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_3 = ___c33;
		__this->set_c3_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void float4x4__ctor_m9C451E986E9271E34E011C51F35160C8E9E6CF98_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, float ___m000, float ___m011, float ___m022, float ___m033, float ___m104, float ___m115, float ___m126, float ___m137, float ___m208, float ___m219, float ___m2210, float ___m2311, float ___m3012, float ___m3113, float ___m3214, float ___m3315, const RuntimeMethod* method)
{
	{
		// this.c0 = new float4(m00, m10, m20, m30);
		float L_0 = ___m000;
		float L_1 = ___m104;
		float L_2 = ___m208;
		float L_3 = ___m3012;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_c0_0(L_4);
		// this.c1 = new float4(m01, m11, m21, m31);
		float L_5 = ___m011;
		float L_6 = ___m115;
		float L_7 = ___m219;
		float L_8 = ___m3113;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_9), L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->set_c1_1(L_9);
		// this.c2 = new float4(m02, m12, m22, m32);
		float L_10 = ___m022;
		float L_11 = ___m126;
		float L_12 = ___m2210;
		float L_13 = ___m3214;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_14;
		memset((&L_14), 0, sizeof(L_14));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_14), L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		__this->set_c2_2(L_14);
		// this.c3 = new float4(m03, m13, m23, m33);
		float L_15 = ___m033;
		float L_16 = ___m137;
		float L_17 = ___m2311;
		float L_18 = ___m3315;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_19;
		memset((&L_19), 0, sizeof(L_19));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_19), L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		__this->set_c3_3(L_19);
		// }
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool float4x4_Equals_m78EF9656D285326360D7957143A42017BE85AD96_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float4x4 rhs) { return c0.Equals(rhs.c0) && c1.Equals(rhs.c1) && c2.Equals(rhs.c2) && c3.Equals(rhs.c3); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_0 = __this->get_address_of_c0_0();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_1 = ___rhs0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_2 = L_1.get_c0_0();
		bool L_3 = float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline((float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_4 = __this->get_address_of_c1_1();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_5 = ___rhs0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = L_5.get_c1_1();
		bool L_7 = float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline((float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_8 = __this->get_address_of_c2_2();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_9 = ___rhs0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_10 = L_9.get_c2_2();
		bool L_11 = float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline((float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004b;
		}
	}
	{
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_12 = __this->get_address_of_c3_3();
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_13 = ___rhs0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_14 = L_13.get_c3_3();
		bool L_15 = float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_inline((float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A *)L_12, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_004b:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_hash_m8E61D111AA597992FA0D8C6D5FBE3FBC7B417B25_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v.c0) * uint4(0xC4B1493Fu, 0xBA0966D3u, 0xAFBEE253u, 0x5B419C01u) +
		//             asuint(v.c1) * uint4(0x515D90F5u, 0xEC9F68F3u, 0xF9EA92D5u, 0xC2FAFCB9u) +
		//             asuint(v.c2) * uint4(0x616E9CA1u, 0xC5C5394Bu, 0xCAE78587u, 0x7A1541C9u) +
		//             asuint(v.c3) * uint4(0xF83BD927u, 0x6A243BCBu, 0x509B84C9u, 0x91D13847u)) + 0x52F7230Fu;
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_0 = ___v0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = L_0.get_c0_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline(L_1, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)-995014337), ((int32_t)-1173788973), ((int32_t)-1346444717), ((int32_t)1531026433), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_2, L_3, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_5 = ___v0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = L_5.get_c1_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_7 = math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline(L_6, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_8 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)1365086453), ((int32_t)-325097229), ((int32_t)-102067499), ((int32_t)-1023738695), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_9 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_7, L_8, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_10 = uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE_inline(L_4, L_9, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_11 = ___v0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_12 = L_11.get_c2_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_13 = math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline(L_12, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_14 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)1634639009), ((int32_t)-976930485), ((int32_t)-890796665), ((int32_t)2048213449), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_15 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_13, L_14, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_16 = uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE_inline(L_10, L_15, /*hidden argument*/NULL);
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_17 = ___v0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_18 = L_17.get_c3_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_19 = math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline(L_18, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_20 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)-130295513), ((int32_t)1780759499), ((int32_t)1352369353), ((int32_t)-1848559545), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_21 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_19, L_20, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_22 = uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE_inline(L_16, L_21, /*hidden argument*/NULL);
		uint32_t L_23 = math_csum_m631B7AE1741D825306BA37F108EFB5BC37DBC20F_inline(L_22, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)((int32_t)1391928079)));
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t float4x4_GetHashCode_m35D91773F446B0D620E5B5E04BFF72BE8BDDD0F1_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_0 = (*(float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC *)__this);
		uint32_t L_1 = math_hash_m8E61D111AA597992FA0D8C6D5FBE3FBC7B417B25_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float4x4_ToString_m782366095AD7C5C4144B9A7EE0113328AAEC908B_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float4x4_ToString_m782366095AD7C5C4144B9A7EE0113328AAEC908BUnity_Mathematics_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4x4({0}f, {1}f, {2}f, {3}f,  {4}f, {5}f, {6}f, {7}f,  {8}f, {9}f, {10}f, {11}f,  {12}f, {13}f, {14}f, {15}f)", c0.x, c1.x, c2.x, c3.x, c0.y, c1.y, c2.y, c3.y, c0.z, c1.z, c2.z, c3.z, c0.w, c1.w, c2.w, c3.w);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_2 = __this->get_address_of_c0_0();
		float L_3 = L_2->get_x_0();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_7 = __this->get_address_of_c1_1();
		float L_8 = L_7->get_x_0();
		float L_9 = L_8;
		RuntimeObject * L_10 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_6;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_12 = __this->get_address_of_c2_2();
		float L_13 = L_12->get_x_0();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = L_11;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_17 = __this->get_address_of_c3_3();
		float L_18 = L_17->get_x_0();
		float L_19 = L_18;
		RuntimeObject * L_20 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_21 = L_16;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_22 = __this->get_address_of_c0_0();
		float L_23 = L_22->get_y_1();
		float L_24 = L_23;
		RuntimeObject * L_25 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_25);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_25);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_26 = L_21;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_27 = __this->get_address_of_c1_1();
		float L_28 = L_27->get_y_1();
		float L_29 = L_28;
		RuntimeObject * L_30 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_30);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_30);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_31 = L_26;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_32 = __this->get_address_of_c2_2();
		float L_33 = L_32->get_y_1();
		float L_34 = L_33;
		RuntimeObject * L_35 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_35);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_35);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_36 = L_31;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_37 = __this->get_address_of_c3_3();
		float L_38 = L_37->get_y_1();
		float L_39 = L_38;
		RuntimeObject * L_40 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_40);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_40);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_41 = L_36;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_42 = __this->get_address_of_c0_0();
		float L_43 = L_42->get_z_2();
		float L_44 = L_43;
		RuntimeObject * L_45 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_45);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_45);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_46 = L_41;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_47 = __this->get_address_of_c1_1();
		float L_48 = L_47->get_z_2();
		float L_49 = L_48;
		RuntimeObject * L_50 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_46);
		ArrayElementTypeCheck (L_46, L_50);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)L_50);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_51 = L_46;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_52 = __this->get_address_of_c2_2();
		float L_53 = L_52->get_z_2();
		float L_54 = L_53;
		RuntimeObject * L_55 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, L_55);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_55);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_56 = L_51;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_57 = __this->get_address_of_c3_3();
		float L_58 = L_57->get_z_2();
		float L_59 = L_58;
		RuntimeObject * L_60 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_59);
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_60);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)L_60);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_61 = L_56;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_62 = __this->get_address_of_c0_0();
		float L_63 = L_62->get_w_3();
		float L_64 = L_63;
		RuntimeObject * L_65 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_65);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_65);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_66 = L_61;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_67 = __this->get_address_of_c1_1();
		float L_68 = L_67->get_w_3();
		float L_69 = L_68;
		RuntimeObject * L_70 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_69);
		NullCheck(L_66);
		ArrayElementTypeCheck (L_66, L_70);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)L_70);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_71 = L_66;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_72 = __this->get_address_of_c2_2();
		float L_73 = L_72->get_w_3();
		float L_74 = L_73;
		RuntimeObject * L_75 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_74);
		NullCheck(L_71);
		ArrayElementTypeCheck (L_71, L_75);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_75);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_76 = L_71;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_77 = __this->get_address_of_c3_3();
		float L_78 = L_77->get_w_3();
		float L_79 = L_78;
		RuntimeObject * L_80 = Box(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_il2cpp_TypeInfo_var, &L_79);
		NullCheck(L_76);
		ArrayElementTypeCheck (L_76, L_80);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)L_80);
		String_t* L_81 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral782960A2A1A5CFC6AA819F3CC53C89E5F0641539, L_76, /*hidden argument*/NULL);
		return L_81;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* float4x4_ToString_m713F20FCA71B96C1A0C98544E066CA6552EB6DA6_inline (float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (float4x4_ToString_m713F20FCA71B96C1A0C98544E066CA6552EB6DA6Unity_Mathematics_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4x4({0}f, {1}f, {2}f, {3}f,  {4}f, {5}f, {6}f, {7}f,  {8}f, {9}f, {10}f, {11}f,  {12}f, {13}f, {14}f, {15}f)", c0.x.ToString(format, formatProvider), c1.x.ToString(format, formatProvider), c2.x.ToString(format, formatProvider), c3.x.ToString(format, formatProvider), c0.y.ToString(format, formatProvider), c1.y.ToString(format, formatProvider), c2.y.ToString(format, formatProvider), c3.y.ToString(format, formatProvider), c0.z.ToString(format, formatProvider), c1.z.ToString(format, formatProvider), c2.z.ToString(format, formatProvider), c3.z.ToString(format, formatProvider), c0.w.ToString(format, formatProvider), c1.w.ToString(format, formatProvider), c2.w.ToString(format, formatProvider), c3.w.ToString(format, formatProvider));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_2 = __this->get_address_of_c0_0();
		float* L_3 = L_2->get_address_of_x_0();
		String_t* L_4 = ___format0;
		RuntimeObject* L_5 = ___formatProvider1;
		String_t* L_6 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_6);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_7 = L_1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_8 = __this->get_address_of_c1_1();
		float* L_9 = L_8->get_address_of_x_0();
		String_t* L_10 = ___format0;
		RuntimeObject* L_11 = ___formatProvider1;
		String_t* L_12 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_12);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_12);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_7;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_14 = __this->get_address_of_c2_2();
		float* L_15 = L_14->get_address_of_x_0();
		String_t* L_16 = ___format0;
		RuntimeObject* L_17 = ___formatProvider1;
		String_t* L_18 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_18);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_18);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_19 = L_13;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_20 = __this->get_address_of_c3_3();
		float* L_21 = L_20->get_address_of_x_0();
		String_t* L_22 = ___format0;
		RuntimeObject* L_23 = ___formatProvider1;
		String_t* L_24 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_24);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_25 = L_19;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_26 = __this->get_address_of_c0_0();
		float* L_27 = L_26->get_address_of_y_1();
		String_t* L_28 = ___format0;
		RuntimeObject* L_29 = ___formatProvider1;
		String_t* L_30 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_27, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_30);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_30);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_31 = L_25;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_32 = __this->get_address_of_c1_1();
		float* L_33 = L_32->get_address_of_y_1();
		String_t* L_34 = ___format0;
		RuntimeObject* L_35 = ___formatProvider1;
		String_t* L_36 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_33, L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_36);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_36);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_37 = L_31;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_38 = __this->get_address_of_c2_2();
		float* L_39 = L_38->get_address_of_y_1();
		String_t* L_40 = ___format0;
		RuntimeObject* L_41 = ___formatProvider1;
		String_t* L_42 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_39, L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_42);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_42);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_43 = L_37;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_44 = __this->get_address_of_c3_3();
		float* L_45 = L_44->get_address_of_y_1();
		String_t* L_46 = ___format0;
		RuntimeObject* L_47 = ___formatProvider1;
		String_t* L_48 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_45, L_46, L_47, /*hidden argument*/NULL);
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, L_48);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_48);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_49 = L_43;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_50 = __this->get_address_of_c0_0();
		float* L_51 = L_50->get_address_of_z_2();
		String_t* L_52 = ___format0;
		RuntimeObject* L_53 = ___formatProvider1;
		String_t* L_54 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_51, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, L_54);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_54);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_55 = L_49;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_56 = __this->get_address_of_c1_1();
		float* L_57 = L_56->get_address_of_z_2();
		String_t* L_58 = ___format0;
		RuntimeObject* L_59 = ___formatProvider1;
		String_t* L_60 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_57, L_58, L_59, /*hidden argument*/NULL);
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, L_60);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)L_60);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_61 = L_55;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_62 = __this->get_address_of_c2_2();
		float* L_63 = L_62->get_address_of_z_2();
		String_t* L_64 = ___format0;
		RuntimeObject* L_65 = ___formatProvider1;
		String_t* L_66 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_63, L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_66);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_66);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_67 = L_61;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_68 = __this->get_address_of_c3_3();
		float* L_69 = L_68->get_address_of_z_2();
		String_t* L_70 = ___format0;
		RuntimeObject* L_71 = ___formatProvider1;
		String_t* L_72 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_69, L_70, L_71, /*hidden argument*/NULL);
		NullCheck(L_67);
		ArrayElementTypeCheck (L_67, L_72);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)L_72);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_73 = L_67;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_74 = __this->get_address_of_c0_0();
		float* L_75 = L_74->get_address_of_w_3();
		String_t* L_76 = ___format0;
		RuntimeObject* L_77 = ___formatProvider1;
		String_t* L_78 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_75, L_76, L_77, /*hidden argument*/NULL);
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, L_78);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_78);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_79 = L_73;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_80 = __this->get_address_of_c1_1();
		float* L_81 = L_80->get_address_of_w_3();
		String_t* L_82 = ___format0;
		RuntimeObject* L_83 = ___formatProvider1;
		String_t* L_84 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_81, L_82, L_83, /*hidden argument*/NULL);
		NullCheck(L_79);
		ArrayElementTypeCheck (L_79, L_84);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)L_84);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_85 = L_79;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_86 = __this->get_address_of_c2_2();
		float* L_87 = L_86->get_address_of_w_3();
		String_t* L_88 = ___format0;
		RuntimeObject* L_89 = ___formatProvider1;
		String_t* L_90 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_87, L_88, L_89, /*hidden argument*/NULL);
		NullCheck(L_85);
		ArrayElementTypeCheck (L_85, L_90);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_90);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_91 = L_85;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A * L_92 = __this->get_address_of_c3_3();
		float* L_93 = L_92->get_address_of_w_3();
		String_t* L_94 = ___format0;
		RuntimeObject* L_95 = ___formatProvider1;
		String_t* L_96 = Single_ToString_mCF682C2751EC9B98F1CE5455066B92D7D3356756((float*)L_93, L_94, L_95, /*hidden argument*/NULL);
		NullCheck(L_91);
		ArrayElementTypeCheck (L_91, L_96);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)L_96);
		String_t* L_97 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral782960A2A1A5CFC6AA819F3CC53C89E5F0641539, L_91, /*hidden argument*/NULL);
		return L_97;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  math_asuint_mAF6BF0C9EDA601A8692DB7BD3B443BFFA8A7C234_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint3 asuint(float3 x) { return uint3(asuint(x.x), asuint(x.y), asuint(x.z)); }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		uint32_t L_2 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_1, /*hidden argument*/NULL);
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		uint32_t L_5 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_4, /*hidden argument*/NULL);
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		uint32_t L_8 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_7, /*hidden argument*/NULL);
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_9 = math_uint3_m267A853D616F9ECD652755897C37C40C97EF2606_inline(L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  math_uint3_m267A853D616F9ECD652755897C37C40C97EF2606_inline (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	{
		// public static uint3 uint3(uint x, uint y, uint z) { return new uint3(x, y, z); }
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint32_t L_2 = ___z2;
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_3;
		memset((&L_3), 0, sizeof(L_3));
		uint3__ctor_m83EC50FF1E3D05E4220E4D77C4D2209FE1BDEE37_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  uint3_op_Multiply_mC75EC5D3A910BC87EC728F1D02D9766F6CBB5E44_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___lhs0, uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint3 operator * (uint3 lhs, uint3 rhs) { return new uint3 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z); }
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint3__ctor_m83EC50FF1E3D05E4220E4D77C4D2209FE1BDEE37_inline((&L_12), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_csum_mE0AA784B28235A836A585BE089A0D71C66087888_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint csum(uint3 x) { return x.x + x.y + x.z; }
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_2 = ___x0;
		uint32_t L_3 = L_2.get_y_1();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_4 = ___x0;
		uint32_t L_5 = L_4.get_z_2();
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), (int32_t)L_5));
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint4 asuint(float4 x) { return uint4(asuint(x.x), asuint(x.y), asuint(x.z), asuint(x.w)); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		uint32_t L_2 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_1, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		uint32_t L_5 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_4, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		uint32_t L_8 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_7, /*hidden argument*/NULL);
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_9 = ___x0;
		float L_10 = L_9.get_w_3();
		uint32_t L_11 = math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline(L_10, /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method)
{
	{
		// public static uint4 uint4(uint x, uint y, uint z, uint w) { return new uint4(x, y, z, w); }
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint32_t L_2 = ___z2;
		uint32_t L_3 = ___w3;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4;
		memset((&L_4), 0, sizeof(L_4));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator * (uint4 lhs, uint4 rhs) { return new uint4 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z, lhs.w * rhs.w); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12 = ___lhs0;
		uint32_t L_13 = L_12.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_14 = ___rhs1;
		uint32_t L_15 = L_14.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_16;
		memset((&L_16), 0, sizeof(L_16));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_16), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_csum_m631B7AE1741D825306BA37F108EFB5BC37DBC20F_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint csum(uint4 x) { return x.x + x.y + x.z + x.w; }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = ___x0;
		uint32_t L_3 = L_2.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = ___x0;
		uint32_t L_5 = L_4.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___x0;
		uint32_t L_7 = L_6.get_w_3();
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), (int32_t)L_5)), (int32_t)L_7));
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator * (float4 lhs, float rhs) { return new float4 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_9 = ___lhs0;
		float L_10 = L_9.get_w_3();
		float L_11 = ___rhs1;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), ((float)il2cpp_codegen_multiply((float)L_10, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___lhs0, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator + (float4 lhs, float4 rhs) { return new float4 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_inline((&L_16), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), ((float)il2cpp_codegen_add((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator + (uint4 lhs, uint4 rhs) { return new uint4 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12 = ___lhs0;
		uint32_t L_13 = L_12.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_14 = ___rhs1;
		uint32_t L_15 = L_14.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_16;
		memset((&L_16), 0, sizeof(L_16));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_16), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t math_asint_mE77DB058803DB637FC10ADA11C8F8B92C53364B6_inline (float ___x0, const RuntimeMethod* method)
{
	IntFloatUnion_t7ECB2A03CE51B658B77F53B39547C3BDB8164CD7  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// u.intValue = 0;
		(&V_0)->set_intValue_0(0);
		// u.floatValue = x;
		float L_0 = ___x0;
		(&V_0)->set_floatValue_1(L_0);
		// return u.intValue;
		IntFloatUnion_t7ECB2A03CE51B658B77F53B39547C3BDB8164CD7  L_1 = V_0;
		int32_t L_2 = L_1.get_intValue_0();
		return L_2;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707_inline (float ___x0, const RuntimeMethod* method)
{
	{
		// public static uint asuint(float x) { return (uint)asint(x); }
		float L_0 = ___x0;
		int32_t L_1 = math_asint_mE77DB058803DB637FC10ADA11C8F8B92C53364B6_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float math_min_mAFED8DFF69B82D3639513AD5275FBBBEE0034403_inline (float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// public static float min(float x, float y) { return float.IsNaN(y) || x < y ? x : y; }
		float L_0 = ___y1;
		bool L_1 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___x0;
		float L_3 = ___y1;
		if ((((float)L_2) < ((float)L_3)))
		{
			goto IL_000e;
		}
	}
	{
		float L_4 = ___y1;
		return L_4;
	}

IL_000e:
	{
		float L_5 = ___x0;
		return L_5;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float math_max_mDA87691EF0947E7D4DE6ADC8AE222FD3F2D15B37_inline (float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// public static float max(float x, float y) { return float.IsNaN(y) || x > y ? x : y; }
		float L_0 = ___y1;
		bool L_1 = Single_IsNaN_m1ACB82FA5DC805F0F5015A1DA6B3DC447C963AFB(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___x0;
		float L_3 = ___y1;
		if ((((float)L_2) > ((float)L_3)))
		{
			goto IL_000e;
		}
	}
	{
		float L_4 = ___y1;
		return L_4;
	}

IL_000e:
	{
		float L_5 = ___x0;
		return L_5;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float math_dot_mD612AB280CD7FF05D3F393CDB7011CD8FBD2B0FA_inline (float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___x0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___y1, const RuntimeMethod* method)
{
	{
		// public static float dot(float3 x, float3 y) { return x.x * y.x + x.y * y.y + x.z * y.z; }
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___x0;
		float L_5 = L_4.get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_6 = ___y1;
		float L_7 = L_6.get_y_1();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_8 = ___x0;
		float L_9 = L_8.get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_10 = ___y1;
		float L_11 = L_10.get_z_2();
		return ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11))));
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float math_rsqrt_m9E4131955C2195AE4F694A4AE87815428FAB59F3_inline (float ___x0, const RuntimeMethod* method)
{
	{
		// public static float rsqrt(float x) { return 1.0f / sqrt(x); }
		float L_0 = ___x0;
		float L_1 = math_sqrt_mFFB10D250782D3B931727A664F421D1061A8F76B(L_0, /*hidden argument*/NULL);
		return ((float)((float)(1.0f)/(float)L_1));
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  float3_op_Multiply_m719CA87B984E18E247E64BE24D994AA45D97752A_inline (float ___lhs0, float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator * (float lhs, float3 rhs) { return new float3 (lhs * rhs.x, lhs * rhs.y, lhs * rhs.z); }
		float L_0 = ___lhs0;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_1 = ___rhs1;
		float L_2 = L_1.get_x_0();
		float L_3 = ___lhs0;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_4 = ___rhs1;
		float L_5 = L_4.get_y_1();
		float L_6 = ___lhs0;
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_7 = ___rhs1;
		float L_8 = L_7.get_z_2();
		float3_tB3DB6E304B40D8C4DA63622603E1671D83A2FDF7  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_0, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_3, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_6, (float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		uint32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		uint32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		uint32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		uint32_t L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Addition_m58A8D6BFA5DDA9143D132B57084CB4B271EF1D35_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint32_t ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator + (uint4 lhs, uint rhs) { return new uint4 (lhs.x + rhs, lhs.y + rhs, lhs.z + rhs, lhs.w + rhs); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint32_t L_2 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = ___lhs0;
		uint32_t L_4 = L_3.get_y_1();
		uint32_t L_5 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___lhs0;
		uint32_t L_7 = L_6.get_z_2();
		uint32_t L_8 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_9 = ___lhs0;
		uint32_t L_10 = L_9.get_w_3();
		uint32_t L_11 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_12), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_2)), ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5)), ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8)), ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_Multiply_m6302785769695A846A3A586E86A3455561C1582B_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint32_t ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator * (uint4 lhs, uint rhs) { return new uint4 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint32_t L_2 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = ___lhs0;
		uint32_t L_4 = L_3.get_y_1();
		uint32_t L_5 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___lhs0;
		uint32_t L_7 = L_6.get_z_2();
		uint32_t L_8 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_9 = ___lhs0;
		uint32_t L_10 = L_9.get_w_3();
		uint32_t L_11 = ___rhs1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_12), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_2)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_4, (int32_t)L_5)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_7, (int32_t)L_8)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_10, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_LeftShift_m031F4835321567AB31E062A9BBD456FEBBF50405_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___x0, int32_t ___n1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator << (uint4 x, int n) { return new uint4 (x.x << n, x.y << n, x.z << n, x.w << n); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		int32_t L_2 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = ___x0;
		uint32_t L_4 = L_3.get_y_1();
		int32_t L_5 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___x0;
		uint32_t L_7 = L_6.get_z_2();
		int32_t L_8 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_9 = ___x0;
		uint32_t L_10 = L_9.get_w_3();
		int32_t L_11 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_12), ((int32_t)((int32_t)L_1<<(int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)31))))), ((int32_t)((int32_t)L_4<<(int32_t)((int32_t)((int32_t)L_5&(int32_t)((int32_t)31))))), ((int32_t)((int32_t)L_7<<(int32_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)31))))), ((int32_t)((int32_t)L_10<<(int32_t)((int32_t)((int32_t)L_11&(int32_t)((int32_t)31))))), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_RightShift_m8714E5CBC739B35AB61645F0E5B2569C2D929DD3_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___x0, int32_t ___n1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator >> (uint4 x, int n) { return new uint4 (x.x >> n, x.y >> n, x.z >> n, x.w >> n); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		int32_t L_2 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_3 = ___x0;
		uint32_t L_4 = L_3.get_y_1();
		int32_t L_5 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___x0;
		uint32_t L_7 = L_6.get_z_2();
		int32_t L_8 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_9 = ___x0;
		uint32_t L_10 = L_9.get_w_3();
		int32_t L_11 = ___n1;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_12), ((int32_t)((uint32_t)L_1>>((int32_t)((int32_t)L_2&(int32_t)((int32_t)31))))), ((int32_t)((uint32_t)L_4>>((int32_t)((int32_t)L_5&(int32_t)((int32_t)31))))), ((int32_t)((uint32_t)L_7>>((int32_t)((int32_t)L_8&(int32_t)((int32_t)31))))), ((int32_t)((uint32_t)L_10>>((int32_t)((int32_t)L_11&(int32_t)((int32_t)31))))), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  uint4_op_BitwiseOr_mA59ED29C9B5B61C2FAE0846C61049A7D69F1F0C0_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___lhs0, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator | (uint4 lhs, uint4 rhs) { return new uint4 (lhs.x | rhs.x, lhs.y | rhs.y, lhs.z | rhs.z, lhs.w | rhs.w); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_12 = ___lhs0;
		uint32_t L_13 = L_12.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_14 = ___rhs1;
		uint32_t L_15 = L_14.get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_16;
		memset((&L_16), 0, sizeof(L_16));
		uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_inline((&L_16), ((int32_t)((int32_t)L_1|(int32_t)L_3)), ((int32_t)((int32_t)L_5|(int32_t)L_7)), ((int32_t)((int32_t)L_9|(int32_t)L_11)), ((int32_t)((int32_t)L_13|(int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_rol_mE88B527F8CC27B5191D4820D20E356489857BD86_inline (uint32_t ___x0, int32_t ___n1, const RuntimeMethod* method)
{
	{
		// public static uint rol(uint x, int n) { return (x << n) | (x >> (32 - n)); }
		uint32_t L_0 = ___x0;
		int32_t L_1 = ___n1;
		uint32_t L_2 = ___x0;
		int32_t L_3 = ___n1;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0<<(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))))|(int32_t)((int32_t)((uint32_t)L_2>>((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)32), (int32_t)L_3))&(int32_t)((int32_t)31)))))));
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  math_float4x4_mDBFE1BFDCF3B3290DEA76F08CBFEF2537A5C60E1_inline (float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c00, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c11, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c22, float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  ___c33, const RuntimeMethod* method)
{
	{
		// public static float4x4 float4x4(float4 c0, float4 c1, float4 c2, float4 c3) { return new float4x4(c0, c1, c2, c3); }
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_0 = ___c00;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_1 = ___c11;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_2 = ___c22;
		float4_t735C40BB7541FD42D3FABC1B87918B2F237A720A  L_3 = ___c33;
		float4x4_t3C77F64161FEB04AE043BE11ED0214EE94DB5BAC  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4x4__ctor_m9A2D24D07FF8DCB7495AC3FD76364FD3FB81D807_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void uint3__ctor_m83EC50FF1E3D05E4220E4D77C4D2209FE1BDEE37_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		uint32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		uint32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		uint32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool uint3_Equals_mF1A43BF5E958201193205C727DE277A13E8A7877_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(uint3 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z; }
		uint32_t L_0 = __this->get_x_0();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_1 = ___rhs0;
		uint32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_002b;
		}
	}
	{
		uint32_t L_3 = __this->get_y_1();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_4 = ___rhs0;
		uint32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		uint32_t L_6 = __this->get_z_2();
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_7 = ___rhs0;
		uint32_t L_8 = L_7.get_z_2();
		return (bool)((((int32_t)L_6) == ((int32_t)L_8))? 1 : 0);
	}

IL_002b:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_hash_mCDE6963B79DB564C76CD37630D658920BFA586B9_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(v * uint3(0xCD266C89u, 0xF1852A33u, 0x77E35E77u)) + 0x863E3729u;
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_0 = ___v0;
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_1 = math_uint3_m267A853D616F9ECD652755897C37C40C97EF2606_inline(((int32_t)-853119863), ((int32_t)-242931149), ((int32_t)2011389559), /*hidden argument*/NULL);
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_2 = uint3_op_Multiply_mC75EC5D3A910BC87EC728F1D02D9766F6CBB5E44_inline(L_0, L_1, /*hidden argument*/NULL);
		uint32_t L_3 = math_csum_mE0AA784B28235A836A585BE089A0D71C66087888_inline(L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)-2042742999)));
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t uint3_GetHashCode_mB20D3856AF84E5AA5A7222DE08C7F21CA127039D_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F  L_0 = (*(uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F *)__this);
		uint32_t L_1 = math_hash_mCDE6963B79DB564C76CD37630D658920BFA586B9_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* uint3_ToString_m02FAB1C4D7D9A4831F7D0D78AF975B8C4B7EF4FD_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uint3_ToString_m02FAB1C4D7D9A4831F7D0D78AF975B8C4B7EF4FDUnity_Mathematics_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint3({0}, {1}, {2})", x, y, z);
		uint32_t L_0 = __this->get_x_0();
		uint32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_1);
		uint32_t L_3 = __this->get_y_1();
		uint32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_4);
		uint32_t L_6 = __this->get_z_2();
		uint32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9 = String_Format_m26BBF75F9609FAD0B39C2242FEBAAD7D68F14D99(_stringLiteral5DBD030B94725EF92B6F4BDDEB5A06DB312AF764, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* uint3_ToString_m7705E589BDC0FCC9D87428DEFD95A66EF4A7A6AF_inline (uint3_t54D5C516F7B31D89CDAE4A34F697F2F97B6E162F * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uint3_ToString_m7705E589BDC0FCC9D87428DEFD95A66EF4A7A6AFUnity_Mathematics_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint3({0}, {1}, {2})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider));
		uint32_t* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_0, L_1, L_2, /*hidden argument*/NULL);
		uint32_t* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_4, L_5, L_6, /*hidden argument*/NULL);
		uint32_t* L_8 = __this->get_address_of_z_2();
		String_t* L_9 = ___format0;
		RuntimeObject* L_10 = ___formatProvider1;
		String_t* L_11 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_8, L_9, L_10, /*hidden argument*/NULL);
		String_t* L_12 = String_Format_m26BBF75F9609FAD0B39C2242FEBAAD7D68F14D99(_stringLiteral5DBD030B94725EF92B6F4BDDEB5A06DB312AF764, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool uint4_Equals_m302069717D95B95C972C7C1B7D29D9E0DBBD6542_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(uint4 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w; }
		uint32_t L_0 = __this->get_x_0();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_1 = ___rhs0;
		uint32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_3 = __this->get_y_1();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_4 = ___rhs0;
		uint32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_6 = __this->get_z_2();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_7 = ___rhs0;
		uint32_t L_8 = L_7.get_z_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_9 = __this->get_w_3();
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_10 = ___rhs0;
		uint32_t L_11 = L_10.get_w_3();
		return (bool)((((int32_t)L_9) == ((int32_t)L_11))? 1 : 0);
	}

IL_0039:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR uint32_t math_hash_m1DF44299A6DEA06F77DA49DFA00039E341DB3F33_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(v * uint4(0xB492BF15u, 0xD37220E3u, 0x7AA2C2BDu, 0xE16BC89Du)) + 0x7AA07CD3u;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = ___v0;
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_1 = math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD_inline(((int32_t)-1265451243), ((int32_t)-747495197), ((int32_t)2057487037), ((int32_t)-513029987), /*hidden argument*/NULL);
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_2 = uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86_inline(L_0, L_1, /*hidden argument*/NULL);
		uint32_t L_3 = math_csum_m631B7AE1741D825306BA37F108EFB5BC37DBC20F_inline(L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)2057338067)));
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t uint4_GetHashCode_m187968010316E4D76A2FD32E8CD068A299007AA8_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC  L_0 = (*(uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC *)__this);
		uint32_t L_1 = math_hash_m1DF44299A6DEA06F77DA49DFA00039E341DB3F33_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* uint4_ToString_m1ADFBF43F0EC44A59D7F9908C9891DC3C31690BC_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uint4_ToString_m1ADFBF43F0EC44A59D7F9908C9891DC3C31690BCUnity_Mathematics_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint4({0}, {1}, {2}, {3})", x, y, z, w);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		uint32_t L_2 = __this->get_x_0();
		uint32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_5 = L_1;
		uint32_t L_6 = __this->get_y_1();
		uint32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_9 = L_5;
		uint32_t L_10 = __this->get_z_2();
		uint32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = L_9;
		uint32_t L_14 = __this->get_w_3();
		uint32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral901D0D2D30D5C7F19A6CF4A9C689EBAFE3B3FB54, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR String_t* uint4_ToString_mED39980F32C81D913660EC644111F2070E7870A2_inline (uint4_t630BFFF601EDEFA92809E170C8A91B3A2B7B57AC * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (uint4_ToString_mED39980F32C81D913660EC644111F2070E7870A2Unity_Mathematics_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint4({0}, {1}, {2}, {3})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider), w.ToString(format, formatProvider));
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		uint32_t* L_2 = __this->get_address_of_x_0();
		String_t* L_3 = ___format0;
		RuntimeObject* L_4 = ___formatProvider1;
		String_t* L_5 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_6 = L_1;
		uint32_t* L_7 = __this->get_address_of_y_1();
		String_t* L_8 = ___format0;
		RuntimeObject* L_9 = ___formatProvider1;
		String_t* L_10 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_11 = L_6;
		uint32_t* L_12 = __this->get_address_of_z_2();
		String_t* L_13 = ___format0;
		RuntimeObject* L_14 = ___formatProvider1;
		String_t* L_15 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = L_11;
		uint32_t* L_17 = __this->get_address_of_w_3();
		String_t* L_18 = ___format0;
		RuntimeObject* L_19 = ___formatProvider1;
		String_t* L_20 = UInt32_ToString_m57BE7A0F4A653986FEAC4794CD13B04CE012F4EE((uint32_t*)L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21 = String_Format_mA3AC3FE7B23D97F3A5BAA082D25B0E01B341A865(_stringLiteral901D0D2D30D5C7F19A6CF4A9C689EBAFE3B3FB54, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
