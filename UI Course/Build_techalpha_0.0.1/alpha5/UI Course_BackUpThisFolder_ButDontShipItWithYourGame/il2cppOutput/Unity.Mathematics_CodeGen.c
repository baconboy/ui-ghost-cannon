﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float3)
extern void math_hash_m2502AF91810C2607571B2000ECA8A7370055FEF5 ();
// 0x00000002 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4)
extern void math_hash_m922609CC35709A98A21766DAB7DD2B0978C5D104 ();
// 0x00000003 Unity.Mathematics.float4x4 Unity.Mathematics.math::float4x4(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void math_float4x4_mDBFE1BFDCF3B3290DEA76F08CBFEF2537A5C60E1 ();
// 0x00000004 Unity.Mathematics.float3 Unity.Mathematics.math::transform(Unity.Mathematics.float4x4,Unity.Mathematics.float3)
extern void math_transform_m67EB6EEB72DC249326CB6D222CB9DD761FE9D551 ();
// 0x00000005 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4x4)
extern void math_hash_m8E61D111AA597992FA0D8C6D5FBE3FBC7B417B25 ();
// 0x00000006 System.Int32 Unity.Mathematics.math::asint(System.Single)
extern void math_asint_mE77DB058803DB637FC10ADA11C8F8B92C53364B6 ();
// 0x00000007 System.UInt32 Unity.Mathematics.math::asuint(System.Single)
extern void math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707 ();
// 0x00000008 Unity.Mathematics.uint3 Unity.Mathematics.math::asuint(Unity.Mathematics.float3)
extern void math_asuint_mAF6BF0C9EDA601A8692DB7BD3B443BFFA8A7C234 ();
// 0x00000009 Unity.Mathematics.uint4 Unity.Mathematics.math::asuint(Unity.Mathematics.float4)
extern void math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F ();
// 0x0000000A System.Single Unity.Mathematics.math::min(System.Single,System.Single)
extern void math_min_mAFED8DFF69B82D3639513AD5275FBBBEE0034403 ();
// 0x0000000B Unity.Mathematics.float3 Unity.Mathematics.math::min(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void math_min_m02CCAAC6A1744B2ECC218EA4A627D565D6B36FC7 ();
// 0x0000000C System.Single Unity.Mathematics.math::max(System.Single,System.Single)
extern void math_max_mDA87691EF0947E7D4DE6ADC8AE222FD3F2D15B37 ();
// 0x0000000D Unity.Mathematics.float3 Unity.Mathematics.math::max(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void math_max_mBE2D84B4A9FC8CFD5473473FB0E2D6D7A16B2010 ();
// 0x0000000E System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void math_dot_mD612AB280CD7FF05D3F393CDB7011CD8FBD2B0FA ();
// 0x0000000F System.Single Unity.Mathematics.math::sqrt(System.Single)
extern void math_sqrt_mFFB10D250782D3B931727A664F421D1061A8F76B ();
// 0x00000010 System.Single Unity.Mathematics.math::rsqrt(System.Single)
extern void math_rsqrt_m9E4131955C2195AE4F694A4AE87815428FAB59F3 ();
// 0x00000011 Unity.Mathematics.float3 Unity.Mathematics.math::normalize(Unity.Mathematics.float3)
extern void math_normalize_mC205C6CF9B74165AA38214CE6692950982E7CDD4 ();
// 0x00000012 System.UInt32 Unity.Mathematics.math::rol(System.UInt32,System.Int32)
extern void math_rol_mE88B527F8CC27B5191D4820D20E356489857BD86 ();
// 0x00000013 System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint3)
extern void math_csum_mE0AA784B28235A836A585BE089A0D71C66087888 ();
// 0x00000014 System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint4)
extern void math_csum_m631B7AE1741D825306BA37F108EFB5BC37DBC20F ();
// 0x00000015 System.UInt32 Unity.Mathematics.math::hash(System.Void*,System.Int32,System.UInt32)
extern void math_hash_m4D2F3D8727152898513C7FF229FE9C8B4A48260F ();
// 0x00000016 Unity.Mathematics.float4 Unity.Mathematics.math::mul(Unity.Mathematics.float4x4,Unity.Mathematics.float4)
extern void math_mul_m37BCAA6C9FA01CCC5E8776F95EB14FAE8EDFAF86 ();
// 0x00000017 Unity.Mathematics.float4x4 Unity.Mathematics.math::mul(Unity.Mathematics.float4x4,Unity.Mathematics.float4x4)
extern void math_mul_m3B547595438404CE64BDF1410080BCC73017F892 ();
// 0x00000018 Unity.Mathematics.uint3 Unity.Mathematics.math::uint3(System.UInt32,System.UInt32,System.UInt32)
extern void math_uint3_m267A853D616F9ECD652755897C37C40C97EF2606 ();
// 0x00000019 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint3)
extern void math_hash_mCDE6963B79DB564C76CD37630D658920BFA586B9 ();
// 0x0000001A Unity.Mathematics.uint4 Unity.Mathematics.math::uint4(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
extern void math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD ();
// 0x0000001B System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint4)
extern void math_hash_m1DF44299A6DEA06F77DA49DFA00039E341DB3F33 ();
// 0x0000001C System.Void Unity.Mathematics.float3::.ctor(System.Single,System.Single,System.Single)
extern void float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_AdjustorThunk ();
// 0x0000001D Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(Unity.Mathematics.float3,System.Single)
extern void float3_op_Multiply_mB63D81E6A68BA3002AB0A2A1BBB2401C7CF7CC47 ();
// 0x0000001E Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(System.Single,Unity.Mathematics.float3)
extern void float3_op_Multiply_m719CA87B984E18E247E64BE24D994AA45D97752A ();
// 0x0000001F Unity.Mathematics.float3 Unity.Mathematics.float3::op_Addition(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void float3_op_Addition_mAC094AA119118FFD28ABADEA18C46849CF2D4609 ();
// 0x00000020 Unity.Mathematics.float3 Unity.Mathematics.float3::op_Subtraction(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void float3_op_Subtraction_mB767105FEAF2403E8597BF6867D214BEE08C3751 ();
// 0x00000021 System.Boolean Unity.Mathematics.float3::Equals(Unity.Mathematics.float3)
extern void float3_Equals_mD9E67316EB63B276A4E7D47E0DA25377FC21C129_AdjustorThunk ();
// 0x00000022 System.Boolean Unity.Mathematics.float3::Equals(System.Object)
extern void float3_Equals_m2B1D30B076B6597B9F40527025211F33E8EC9145_AdjustorThunk ();
// 0x00000023 System.Int32 Unity.Mathematics.float3::GetHashCode()
extern void float3_GetHashCode_mA8A92E15BE1FE55D94575007DAF37C6AB9379C24_AdjustorThunk ();
// 0x00000024 System.String Unity.Mathematics.float3::ToString()
extern void float3_ToString_mB4863B6E2512E53544B02772705EBF221D744416_AdjustorThunk ();
// 0x00000025 System.String Unity.Mathematics.float3::ToString(System.String,System.IFormatProvider)
extern void float3_ToString_m513168CE73E3A1943FBF999282E27D81DB726E1C_AdjustorThunk ();
// 0x00000026 UnityEngine.Vector3 Unity.Mathematics.float3::op_Implicit(Unity.Mathematics.float3)
extern void float3_op_Implicit_m36AA7A36BB934C8EF6E5D473B001631A692572BA ();
// 0x00000027 System.Void Unity.Mathematics.float4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_AdjustorThunk ();
// 0x00000028 System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float3,System.Single)
extern void float4__ctor_mC873E36671C90EFA5B0F4513EEA6D79B0E4D4333_AdjustorThunk ();
// 0x00000029 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(Unity.Mathematics.float4,System.Single)
extern void float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93 ();
// 0x0000002A Unity.Mathematics.float4 Unity.Mathematics.float4::op_Addition(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493 ();
// 0x0000002B Unity.Mathematics.float3 Unity.Mathematics.float4::get_xyz()
extern void float4_get_xyz_mC5FD59B33F4C3F7DA22E8C0F0B1FFC2C38A1F8DB_AdjustorThunk ();
// 0x0000002C System.Boolean Unity.Mathematics.float4::Equals(Unity.Mathematics.float4)
extern void float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_AdjustorThunk ();
// 0x0000002D System.Boolean Unity.Mathematics.float4::Equals(System.Object)
extern void float4_Equals_m6E45C7C37F14AD55CF58E8C81A570A7035C33987_AdjustorThunk ();
// 0x0000002E System.Int32 Unity.Mathematics.float4::GetHashCode()
extern void float4_GetHashCode_m9510EAC95094BD49DF7CF63937B46669614AB277_AdjustorThunk ();
// 0x0000002F System.String Unity.Mathematics.float4::ToString()
extern void float4_ToString_m8D534BBA43D7804CD31E26FE4C1972284DE35FBC_AdjustorThunk ();
// 0x00000030 System.String Unity.Mathematics.float4::ToString(System.String,System.IFormatProvider)
extern void float4_ToString_m92F4A00D57F53DC94183F09F72ED6CAC1F390650_AdjustorThunk ();
// 0x00000031 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Implicit(UnityEngine.Vector4)
extern void float4_op_Implicit_mD5B7AB76E24FAE3CB1B9F8257873D3294182C36E ();
// 0x00000032 System.Void Unity.Mathematics.float4x4::.ctor(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void float4x4__ctor_m9A2D24D07FF8DCB7495AC3FD76364FD3FB81D807_AdjustorThunk ();
// 0x00000033 System.Void Unity.Mathematics.float4x4::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void float4x4__ctor_m9C451E986E9271E34E011C51F35160C8E9E6CF98_AdjustorThunk ();
// 0x00000034 System.Boolean Unity.Mathematics.float4x4::Equals(Unity.Mathematics.float4x4)
extern void float4x4_Equals_m78EF9656D285326360D7957143A42017BE85AD96_AdjustorThunk ();
// 0x00000035 System.Boolean Unity.Mathematics.float4x4::Equals(System.Object)
extern void float4x4_Equals_m2E4E7CA61E037ECF3FCFF3CDF4A454F2DA1DD202_AdjustorThunk ();
// 0x00000036 System.Int32 Unity.Mathematics.float4x4::GetHashCode()
extern void float4x4_GetHashCode_m35D91773F446B0D620E5B5E04BFF72BE8BDDD0F1_AdjustorThunk ();
// 0x00000037 System.String Unity.Mathematics.float4x4::ToString()
extern void float4x4_ToString_m782366095AD7C5C4144B9A7EE0113328AAEC908B_AdjustorThunk ();
// 0x00000038 System.String Unity.Mathematics.float4x4::ToString(System.String,System.IFormatProvider)
extern void float4x4_ToString_m713F20FCA71B96C1A0C98544E066CA6552EB6DA6_AdjustorThunk ();
// 0x00000039 Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Implicit(UnityEngine.Matrix4x4)
extern void float4x4_op_Implicit_m6AE393AD5B13121437B0FACB0DFCA8098C8C8F9A ();
// 0x0000003A System.Void Unity.Mathematics.float4x4::.cctor()
extern void float4x4__cctor_mFA9894818878962653AD1F8C490BF8B0D1E5F7EF ();
// 0x0000003B System.Void Unity.Mathematics.uint3::.ctor(System.UInt32,System.UInt32,System.UInt32)
extern void uint3__ctor_m83EC50FF1E3D05E4220E4D77C4D2209FE1BDEE37_AdjustorThunk ();
// 0x0000003C Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_Multiply(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
extern void uint3_op_Multiply_mC75EC5D3A910BC87EC728F1D02D9766F6CBB5E44 ();
// 0x0000003D System.Boolean Unity.Mathematics.uint3::Equals(Unity.Mathematics.uint3)
extern void uint3_Equals_mF1A43BF5E958201193205C727DE277A13E8A7877_AdjustorThunk ();
// 0x0000003E System.Boolean Unity.Mathematics.uint3::Equals(System.Object)
extern void uint3_Equals_mD0F46E10E9BBF31279855DAFE2325C6038C0E6D3_AdjustorThunk ();
// 0x0000003F System.Int32 Unity.Mathematics.uint3::GetHashCode()
extern void uint3_GetHashCode_mB20D3856AF84E5AA5A7222DE08C7F21CA127039D_AdjustorThunk ();
// 0x00000040 System.String Unity.Mathematics.uint3::ToString()
extern void uint3_ToString_m02FAB1C4D7D9A4831F7D0D78AF975B8C4B7EF4FD_AdjustorThunk ();
// 0x00000041 System.String Unity.Mathematics.uint3::ToString(System.String,System.IFormatProvider)
extern void uint3_ToString_m7705E589BDC0FCC9D87428DEFD95A66EF4A7A6AF_AdjustorThunk ();
// 0x00000042 System.Void Unity.Mathematics.uint4::.ctor(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
extern void uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_AdjustorThunk ();
// 0x00000043 Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Multiply(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
extern void uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86 ();
// 0x00000044 Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Multiply(Unity.Mathematics.uint4,System.UInt32)
extern void uint4_op_Multiply_m6302785769695A846A3A586E86A3455561C1582B ();
// 0x00000045 Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Addition(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
extern void uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE ();
// 0x00000046 Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Addition(Unity.Mathematics.uint4,System.UInt32)
extern void uint4_op_Addition_m58A8D6BFA5DDA9143D132B57084CB4B271EF1D35 ();
// 0x00000047 Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_LeftShift(Unity.Mathematics.uint4,System.Int32)
extern void uint4_op_LeftShift_m031F4835321567AB31E062A9BBD456FEBBF50405 ();
// 0x00000048 Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_RightShift(Unity.Mathematics.uint4,System.Int32)
extern void uint4_op_RightShift_m8714E5CBC739B35AB61645F0E5B2569C2D929DD3 ();
// 0x00000049 Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_BitwiseOr(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
extern void uint4_op_BitwiseOr_mA59ED29C9B5B61C2FAE0846C61049A7D69F1F0C0 ();
// 0x0000004A System.Boolean Unity.Mathematics.uint4::Equals(Unity.Mathematics.uint4)
extern void uint4_Equals_m302069717D95B95C972C7C1B7D29D9E0DBBD6542_AdjustorThunk ();
// 0x0000004B System.Boolean Unity.Mathematics.uint4::Equals(System.Object)
extern void uint4_Equals_mA3312E8FC1A4734E2402424ADAC7BCE386918488_AdjustorThunk ();
// 0x0000004C System.Int32 Unity.Mathematics.uint4::GetHashCode()
extern void uint4_GetHashCode_m187968010316E4D76A2FD32E8CD068A299007AA8_AdjustorThunk ();
// 0x0000004D System.String Unity.Mathematics.uint4::ToString()
extern void uint4_ToString_m1ADFBF43F0EC44A59D7F9908C9891DC3C31690BC_AdjustorThunk ();
// 0x0000004E System.String Unity.Mathematics.uint4::ToString(System.String,System.IFormatProvider)
extern void uint4_ToString_mED39980F32C81D913660EC644111F2070E7870A2_AdjustorThunk ();
static Il2CppMethodPointer s_methodPointers[78] = 
{
	math_hash_m2502AF91810C2607571B2000ECA8A7370055FEF5,
	math_hash_m922609CC35709A98A21766DAB7DD2B0978C5D104,
	math_float4x4_mDBFE1BFDCF3B3290DEA76F08CBFEF2537A5C60E1,
	math_transform_m67EB6EEB72DC249326CB6D222CB9DD761FE9D551,
	math_hash_m8E61D111AA597992FA0D8C6D5FBE3FBC7B417B25,
	math_asint_mE77DB058803DB637FC10ADA11C8F8B92C53364B6,
	math_asuint_m8D241E84216CB1F74E4963F16C8223E9F705A707,
	math_asuint_mAF6BF0C9EDA601A8692DB7BD3B443BFFA8A7C234,
	math_asuint_mD84DB0C5EB138860FA8A76E9C9F496010F77849F,
	math_min_mAFED8DFF69B82D3639513AD5275FBBBEE0034403,
	math_min_m02CCAAC6A1744B2ECC218EA4A627D565D6B36FC7,
	math_max_mDA87691EF0947E7D4DE6ADC8AE222FD3F2D15B37,
	math_max_mBE2D84B4A9FC8CFD5473473FB0E2D6D7A16B2010,
	math_dot_mD612AB280CD7FF05D3F393CDB7011CD8FBD2B0FA,
	math_sqrt_mFFB10D250782D3B931727A664F421D1061A8F76B,
	math_rsqrt_m9E4131955C2195AE4F694A4AE87815428FAB59F3,
	math_normalize_mC205C6CF9B74165AA38214CE6692950982E7CDD4,
	math_rol_mE88B527F8CC27B5191D4820D20E356489857BD86,
	math_csum_mE0AA784B28235A836A585BE089A0D71C66087888,
	math_csum_m631B7AE1741D825306BA37F108EFB5BC37DBC20F,
	math_hash_m4D2F3D8727152898513C7FF229FE9C8B4A48260F,
	math_mul_m37BCAA6C9FA01CCC5E8776F95EB14FAE8EDFAF86,
	math_mul_m3B547595438404CE64BDF1410080BCC73017F892,
	math_uint3_m267A853D616F9ECD652755897C37C40C97EF2606,
	math_hash_mCDE6963B79DB564C76CD37630D658920BFA586B9,
	math_uint4_m0398DAB18DDD16BEEC598D2D4C24C2BF1DDC9AAD,
	math_hash_m1DF44299A6DEA06F77DA49DFA00039E341DB3F33,
	float3__ctor_m1AFE54C813693DB16235AD5A2A67C0019681A3DD_AdjustorThunk,
	float3_op_Multiply_mB63D81E6A68BA3002AB0A2A1BBB2401C7CF7CC47,
	float3_op_Multiply_m719CA87B984E18E247E64BE24D994AA45D97752A,
	float3_op_Addition_mAC094AA119118FFD28ABADEA18C46849CF2D4609,
	float3_op_Subtraction_mB767105FEAF2403E8597BF6867D214BEE08C3751,
	float3_Equals_mD9E67316EB63B276A4E7D47E0DA25377FC21C129_AdjustorThunk,
	float3_Equals_m2B1D30B076B6597B9F40527025211F33E8EC9145_AdjustorThunk,
	float3_GetHashCode_mA8A92E15BE1FE55D94575007DAF37C6AB9379C24_AdjustorThunk,
	float3_ToString_mB4863B6E2512E53544B02772705EBF221D744416_AdjustorThunk,
	float3_ToString_m513168CE73E3A1943FBF999282E27D81DB726E1C_AdjustorThunk,
	float3_op_Implicit_m36AA7A36BB934C8EF6E5D473B001631A692572BA,
	float4__ctor_m114ACE27918FBE1894FFD76938F404AC6704B5BB_AdjustorThunk,
	float4__ctor_mC873E36671C90EFA5B0F4513EEA6D79B0E4D4333_AdjustorThunk,
	float4_op_Multiply_mBC41BCDCE0BD3D3F9DC20CB885B7F62D9CA44C93,
	float4_op_Addition_mA393550B92FA102FB7640ECFD1EFE1769C2E3493,
	float4_get_xyz_mC5FD59B33F4C3F7DA22E8C0F0B1FFC2C38A1F8DB_AdjustorThunk,
	float4_Equals_m84F2CBF706F7D2EA6903F5B0C8E1C4F65BCB0D65_AdjustorThunk,
	float4_Equals_m6E45C7C37F14AD55CF58E8C81A570A7035C33987_AdjustorThunk,
	float4_GetHashCode_m9510EAC95094BD49DF7CF63937B46669614AB277_AdjustorThunk,
	float4_ToString_m8D534BBA43D7804CD31E26FE4C1972284DE35FBC_AdjustorThunk,
	float4_ToString_m92F4A00D57F53DC94183F09F72ED6CAC1F390650_AdjustorThunk,
	float4_op_Implicit_mD5B7AB76E24FAE3CB1B9F8257873D3294182C36E,
	float4x4__ctor_m9A2D24D07FF8DCB7495AC3FD76364FD3FB81D807_AdjustorThunk,
	float4x4__ctor_m9C451E986E9271E34E011C51F35160C8E9E6CF98_AdjustorThunk,
	float4x4_Equals_m78EF9656D285326360D7957143A42017BE85AD96_AdjustorThunk,
	float4x4_Equals_m2E4E7CA61E037ECF3FCFF3CDF4A454F2DA1DD202_AdjustorThunk,
	float4x4_GetHashCode_m35D91773F446B0D620E5B5E04BFF72BE8BDDD0F1_AdjustorThunk,
	float4x4_ToString_m782366095AD7C5C4144B9A7EE0113328AAEC908B_AdjustorThunk,
	float4x4_ToString_m713F20FCA71B96C1A0C98544E066CA6552EB6DA6_AdjustorThunk,
	float4x4_op_Implicit_m6AE393AD5B13121437B0FACB0DFCA8098C8C8F9A,
	float4x4__cctor_mFA9894818878962653AD1F8C490BF8B0D1E5F7EF,
	uint3__ctor_m83EC50FF1E3D05E4220E4D77C4D2209FE1BDEE37_AdjustorThunk,
	uint3_op_Multiply_mC75EC5D3A910BC87EC728F1D02D9766F6CBB5E44,
	uint3_Equals_mF1A43BF5E958201193205C727DE277A13E8A7877_AdjustorThunk,
	uint3_Equals_mD0F46E10E9BBF31279855DAFE2325C6038C0E6D3_AdjustorThunk,
	uint3_GetHashCode_mB20D3856AF84E5AA5A7222DE08C7F21CA127039D_AdjustorThunk,
	uint3_ToString_m02FAB1C4D7D9A4831F7D0D78AF975B8C4B7EF4FD_AdjustorThunk,
	uint3_ToString_m7705E589BDC0FCC9D87428DEFD95A66EF4A7A6AF_AdjustorThunk,
	uint4__ctor_m0B7104813D16F1B7AE4549A27050566483847240_AdjustorThunk,
	uint4_op_Multiply_mC55C0883F454861C611AF783A442F4B8FB5EAB86,
	uint4_op_Multiply_m6302785769695A846A3A586E86A3455561C1582B,
	uint4_op_Addition_m54936586CF71B152336C0EED75110ADF9CA704CE,
	uint4_op_Addition_m58A8D6BFA5DDA9143D132B57084CB4B271EF1D35,
	uint4_op_LeftShift_m031F4835321567AB31E062A9BBD456FEBBF50405,
	uint4_op_RightShift_m8714E5CBC739B35AB61645F0E5B2569C2D929DD3,
	uint4_op_BitwiseOr_mA59ED29C9B5B61C2FAE0846C61049A7D69F1F0C0,
	uint4_Equals_m302069717D95B95C972C7C1B7D29D9E0DBBD6542_AdjustorThunk,
	uint4_Equals_mA3312E8FC1A4734E2402424ADAC7BCE386918488_AdjustorThunk,
	uint4_GetHashCode_m187968010316E4D76A2FD32E8CD068A299007AA8_AdjustorThunk,
	uint4_ToString_m1ADFBF43F0EC44A59D7F9908C9891DC3C31690BC_AdjustorThunk,
	uint4_ToString_mED39980F32C81D913660EC644111F2070E7870A2_AdjustorThunk,
};
static const int32_t s_InvokerIndices[78] = 
{
	2239,
	2240,
	2241,
	2242,
	2243,
	231,
	231,
	2244,
	2245,
	395,
	2246,
	395,
	2246,
	2247,
	394,
	394,
	2248,
	138,
	2249,
	2250,
	810,
	2251,
	2252,
	2253,
	2249,
	2254,
	2250,
	1244,
	2255,
	2256,
	2246,
	2246,
	2257,
	9,
	10,
	14,
	113,
	2258,
	1103,
	2259,
	2260,
	2261,
	2262,
	2263,
	9,
	10,
	14,
	113,
	2264,
	2265,
	2266,
	2267,
	9,
	10,
	14,
	113,
	2268,
	3,
	38,
	2269,
	2270,
	9,
	10,
	14,
	113,
	300,
	2271,
	2272,
	2271,
	2272,
	2272,
	2272,
	2271,
	2273,
	9,
	10,
	14,
	113,
};
extern const Il2CppCodeGenModule g_Unity_MathematicsCodeGenModule;
const Il2CppCodeGenModule g_Unity_MathematicsCodeGenModule = 
{
	"Unity.Mathematics.dll",
	78,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
