﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class missionwaypoint : MonoBehaviour
{
    public GameObject IMAGE;

    public GameObject target;
    public Camera cam;
    private Image img;

    public GameObject Player;

    private void Start()
    {
        cam = GetComponent<Camera>();
        img = IMAGE.GetComponent<Image>();
    }
    void Update()
    {
        float minX = img.GetPixelAdjustedRect().width / 2;
        float maxX = Screen.width - minX;

        float minY = img.GetPixelAdjustedRect().height / 2;
        float maxY = Screen.height - minY;

        Vector2 pos = Camera.main.WorldToScreenPoint(target.transform.position);

        pos.x = Mathf.Clamp(pos.x, minX, maxX);
        pos.y = Mathf.Clamp(pos.y, minY, maxY);

        img.transform.position = pos;

        //lookrotation();
    }
    public void lookrotation()
    {
        Vector3 playerpos = cam.WorldToScreenPoint(Player.transform.position);
        Vector3 dir = playerpos - IMAGE.transform.position;
        Quaternion rotation = Quaternion.LookRotation(dir, Vector3.up);
        //IMAGE.transform.DOLookAt()
        IMAGE.transform.localRotation = rotation;
      
       
    }
}