﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootPlayer : MonoBehaviour
{
    public GameObject Playerprefab;
    public float shootspeed;
    public float Zvalue;
    public Vector2 shootvector;
    private Transform target;
    private Rigidbody2D instPlayerRigid;
    private bool dead;
    private GameObject Player;
    private bool isactived;
    private bool ghostballactive;
    private Transform currentbullet;
    public GameObject gameobserver;


    private void Start()
    {
        isactived = false;
        //Playerprefab = GameObject.FindGameObjectWithTag("Playerprefab");
        target = GameObject.FindGameObjectWithTag("Fokuspoint_end").transform;
        Player = GameObject.FindGameObjectWithTag("Player");
        ghostballactive = false;
    }
    void Update()
    {
        Debug.Log(dead);

        if (Input.GetMouseButtonDown(0))
        {
            cleanoldballs();
            shoottodirection();
            GameObject instPlayer = Instantiate(Playerprefab, transform.position, Quaternion.identity) as GameObject;
            instPlayer.GetComponent<Rigidbody2D>().AddRelativeForce(shootvector * shootspeed);
            ghostballactive = true;
            gameobserver.GetComponent<Gameobserver>().BulletShot();
            return;

        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (ghostballactive)
            {
                currentbullet = GameObject.FindGameObjectWithTag("ghostball").transform;
                cleanoldballs();
                Player.transform.position = currentbullet.position;
                ghostballactive = false;
            }
        }


    }
    private void cleanoldballs()
    {
        GameObject oldball = GameObject.FindGameObjectWithTag("ghostball");
        Destroy(oldball);
    }
    public void shoottodirection()
    {
        shootvector = target.transform.position - transform.position;
    }
}