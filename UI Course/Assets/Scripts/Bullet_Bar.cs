﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bullet_Bar : MonoBehaviour
{
    public Gameobserver observer;
    Image fillBulletsImage;
    private static int bulletAmount;

    // Start is called before the first frame update
    void Start()
    {
        fillBulletsImage = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        bulletAmount = observer.bulletAmount;
        fillBulletsImage.fillAmount = (float)bulletAmount / 100;
    }
}
