﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class image : MonoBehaviour
{
    public Gameobserver observer;
    Image fillImage;
    private static int health;

    // Start is called before the first frame update
    void Start()
    {
        fillImage = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        health = observer.playerHealth;
        fillImage.fillAmount = (float)health / 100;
    }
}
