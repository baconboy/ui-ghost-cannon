﻿using System.Collections; 
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour
{
    public float speed;
    public float rotationspeed = 4.0f;
    Vector3 averageheading;
    Vector3 averageposition;
    public float neighbourDistance = 2.0f;
    bool turning = false;
    private float startspeed;
    [SerializeField]
    private float sprintspeed;

    public GameObject globalF;
    void Start()
    {
        startspeed = speed;
        speed = Random.Range(0.5f, 1);

    }

    void Update()
    {
        if(Vector3.Distance(transform.position, Vector3.zero) >= globalFlock.tankSize)
        {
            turning = true;
        }
        else
        {
            turning = false;
        }
        if(turning)
        {
            Vector3 direction = Vector3.zero - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationspeed * Time.deltaTime);
            speed = Random.Range(0.5f, 1);
        }
        if(Random.Range(0,5) < 1)
        {
            ApplyRules();
            transform.Translate(0, 0, Time.deltaTime * speed);

        }
        transform.Translate(0, 0, Time.deltaTime * speed);

        
    }
    void ApplyRules()
    {
        GameObject[] gos;
        gos = globalFlock.allFish;
        Vector3 vcentre = Vector3.zero;
        Vector3 vavoid = Vector3.zero;
        float gSpeed = 0.1f;

        Vector3 goalPos = globalFlock.goalPos;
        float dist;

        int groupSize = 0;

        foreach ( GameObject go in gos)
        {
            if(go != this.gameObject)
                {
                dist = Vector3.Distance(go.transform.position, this.transform.position);

                if (dist <= neighbourDistance)
                {
                    vcentre += go.transform.position;
                    groupSize++;

                    if (dist < 1.0f)
                    {
                        vavoid = vavoid + (this.transform.position - go.transform.position);

                    }
                    Flock anotherFlock = go.GetComponent<Flock>();
                    gSpeed = gSpeed + anotherFlock.speed;
                }
            }
        }

        if(groupSize > 0)
        {
            vcentre = vcentre / groupSize + (goalPos - this.transform.position);
            speed = gSpeed / groupSize;

            Vector3 direction = (vcentre + vavoid) - transform.position;

            if(direction != Vector3.zero)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), rotationspeed * Time.deltaTime);
            }
        }
    }

    private void OnMouseEnter()
    {
        turning = true;
        Debug.Log("Beep");
        speed = sprintspeed;
    }
    private void OnMouseExit()
    {

        turning = false;
        speed = startspeed;
    }
}
