﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class globalFlock : MonoBehaviour
{
    public GameObject fishprefab;
    public static int tankSize;
    public int cage;
    static int numFish = 30;
    public static GameObject[] allFish = new GameObject[numFish];
    Flock FLOCK;
    public int maxtanksizeX;
    public int mintanksizeX;
    public int maxtanksizeY;
    public int mintanksizeY;
    public int maxtanksizeZ;
    public int mintanksizeZ;
    public bool mouseisover = false;

    public GameObject goalprefab;

    public static Vector3 goalPos = Vector3.zero;

    private void Start()
    {
        tankSize = cage;
        for (int i = 0; i < numFish; i++)
        {
            Vector3 pos = new Vector3(Random.Range(mintanksizeX, maxtanksizeX),
                                      Random.Range(mintanksizeY, maxtanksizeY),
                                      Random.Range(mintanksizeZ, maxtanksizeZ));
            allFish[i] = (GameObject)Instantiate(fishprefab, pos, Quaternion.identity);
        }
    }
    private void Update()
    {
        if (Random.Range(0, 10000) < 50)
        {
            goalPos = new Vector3(Random.Range(mintanksizeX, maxtanksizeX),
                                  Random.Range(mintanksizeY, maxtanksizeY),
                                  Random.Range(mintanksizeZ, maxtanksizeZ));
            goalprefab.transform.position = goalPos;
        }
        if (mouseisover)
        {
            goalPos = new Vector3(Random.Range(mintanksizeX, maxtanksizeX),
                                  Random.Range(mintanksizeY, maxtanksizeY),
                                  Random.Range(mintanksizeZ, maxtanksizeZ));
            goalprefab.transform.position = goalPos;
        }
      
    }




}
