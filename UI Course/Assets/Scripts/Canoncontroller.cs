﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Canoncontroller : MonoBehaviour
{
    public float followspeed;
    private Transform target;
    private Vector2 Mouseposition;
    public float lookspeed;
    public float generalzposition;
    private Vector3 combinedpos;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

        
    }

    // Update is called once per frame
    void Update()
    {
        Mouseposition = Input.mousePosition;
        transform.position = Vector2.MoveTowards(transform.position, target.position, followspeed * Time.deltaTime);
        Vector3 targetDir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = (Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg) - 90f;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

}
