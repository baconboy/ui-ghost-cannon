﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Playercontroller : MonoBehaviour
{
    public float Playerspeed;
    public float Jumpheight;
    private Rigidbody2D Rigid;
    //public Vector2 Groundchecklength;
    public float grounddistance;
    //public bool isgrounded;
    //public GameObject Colliderhost;
    public LayerMask groundlayer;
    public bool openUI;
    public CanvasGroup UI;
   

    void Start()
    {
        Rigid = this.GetComponent<Rigidbody2D>();
        openUI = false;
    }

    void Update()
    {
                    basicmovement();

    }
    bool isgrounded()
    {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        RaycastHit2D hit = Physics2D.Raycast(position, direction, grounddistance, groundlayer);
        if (hit.collider != null)
        {
            return true;
        }
        return false;
        //isgrounded = false;
        //bool isgrounded = Physics2D.Raycast(transform.position, Groundchecklength);
        //Debug.DrawRay(transform.position, Groundchecklength, Color.red);

        //if (groundhit.collider.tag == "Ground")
        //{
        //    isgrounded = true;
        //    Debug.Log("ground");
        //}
        //else
        // {
        //    isgrounded = false;
        //    Debug.Log("false");
        //}
    }
    public void basicmovement()
    {
        if (Input.GetKey(KeyCode.A))
        {
            Rigid.AddForce(-Camera.main.transform.right * Playerspeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            Rigid.AddForce(Camera.main.transform.right * Playerspeed);
        }
        if (Input.GetKey(KeyCode.Space))
        {
            jumping();
        }
    }
    public void jumping()
    {

            if(!isgrounded())
            {
            return;
           

            }
        else
        {
         Rigid.AddForce(Camera.main.transform.up * Jumpheight);
        }
        

    }

}
