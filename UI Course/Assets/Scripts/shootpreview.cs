﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootpreview : MonoBehaviour
{
    public LineRenderer linerenderer;
    private int numberofpoints = 10;
    private Transform point0, point1;
    private Transform halfpoint;
    private Vector3[] positions = new Vector3[10];
    public float T;
  
    private void Start()
    {
        point1 = GameObject.FindGameObjectWithTag("Fokuspoint_end").transform;

        linerenderer.enabled = false;

        linerenderer.positionCount = numberofpoints;

        point0 = GameObject.FindGameObjectWithTag("ghostballspawner").transform;


    }
    private void Update()
    {
        if (Input.GetMouseButton(1))
        {
            point0 = GameObject.FindGameObjectWithTag("ghostballspawner").transform;
            linerenderer.enabled = true;

            DrawLinearCurve();
            DrawLinearCurve2();
            Time.timeScale = 0.2f;
        }
        else
        {
            linerenderer.enabled = false;
            Time.timeScale = 1f;
        }
    }

    private Vector3 CalculatelinearBezierpoint(float T, Vector3 p0, Vector3 P1)
    {
        return p0 + T * (P1 - p0);

    }


    private void DrawLinearCurve()
    {
        point1 = GameObject.FindGameObjectWithTag("Fokuspoint_end").transform;

        for (int i = 1; i< numberofpoints + 1; i++)
        {
            float T = i / (float)numberofpoints;
            positions[i - 1] = CalculatelinearBezierpoint(T, point0.position, point1.position); 
        }
        //halfpoint.position = positions[numberofpoints / 2];
        // wiederhole die berechnung linear curve, mit parameterersatz
        linerenderer.SetPositions(positions);

    }
    private void DrawLinearCurve2()
    {
        for (int i = 1; i < numberofpoints + 1; i++)
        {
            float T = i / (float)numberofpoints;
            positions[i - 1] = CalculatelinearBezierpoint(T, point0.position, linerenderer.GetPosition(5));
        }
        //halfpoint.position = positions[numberofpoints / 2];
        // wiederhole die berechnung linear curve, mit parameterersatz
        linerenderer.SetPositions(positions);
    }
}

//private void Awake()
//{
//    StartCoroutine(LineDraw());
//}
//IEnumerator LineDraw()
//{
//    float t = 0;
//    float time = 2;
//    Vector3 orig = lr.GetPosition(0);
//    Vector3 orig2 = lr.GetPosition(1);
//    lr.SetPosition(1, orig);
//    Vector3 newpos;
//    for (; t < time; t += Time.deltaTime)
//    {
//        newpos = Vector3.Lerp(orig, orig2, t / time);
//        lr.SetPosition(1, newpos);
//        yield return null;
//    }
//    lr.SetPosition(1, orig2);
//}
