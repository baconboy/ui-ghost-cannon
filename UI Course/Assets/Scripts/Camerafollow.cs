﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camerafollow : MonoBehaviour
{
    private GameObject player;
    public Vector3 offset;
    public int followspeed;
    public float Zposition;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        //transform.position = new Vector3(0, 0, Zposition);
       
    }

    void Update()
    {
        transform.position = new Vector3(player.transform.position.x + offset.x, player.transform.position.y, Zposition);
    }
}
