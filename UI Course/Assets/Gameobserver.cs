﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gameobserver : MonoBehaviour
{

    private GameObject Player;
    public bool isdead;
    private GameObject Spawnpoint;
    private GameObject Playerrigid;
    public int playerHealth;
    public int startPlayerHealth;
    public int damage;
    public int bulletAmount;
    public int startBulletAmount;
    public int bulletValue;

    private void Start()
    {
        isdead = false;
        Playerrigid = GameObject.FindGameObjectWithTag("Player");
        Spawnpoint = GameObject.FindGameObjectWithTag("Playerspawn");
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Main Menu");
        }
    }

    public void PlayerDamaged()
    {
        playerHealth =playerHealth-damage;
        print(playerHealth);

        if(playerHealth <= 0)
        {
            Playerdied();
        }
    }

    public void Playerdied()
    {
        Player = GameObject.FindGameObjectWithTag("Player"); 
        Playerrigid.GetComponent<Rigidbody2D>().isKinematic = true;
        Player.transform.position = Spawnpoint.transform.position;
        isdead = false;
        Playerrigid.GetComponent<Rigidbody2D>().isKinematic = false;
        playerHealth = startPlayerHealth;
        bulletAmount = startBulletAmount;
        Debug.Log("spawnpoint should work here");
        
    }

    public void BulletShot()
    {
        bulletAmount = bulletAmount - bulletValue;
        print(bulletAmount);

        if(bulletAmount <= 0)
        {
            Playerdied();
        }
    }
}
