﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIanchor : MonoBehaviour
{


    public GameObject target;
    public GameObject itself;
    public Camera cam;
    public float depth;

    private void Start()
    {
        cam = GetComponent<Camera>();
        
        
    }
    void Update()
    {
        this.transform.position = Camera.main.ScreenToWorldPoint(target.transform.position) + new Vector3(0, 0, depth);
      
        itself.SetActive(target.activeSelf);
        
    }
}
