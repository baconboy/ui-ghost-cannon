﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class Custombuttonscript : MonoBehaviour
{
    public GameObject Upperobj;
    public GameObject Particleholder;
    private bool hoverover;
    
    private bool gotpressed;
    private bool afterclick;
    private bool inactive;
    private bool idle;

    public GameObject [] PS;
    private Animator anim;

    private GameObject mainbutton;

    public bool hidemenu;
    public bool unhidemenu;

    void Start()
    {
        anim = this.GetComponent<Animator>();
        idle = true;
        ingameUIactive = false;
        _menugroup = GameObject.FindGameObjectWithTag("ingamemenu").GetComponent<CanvasGroup>();
        mainbutton = GameObject.FindGameObjectWithTag("mainbutton3d");
    }


    private void OnMouseOver()
    {
        //idle = false;
        //hoverover = true;
        //gotpressed = false;

        anim.Play("hoverover");
    }
    private void OnMouseExit()
    {
        anim.Play("idle");

        //idle = true;
        //Debug.Log("hovert nicht");
    }
    private void OnMouseDown()
    {
        if (this.tag == "Mainmenubutton")
        {
            
            dragtiteldown();

           
        }
        anim.Play("onclick");
        foreach (GameObject ps in PS)
        {
            ps.GetComponent<ParticleSystem>().Play();
        }
        FadeOUT();
        FadeGroup();


    }
    private void OnMouseUp()
    {
       
    }


    public CanvasGroup _group;
    public float time;
    [Range(0, 1)] public float fadeEnd;
    public CanvasGroup _hostgroup;
    [Range(0, 1)] public float fadeoutend;
    public GameObject Titleobj;
    private bool escapemenu;
    public float Yend;
    public float Ydown;
    private string sceneName;
    private bool isIngameSceneLoaded;
    private bool ingameUIactive;
    private CanvasGroup _menugroup;


    private void Update()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;


        if (sceneName == "Main Menu")
        {
            isIngameSceneLoaded = false;
        }
        if (sceneName == "Test level")
        {
            isIngameSceneLoaded = true;
        }
        if (isIngameSceneLoaded)
        {
            Debug.Log("richtige scene");
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!ingameUIactive)
                {
                    StartCoroutine(FadeESC());
                }
                if (ingameUIactive)
                {
                    StartCoroutine(FadeoutESC());
                }

            }
        }
    }
    public void fadeoutesc()
    {
        StartCoroutine(FadeoutESC());
    }
    public void fadeinesc()
    {
        StartCoroutine(FadeESC());
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void START()
    {
        SceneManager.LoadScene("Test level");

    }
    public void LoadMM()
    {
        SceneManager.LoadScene("Main Menu");

    }

    public void FadeGroup()
    {
        StartCoroutine(Fade());

    }
    public void FadeOUT()
    {
        StartCoroutine(Fadeout());

    }
    public void dragtiteltop()
    {
        Titleobj.transform.DOMoveY(Yend, time, true);
    }
    public void dragtiteldown()
    {
        Titleobj.transform.DOMoveY(Ydown, time, true);
    }
    IEnumerator Fade()
    {
        float fadestart = _group.alpha;
        _group.gameObject.SetActive(true);


        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / time)
        {
            _group.alpha = Mathf.Lerp(fadestart, fadeEnd, t);
            yield return null;
        }
        if (this.tag == "Mainmenubutton")
        {
            if (hidemenu)
            {
                hidemainbuttons();

            }
            if (unhidemenu)
            {
                unhidemainbuttons();
            }
        }
    }
    IEnumerator Fadeout()
    {
        float fadestart = _hostgroup.alpha;

        for (float b = 1.0f; b > 0.0f; b -= Time.deltaTime / time)
        {
            _hostgroup.alpha = Mathf.Lerp(fadestart, fadeoutend, b);

            yield return null;
        }
        _hostgroup.alpha = fadeoutend;
        _hostgroup.gameObject.SetActive(false);

        if (this.tag == "Mainmenubutton")
        {
            if (hidemenu)
            {
                hidemainbuttons();

            }
            if (unhidemenu)
            {
                unhidemainbuttons();
            }
        }
    }
    IEnumerator FadeESC()
    {
        float fadestart = _menugroup.alpha;
        //float fadeEnd = 1;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / time)
        {
            _menugroup.alpha = Mathf.Lerp(fadestart, fadeEnd, t);
            yield return null;
            ingameUIactive = true;
        }
    }
    IEnumerator FadeoutESC()
    {
        float fadestart = _menugroup.alpha;

        for (float b = 1.0f; b > 0.0f; b -= Time.deltaTime / time)
        {
            _menugroup.alpha = Mathf.Lerp(fadestart, fadeoutend, b);

            yield return null;
        }
        _menugroup.alpha = fadeoutend;
        ingameUIactive = false;
    }

    public void hidemainbuttons()
    {
        mainbutton.SetActive(false);
    }
    public void unhidemainbuttons()
    {
        mainbutton.SetActive(true);
    }

}
